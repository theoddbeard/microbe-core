/*
 * pager.c
 *
 *  Created on: 23 дек. 2013 г.
 *      Author: theoddbeard
 */
#include "console.h"
#define PAGE_ATTR 0b1111
typedef unsigned int pageCatalog;

pageCatalog* mainCatalog;
void* freePage;
unsigned int freeCount;


void* pager_getPage(){
	freeCount--;
	return freePage+=0x1000;
}

void* getFreeBorder(){
	return freePage+0x1000;
}
void initCatalog(pageCatalog* catalog){
	for (unsigned int i=0;i<0x1000;i++){
		catalog[i] = 0x0;
	}
}

void pager_init(unsigned int freeSize,void* freeAddr){
	printfln("Initialization pager: Free memory (%d bytes) begins at %h",freeSize,freeAddr);
	freeCount = (freeSize>>12)-1;
	freePage = (void*)((unsigned int)freeAddr&0xFFFFF000);
	mainCatalog = pager_getPage();
	printfln("Free pages count=%d, Main catalog at %h",freeCount,mainCatalog);
	initCatalog(mainCatalog);
}


void pager_mount(void* paddr,void* vaddr){
	//printfln("Mount %h to %h",paddr,vaddr);
	unsigned int catNumber = (unsigned int)vaddr>>22;
	unsigned int entryNumber = ((unsigned int)vaddr>>12)&0x3FF;
	pageCatalog* catalog = (pageCatalog*)mainCatalog[catNumber];
	if (((unsigned int)catalog&0x1)==0){
		catalog = pager_getPage();
		initCatalog(catalog);
		mainCatalog[catNumber]=(((unsigned int)catalog))|PAGE_ATTR;
	}
	catalog = (pageCatalog*)((unsigned int)catalog&0xFFFFFC00);
	//printfln("new catalog %h",catalog);
	catalog[entryNumber] = ((unsigned int)paddr&0xFFFFF000)|PAGE_ATTR;
}

void pager_switchOn(){
	__asm__("pushl %eax");
	__asm__("movl mainCatalog,%eax");
	__asm__("movl %eax,%cr3");

	__asm__("movl %cr0,%eax");
	__asm__("orl $0x80000000,%eax");
	__asm__("movl %eax,%cr0");
	__asm__("popl %eax");
}
