#include "console.h"
#include <stdarg.h>

unsigned short width,height;
unsigned short x,y;

unsigned char color;
unsigned char color_str;
unsigned char color_dec;
unsigned char color_hex;

char* vbuffer;
char* cursor;
char* secondline;
char* lastline;

char lptchar;
void lptout(char c){
	lptchar = c;
	__asm__("pushw %ax");
	__asm__("pushw %dx");
	__asm__("movw $0x278,%dx");
	__asm__("movb lptchar,%al");
	__asm__("outb %al,%dx");
	__asm__("popw %dx");
	__asm__("popw %ax");
}
void console_init(unsigned short aWidth, unsigned short aHeight){
	vbuffer = (char*) 0xb8000;
	cursor = vbuffer;
	width = aWidth;
	height = aHeight;
	x = 0;
	y = 0;
	color = EGAWhite;
	color_str = EGAWhite;
	color_dec = EGABlue;
	color_hex = EGAYellow;
	
	secondline = vbuffer+width*2;
	lastline = vbuffer+(width*(height-1)*2);
	
	unsigned long count = aWidth*aHeight;
	for(unsigned long i = 0;i<count;i++){
		vbuffer[i*2] = color;
		vbuffer[i*2+1] = 0x00;
	}
	
	
}

void console_setColor(EGAColor aColor){
	color = aColor;
}

EGAColor console_getColor(){
	return color_str;
}

void memcpyd(short* src,short* dest,unsigned long count){
	for(unsigned long i=0;i<count;i++){
		dest[i] = src[i];
	}
}




void checkCursorPosition(){
	if (x==width){
		x = 0;
		y++;
		cursor+=2;
	}
	
	if (y==height){
		memcpyd((short*)secondline,(short*)vbuffer,width*(height-1));
		for (unsigned short i=0;i<width;i++){
			lastline[i*2] = 0x0;
			lastline[i*2+1] = color;
		}
		y--;
		cursor-=width<<1;
	}	
}

void doCR(){
	unsigned short linePosition = (cursor-vbuffer)%(width<<1);
	x-=linePosition;
	cursor-=linePosition;
	checkCursorPosition();
	lptout(0x0D);
}

void doLF(){
	y++;
	cursor+=width<<1;
	checkCursorPosition();
	lptout(0x0A);
}

void doTab(){
	console_outString("        ");
}


void console_outChar(char aChar){
	checkCursorPosition();
	cursor[0] = aChar;
	cursor[1] = color;
	lptout(aChar);
	x++;	
	cursor+=2;
}

void console_outString(char* aString){	
while (aString[0] != 0x0){
	unsigned char achar = aString[0];	
	switch(achar){
		case 0x0A: doLF();break;
		case 0x0D: doCR();break;
		case 0x09: doTab();break;
		default:console_outChar(achar);break;
	}
	aString++;
}
		
}
void console_outStringln(char* aString){
	console_outString(aString);
	doCR();
	doLF();
	
}


void console_outIntDec(unsigned long aValue){
	char buffer[11];
	char* current = &buffer[10];
	
	*current = 0x0;
	do{
		current--;		
		char digit = (aValue%10)+0x30;						
		*current = digit;
		
		
		aValue = aValue/10;
	} while (aValue>0);

	char oldc = color;
	color = color_dec;
	console_outString(current);
	color = oldc;
	
	
	
}
void console_outIntDecln(unsigned long aValue){
	console_outIntDec(aValue);
	doCR();
	doLF();
}

void console_outIntHex(unsigned long aValue){
	char buffer[11];
	char *string = &buffer[8];
	*string = 0x0;
	do{
		string--;
		char digit = aValue&0xF;

		aValue = aValue>>4;
		
		switch(digit){
			case 0x0A:digit = 'A';break;
			case 0x0B:digit = 'B';break;
			case 0x0C:digit = 'C';break;
			case 0x0D:digit = 'D';break;
			case 0x0E:digit = 'E';break;
			case 0x0F:digit = 'F';break;
			default:digit+=0x30;break;
		}
		*string = digit;		
	} while (aValue>0);
	char oldc = color;
	color = color_hex;
	console_outString("0x");
	console_outString(string);
	color = oldc;
	
}
void console_outIntHexln(unsigned long aValue){
		console_outIntHex(aValue);
		doCR();
		doLF();
}

void console_endline(){
	doCR();
	doLF();
}

void vprintf(const char* format,va_list args){
	while (*format){
		switch(*format){
			case '%':
				format++;
				switch(*format){
					case 's':
						console_outString(va_arg(args, char*));
						break;
					case 'c':
						console_outChar(va_arg(args,unsigned int));
						break;
					case 'd':
						console_outIntDec(va_arg(args,unsigned int));
						break;
					case 'h':
						console_outIntHex(va_arg(args,unsigned int));
						break;
					case 'n':
						console_endline();
						break;
					case 't':
						doTab();
						break;
				}
				break;
			
			default: 
				console_outChar(*format);
						
		}
		format++;
	}
	
}

void printf(const char* format,...){
	va_list args;
	va_start(args,format);
	vprintf(format,args);
	va_end(args);
	
}

void printfln(const char* format,...){
	va_list args;
	va_start(args,format);
	vprintf(format,args);
	console_endline();
	va_end(args);
}
