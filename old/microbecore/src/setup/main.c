#include "multiboot.h"
#include "elf.h"
#include "console.h"
#include "pager.h"
#include "utils.h"

typedef void(*kinit)(unsigned int memsz,void* freemem);

void* loadModule(moduleInfo* module){
	elfHeader* modHeader = (elfHeader*)module->mod_start;
	printfln("Module %s",module->cmdLine);

	for (unsigned int i=0;i<modHeader->e_phnum;i++){
		elfProgHdr* pSect = (elfProgHdr*)(modHeader->e_phoff+module->mod_start)+i;
		printfln("Secton paddr=%h,vaddr=%h,type=%d,psize=%d,vsize=%d",pSect->p_offset,pSect->p_vaddr,pSect->p_type,pSect->p_filesz,pSect->p_memsz);
		if (pSect->p_type==PT_LOAD){
			for (unsigned int j=0;j<((pSect->p_memsz+0xFFF)&0xFFFFF000);j+=0x1000){
				void* src = (void*)(module->mod_start+pSect->p_offset+j);
				void* dst = (void*)(pSect->p_vaddr+j);
				//printfln("Mount %h to %h",src,dst);
				pager_mount(src,dst);
			}
		}
	}
	return modHeader->e_entry;
}


void main(multibootInfo* mbInfo){
	console_init(80,25);
	printfln("Starting MicrobeCore...");
	unsigned int memSzTotal = (mbInfo->mem_lower+mbInfo->mem_upper)<<10;
	printfln("Memory size=%dMb",(mbInfo->mem_lower+mbInfo->mem_upper)>>10);
	moduleInfo* lastModule = &mbInfo->mods_addr[mbInfo->mods_count-1];
	printfln("Modules count=%d, last module loaded at %h",mbInfo->mods_count,lastModule->mod_end);

	pager_init(memSzTotal-(unsigned int)lastModule->mod_end,lastModule->mod_end);

	unsigned int basePages = ((unsigned int)lastModule->mod_end>>12)+1;

	printfln("Switching on pager");
	kinit kInit = loadModule(mbInfo->mods_addr);
	printfln("Free mem at %h",getFreeBorder());
	for (unsigned int i=0;i<=(unsigned int)getFreeBorder()+0x5000;i+=0x1000){
			pager_mount((void*)i,(void*)i);
	}
	pager_switchOn();
	kInit(memSzTotal,getFreeBorder());

	for(;;);

}



