typedef enum EGAColor {EGABlack=0,EGANavy=1,EGAGreen=2,EGATeal=3,EGAMaroon=4,EGAPurpure=5,EGAOlive=6,EGAGray=7,
	 EGASilver=8,EGABlue=9,EGALime=10,EGAAqua=11,EGARed=12,EGAFuchsia=13,EGAYellow=14,EGAWhite=15} EGAColor;
	 
void console_init(unsigned short aWidth, unsigned short aHeight);
void console_setColor(EGAColor);

void console_outString(char*);
void console_outStringln(char*);

void console_outIntDec(unsigned long);
void console_outIntDecln(unsigned long);

void console_outIntHex(unsigned long);
void console_outIntHexln(unsigned long);

void console_endline();

void printf(const char* format,...);
void printfln(const char* format,...);