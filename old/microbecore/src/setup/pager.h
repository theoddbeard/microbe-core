/*
 * pager.h
 *
 *  Created on: 03 февр. 2014 г.
 *      Author: theoddbeard
 */

void* pager_getPage();
void pager_init(unsigned int freeSize,void* freeAddr);
void pager_mount(void* paddr,void* vaddr);
void pager_switchOn();
void* getFreeBorder();
