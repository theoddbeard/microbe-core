#include "tss.h"
#define TSS_ADDR 0xFFFFF400
/*

+00h: 4 байта - селектор предыдущей задачи (старшее слово содержит нули -
здесь и для всех остальных селекторов)
+04h: 4 байта - ESP для CPL = О
+08h: 4 байта - SS для CPL = О
+OCh: 4 байта - ESP для CPL = 1
+10h: 4 байта - SS для CPL = 1
+14h: 4 байта - ESP для CPL = 2
+ 18h: 4 байта - SS для CPL = 2
+lCh: 4байта-СКЗ
+20h: 4байта-Е1Р
+24h: 4 байта - EFLAGS
+28h: 4байта-ЕАХ
+2Ch: 4 байта - ЕСХ
+30h: 4 байта - EDX
+34h: 4байта-ЕВХ
+38h: 4 байта-ESP
+3Ch: 4 байта - EBP
+40h: 4 байта - ESI
+44h: 4 байта-EDI
+48h: 4 байта - ES
+4Ch: 4 байта - CS
+50h: 4 байта - SS
+54h: 4 байта - DS
-f 58h: 4 байта - FS
+5Ch: 4 байта - GS
+60h: 4 байта - LDTR
+64h: 2 байта - слово флагов задачи
 */

typedef struct TSS {
	unsigned int previous_tss;
	unsigned int ESP0;
	unsigned int SS0;
	unsigned int EPS1;
	unsigned int SS1;
	unsigned int ESP2;
	unsigned int SS2;
	unsigned int CR3;
	unsigned int EIP;
	unsigned int EFLASGS;
	unsigned int EAX;
	unsigned int ECX;
	unsigned int EDX;
	unsigned int EBX;
	unsigned int ESP;
	unsigned int EBP;
	unsigned int ESI;
	unsigned int EDI;
	unsigned int ES;
	unsigned int CS;
	unsigned int SS;
	unsigned int DS;
	unsigned int FS;
	unsigned int GS;
	unsigned int LDTR;
	unsigned int tss_flags;
} TSS;

TSS* tss;

void tss_init(){
	tss = (TSS*)TSS_ADDR;
	tss->SS0 = 0x10;
	tss->ESP0 = 0xEFBFEFF0;
}

void tss_flush(){
	__asm__("pushl %eax");
	__asm__("movw $0x3B,%ax");
	__asm__("ltr %ax");
	__asm__("popl %eax");
}

void* tss_get_addr(){
	return tss;
}

unsigned int tss_get_size(){
	return sizeof(TSS);
}



