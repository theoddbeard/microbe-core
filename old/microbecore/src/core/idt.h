/*
 * idt.h
 *
 *  Created on: 04 февр. 2014 г.
 *      Author: theoddbeard
 */
Ivoid idt_init();
void idt_set_int_handler(unsigned char intNumber, unsigned short segment,void* handler,unsigned int handlerType);
