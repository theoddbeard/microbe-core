void tss_init();
void tss_flush();

void* tss_get_addr();
unsigned int tss_get_size();
