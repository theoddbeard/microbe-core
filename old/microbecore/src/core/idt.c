/*
 * idt.c
 *
 *  Created on: 04 февр. 2014 г.
 *      Author: theoddbeard
 */

#include "idt.h"
#include "console.h"
#include "idt-handlers.inc"
#define IDT_BASE_ADDR (IDTEntry*)0xFFFFF800

#pragma pack(1)
typedef struct IDTEntry{
	unsigned char byte0;
	unsigned char byte1;
	unsigned char byte2;
	unsigned char byte3;
	unsigned char byte4;
	unsigned char byte5;
	unsigned char byte6;
	unsigned char byte7;
} IDTEntry;

struct{
	unsigned short size;
	void* addr;
} LIDT_arg;

#pragma pack(0)

IDTEntry* idt;
IDTEntry idtTable[256];



void idt_init(){
	idt = (IDTEntry*)&idtTable;//IDT_BASE_ADDR;

	for(unsigned short i=0;i<256;i++){
		idt[i].byte5 = 0;
	}

	LIDT_arg.size = 8*256-1;
	LIDT_arg.addr = idt;

	__asm__("lidt LIDT_arg");

	idt_set_int_handler(0,0x8,int_0,INT_HANDLER_TRAP);
	idt_set_int_handler(1,0x8,int_1,INT_HANDLER_TRAP);
	idt_set_int_handler(2,0x8,int_2,INT_HANDLER_TRAP);
	idt_set_int_handler(3,0x8,int_3,INT_HANDLER_TRAP);
	idt_set_int_handler(4,0x8,int_4,INT_HANDLER_TRAP);
	idt_set_int_handler(5,0x8,int_5,INT_HANDLER_TRAP);
	idt_set_int_handler(6,0x8,int_6,INT_HANDLER_TRAP);
	idt_set_int_handler(7,0x8,int_7,INT_HANDLER_TRAP);
	idt_set_int_handler(8,0x8,int_8,INT_HANDLER_TRAP);
	idt_set_int_handler(9,0x8,int_9,INT_HANDLER_TRAP);
	idt_set_int_handler(10,0x8,int_10,INT_HANDLER_TRAP);
	idt_set_int_handler(11,0x8,int_11,INT_HANDLER_TRAP);
	idt_set_int_handler(12,0x8,int_12,INT_HANDLER_TRAP);
	idt_set_int_handler(13,0x8,int_13,INT_HANDLER_TRAP);
	idt_set_int_handler(14,0x8,int_14,INT_HANDLER_TRAP);
	idt_set_int_handler(15,0x8,int_15,INT_HANDLER_TRAP);
	idt_set_int_handler(16,0x8,int_16,INT_HANDLER_TRAP);
	idt_set_int_handler(17,0x8,int_17,INT_HANDLER_TRAP);
	idt_set_int_handler(18,0x8,int_18,INT_HANDLER_TRAP);


}

void idt_set_int_handler(unsigned char intNumber, unsigned short segment,void* handler,unsigned int handlerType){
	idt[intNumber].byte7 = (unsigned int)handler>>24;
	idt[intNumber].byte6 = ((unsigned int)handler>>16)&0xFF;
	idt[intNumber].byte5 = 0b10000000|0xF;
	idt[intNumber].byte4 = 0;
	idt[intNumber].byte3 = 0;
	idt[intNumber].byte2 = segment;
	idt[intNumber].byte1 = ((unsigned int)handler>>8)&0xFF;
	idt[intNumber].byte0 = (unsigned int)handler&0xFF;


}

void int_handler(unsigned int intn,unsigned int errcode,unsigned int addr,unsigned short segment){
	//__asm__("pusha");
	printfln("[Int %h,error=%h,%h:%h]",intn,errcode,segment,addr);
	//__asm__("popa");
	//__asm__("hlt");
	//for(;;);
}



