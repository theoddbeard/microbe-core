/*
 * pager.h
 *
 *  Created on: 04 февр. 2014 г.
 *      Author: theoddbeard
 */
void pager_init(void* upperBorder,void* freeBorder);
void pager_mount(void* paddr,void* vaddr);
void* pager_getPage();
