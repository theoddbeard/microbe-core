#include "console.h"
#include "pager.h"
#include "gdt.h"
#include "idt.h"
#include "tss.h"
#include "pic.h"


void main(unsigned int memsz,void* freeMem){
	console_init(80,25);
	printfln("Starting MicrobeCore kernel...");
	printfln("Mem total=%d,free at %h",memsz,freeMem);
	pager_init((void*)memsz,freeMem);
	void* systables = pager_getPage();
	pager_mount(systables,(void*)0xFFFFF000);

	void* kernelStack = pager_getPage();
	pager_mount(kernelStack,(void*)0xEFBFE000);

	printf("System tables init:");
	gdt_init();
	printf("GDT ");
	idt_init();
	printf("IDT ");
	//tss_init();
	//tss_flush();
	printf("TSS ");
	pic_init();
	printf("PIC ");
	printfln("");
	__asm__("sti");
	printfln("Ok");
	__asm__("int $18");
	__asm__("int $16");
	int* a =(int*)0xc4000000;
	int b = *a;
	int c = *a;
	__asm__("int $18");
	for(;;);
}



void kmain(){

}


