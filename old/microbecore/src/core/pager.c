/*
 * pager.c
 *
 *  Created on: 04 февр. 2014 г.
 *      Author: theoddbeard
 */

#define CATALOG_MAIN_ADDR (void*)0xEFBFF000
#define CATALOG_3BF_ADDR (void*)0xEFBFF000
#define CATALOG_ATTRIB 0xF
#define CATALOGS_BASE (void*)0xEFC00000
#define PAGE_ATTRIB 0xF

typedef unsigned int catalogEntry;

catalogEntry* mainCatalog;
catalogEntry* cat3bf;

void* freeBorder;

void* pager_getPage(){
	return freeBorder+=0x1000;
}

void initCatalog(catalogEntry* catalog){
	for(unsigned i=0;i<1024;i++){
		catalog[i] = 0;
	}
}

void pager_mount(void* paddr,void* vaddr){
	unsigned int catNumber = (unsigned int)vaddr>>22;
	unsigned int entryNumber = (unsigned int)vaddr>>12&0x3FF;

	catalogEntry* catalog = CATALOGS_BASE+0x1000*catNumber;

	if ((mainCatalog[catNumber]&1)==0){
		catalogEntry* newCatalog = pager_getPage();
		mainCatalog[catNumber] = (unsigned int)newCatalog|CATALOG_ATTRIB;
		cat3bf[catNumber] = mainCatalog[catNumber];
	}

	catalog[entryNumber] = ((unsigned int)paddr&0xFFFFF000)|PAGE_ATTRIB;
}


void pager_init(void* upperBorder,void* freeMemBorder){
	__asm__("pushl %eax");
	__asm__("movl %cr3,%eax");
	__asm__("movl %eax,mainCatalog");
	__asm__("popl %eax");

	freeBorder = freeMemBorder;
	cat3bf = pager_getPage();
	catalogEntry* cat3be = pager_getPage();

	mainCatalog[0x3bf] = (catalogEntry)(((unsigned int)cat3bf&0xFFFFF000)|CATALOG_ATTRIB);
	mainCatalog[0x3be] = (catalogEntry)(((unsigned int)cat3be&0xFFFFF000)|CATALOG_ATTRIB);

	cat3bf[0x3bf] = (catalogEntry)(((unsigned int)cat3bf&0xFFFFF000)|CATALOG_ATTRIB);
	cat3bf[0x3be] = (catalogEntry)(((unsigned int)cat3be&0xFFFFF000)|CATALOG_ATTRIB);

	cat3be[0x3ff] = cat3bf[0x3bf] = (catalogEntry)(((unsigned int)mainCatalog&0xFFFFF000)|CATALOG_ATTRIB);

	for (int i=0;i<1024;i++){
		cat3bf[i] = mainCatalog[i];
	}

	mainCatalog = (catalogEntry*)CATALOG_MAIN_ADDR;
}







