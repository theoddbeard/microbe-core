#include "console.h"


void lpt_out(unsigned char data){
	__asm__("pushl %eax");
	__asm__("pushl %edx");



	__asm__("movw $0x378, %dx");
	__asm__("movb %0,%%al"::"b"(data));
	__asm__("outb %al,%dx");


	__asm__("movb $0x01,%ah");
	__asm__("movw $0x37A,%dx");
	__asm__("inb %dx,%al");
	__asm__("orb %ah,%al");
	__asm__("outb %al,%dx");

	__asm__("movb $0xFE,%ah");
	__asm__("inb %dx,%al");
	__asm__("andb %ah,%al");
	__asm__("outb %al,%dx");







	__asm__("popl %edx");
	__asm__("popl %eax");
}


void lpt_scan(){
	unsigned char lpt_old = 0;
	unsigned char lpt = 0;
	for (;;){
		__asm__("pushl %eax");
		__asm__("pushl %edx");
		__asm__("xorl %eax,%eax");
		__asm__("movw $0x379,%dx");
		__asm__("inb %dx,%al");
		__asm__("movb %%al,%0":"=b"(lpt):);
		__asm__("popl %edx");
		__asm__("popl %eax");
		if (lpt!=lpt_old)
		printf("lpt=%h",lpt);
		lpt_old = lpt;
	}

}


