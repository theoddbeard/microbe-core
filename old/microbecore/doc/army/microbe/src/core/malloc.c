/*
 * malloc.c
 *
 *  Created on: 08.01.2013
 *      Author: theoddbeard
 */
#include "console.h"

typedef struct {
	unsigned int size;
	void* next;
} Entry;

Entry* heap;
Entry* current;
unsigned int freememsz;

unsigned char semaphore;



void heap_init(void* memptr,unsigned int memsz){
	heap = (Entry*)memptr;
	heap->next = heap;
	heap->size = memsz-sizeof(Entry);

	current = heap;
	freememsz = memsz;
}


void* malloc(unsigned int memsz){
	while(semaphore){

	}
	semaphore = 1;
	Entry* ptr;
	Entry* prev;
	prev = current;
	if (current->next==0) return 0;
	for(ptr=(Entry*)prev->next; ;prev=ptr, ptr=(Entry*)ptr->next){
		if (ptr->size>=(memsz+(sizeof(Entry)*2))){
			//Entry* dirty = (Entry*)((unsigned int)ptr+(ptr->size-memsz+sizeof(Entry)));
			Entry* dirty = (Entry*)((unsigned int)ptr+sizeof(Entry)+ptr->size-memsz-sizeof(Entry));
			dirty->next = 0;
			dirty->size = memsz;
			ptr->size = ptr->size-(memsz)-sizeof(Entry);
			current = prev;
			freememsz-=memsz+sizeof(Entry);
			printfln("malloc at %h,size=%d",dirty,dirty->size);
			semaphore = 0;
			return (void*)((unsigned int)dirty+sizeof(Entry));
		}

		if ((ptr->size>=memsz)&&(ptr->size)<memsz+sizeof(Entry)){
			prev->next = ptr->next;
			ptr->next = 0;
			current = prev;
			freememsz-=ptr->size+sizeof(Entry);
			printfln("malloc at %h,size=%d",ptr,ptr->size);
			semaphore = 0;
			return (void*)((unsigned int)ptr+sizeof(Entry));
		}

		if (ptr==current) {
			semaphore = 0;
			return 0;
		}
	}
	semaphore = 0;
	return 0;
}

void mfree(void* memptr){
	while(semaphore){}
	semaphore = 1;
	Entry* ptr;
	Entry* dirtyptr = (Entry*)((unsigned int)memptr-sizeof(Entry));
	ptr = heap;
	if (heap->next==0) {
		dirtyptr->next = dirtyptr;
		heap = dirtyptr;
		current = heap;
		semaphore = 0;
		return;
	}

	void block_insert(Entry* block,Entry* prev){
		//printf("ins");
		block->next = prev->next;
		prev->next = block;
		/*
		printf("{blk=%h,s=%d,n=%h}",block,block->size,block->next);
		printf("{blk=%h,s=%d,n=%h}",prev,prev->size,prev->next);
*/
		if (((unsigned int)prev+sizeof(Entry)+prev->size)==(unsigned int)block){
			prev->next = block->next;
			prev->size += block->size+sizeof(Entry);
			block = prev;
		}

		if (((unsigned int)block+sizeof(Entry)+block->size)==(unsigned int)block->next){
			if (heap==block->next) heap=block;

	//		printf("[dfg:os=%d,ds=%d]",block->size,((Entry*)block->next)->size);
			block->size+=((Entry*)block->next)->size+sizeof(Entry);
			block->next = ((Entry*)block->next)->next;

		}
		//printf("{blk=%h,s=%d,n=%h}",block,block->size,block->next);
		//printf("{blk=%h,s=%d,n=%h}",prev,prev->size,prev->next);
	}

	//printf("search..");
	while(1){
		if (((unsigned int)dirtyptr<(unsigned int)ptr)&&((unsigned int)dirtyptr<(unsigned int)ptr->next)&&((unsigned int)ptr>=(unsigned int)ptr->next)){
			//printf("before..");
			block_insert(dirtyptr,ptr);
			semaphore = 0;
			return;

		}

		if (((unsigned int)dirtyptr>(unsigned int)ptr)&&((unsigned int)dirtyptr<(unsigned int)ptr->next)&&((unsigned int)ptr<=(unsigned int)ptr->next)){
			//printf("middle..");
			block_insert(dirtyptr,ptr);
			semaphore = 0;
			return;
		}

		if (((unsigned int)dirtyptr>(unsigned int)ptr)&&((unsigned int)dirtyptr>(unsigned int)ptr->next)&&((unsigned int)ptr>=(unsigned int)ptr->next)){
			//printf("after");
			block_insert(dirtyptr,ptr);
			semaphore = 0;
			return;
		}
		ptr = (Entry*)ptr->next;
	}

}

void heapstat(){
	Entry* ptr = heap;
	if (heap->next==0) return;
	do{
		printfln("memblk %h,size=%d,next=%h",ptr,ptr->size,ptr->next);
		ptr = (Entry*)ptr->next;
	}while(ptr!=heap);

}



