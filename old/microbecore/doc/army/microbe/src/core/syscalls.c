#include "idt.h"
#include "console.h"
//Int 0x50

//eax = function
//ebx = arg1
//ecx = arg2
//edx = arg3


/*
 * 0x1 alloc page
 * 0x2 get page
 * 0x3 free page
 * 0x4 create space
 * 0x5 send page to space
 * 0x6 destroy space
 * 0x7 create process
 * 0x8 destroy process
 */

//function 0xF0 - test syscalls
//function 0xF1 - fork
void syscall_int_handler();

__asm__("syscall_int_handler:");
__asm__("pusha");
//__asm__("movl %eax,%ecx");
__asm__("pushl %edx");
__asm__("pushl %ecx");
__asm__("pushl %ebx");
__asm__("pushl %eax");

__asm__("call syscall_dispatcher");
__asm__("addl $0x10,%esp");
__asm__("popa");
__asm__("iret");

void syscalls_init(){
	idt_set_handler_ring(0x50,IDT_GATE_TRAP,0x3,&syscall_int_handler);
}


void syscall_0xF0(unsigned int arg0,unsigned int arg1,unsigned int arg2);

void syscall_dispatcher(unsigned int number,unsigned int arg0,unsigned int arg1,unsigned int arg2){
	printfln("syscall %h,a1=%h,a2=%h,a3=%h,",number,arg0,arg1,arg2);
	//__asm__("hlt");
	switch(number){
		case 0xF0: syscall_0xF0(arg0,arg1,arg2);break;
	}
}

void syscall_0xF0(unsigned int arg0,unsigned int arg1,unsigned int arg2){
	console_outChar(arg0&0xFF);
}






