#include "mmap.h"
#include "console.h"
#include "idt.h"

#define PAGE_ATTR       0b000000001111



typedef char PagesMap;

typedef unsigned int pageEntry;

pageEntry* get_catalog_vaddr(unsigned short index);
unsigned char check_catalog_exists(unsigned short index);
void init_catalog(pageEntry* catalog);



PagesMap* pages_map;
unsigned int pages_map_size;
pageEntry* mainCatalog;
pageEntry* catalog_handler;

void* freeptr;

void pmap_init(unsigned int memsz,void* freePtr){
	freeptr = freePtr;
	mainCatalog = (pageEntry*)0xFEFFF000;
	catalog_handler = (pageEntry*)0xFEFFB000;

	unsigned int mmap_pages_need = ((memsz>>12)+0xFFF)>>12;
	//printfln("mmap_pages_need=%d",mmap_pages_need);

	void* mountaddr = (void*)0xFFEFC000;

	for (unsigned short i=0;i<mmap_pages_need;i++){
		unsigned int cat_index = (unsigned int)mountaddr>>22;
		unsigned int entry_index = ((unsigned int)mountaddr>>12)&0x3FF;
		char catalog_exisits = check_catalog_exists(cat_index);
		pageEntry* catalog = get_catalog_vaddr(cat_index);

		if (catalog_exisits){
			catalog[entry_index] = (unsigned int)freeptr|PAGE_ATTR;
		}
		//printfln("c=%d,e=%d,cat=0x%h,exists=%d",cat_index, entry_index,catalog,catalog_exisits);
		mountaddr+=0x1000;
		freeptr+=0x1000;
	}
	pages_map = (PagesMap*)0xFFEFC000;
	pages_map_size = memsz>>12;

	for(unsigned int i=0;i<pages_map_size;i++){
		if ((i<<12)<(unsigned int)freeptr){
			pages_map[i] = 1;
		} else {
			pages_map[i] = 0;
		}
	}

}

void* pmap_get_page(){
	for(unsigned int i=0;i<pages_map_size;i++){
		if (pages_map[i]==0){
			pages_map[i] = 1;
			return (void*)(i<<12);
		}
	}
	return 0x0;
}

void pmap_free_page(void* page){
	unsigned int index = (unsigned int)page>>12;
	pages_map[index] = 0;
}


pageEntry* get_catalog_vaddr(unsigned short index){
	if (index<1020){
		return (pageEntry*)(0xFEC00000+(0x1000*index));
	}
	else{
		return (pageEntry*)(0xFFFFC000+0x1000*(index-1020));
	}
}


void pmap_mount_page(void* paddr,void* vaddr){
	paddr = (void*)((unsigned int)paddr&0xFFFFF000);
	unsigned int cat_index = (unsigned int)vaddr>>22;
	unsigned int entry_index = ((unsigned int)vaddr>>12)&0x3FF;

	if (check_catalog_exists(cat_index)==0){
		printf("create catalog");
		pageEntry* new_catalog = pmap_get_page();
		//init_catalog(new_catalog);
		mainCatalog[cat_index] = (unsigned int)new_catalog|PAGE_ATTR;
		catalog_handler[cat_index] = (unsigned int)new_catalog|PAGE_ATTR;
		new_catalog = get_catalog_vaddr(cat_index);
		init_catalog(new_catalog);
	}

	//pageEntry* catalog = (pageEntry*)(mainCatalog[cat_index]&0xFFFFF000);
	pageEntry* catalog = get_catalog_vaddr(cat_index);
	catalog[entry_index] = (unsigned int)paddr|PAGE_ATTR;
}

unsigned char check_catalog_exists(unsigned short index){
	return mainCatalog[index]&1;
}

void init_catalog(pageEntry* catalog){
	for(short i=0;i<1024;i++){
		catalog[i] = 0x0;
	}
}


//Pager service

unsigned int get_page_entry(void* vaddr){
	unsigned int cat_index = (unsigned int)vaddr>>22;
	unsigned int entry_index = ((unsigned int)vaddr>>12)&0x3FF;
	return 0;
}

void page_not_found(unsigned int vaddr,unsigned int errcode){
	printf("err=%h,page=%h",errcode,vaddr);
	void* new_page = pmap_get_page();
	pmap_mount_page(new_page,(void*)vaddr);
	printfln(",fixed");
}

void int_0e();
__asm__("int_0e:");
__asm__("push %ebx");
__asm__("movl 4(%esp),%ebx");
__asm__("pusha");
__asm__("movl %cr2,%eax");
__asm__("pushl %ebx");
__asm__("pushl %eax");
__asm__("call page_not_found");
__asm__("add $8,%esp");
__asm__("popa");
__asm__("popl %ebx");
__asm__("addl $4,%esp");
__asm__("iret");


void pager_init(){
	idt_set_handler(0x0e,IDT_GATE_TRAP,&int_0e);
}






#define USER_ENABLE_MASK 0b100;
#define USER_DISABLE_MASK 0xFFFFFFFB
void pmap_catalog_enableToUser(unsigned short index){
	mainCatalog[index] |=USER_ENABLE_MASK;
}

void pmap_catalog_disableToUser(unsigned short index){
	mainCatalog[index] &= USER_DISABLE_MASK;

}

void space_init(){
	pmap_catalog_disableToUser(1023);
	pmap_catalog_disableToUser(1022);
	pmap_catalog_disableToUser(1021);
	pmap_catalog_disableToUser(1020);
	pmap_catalog_disableToUser(1019);

}






