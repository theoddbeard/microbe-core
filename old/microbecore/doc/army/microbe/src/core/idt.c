#include "offset.h"
#include "idt.h"
#include "console.h"

/*
Шлюзы прерываний и ловушек используются для вызова обработчиков пре-
рываний и исключений типа ловушки (см. раздел 10.5).
байты 7-6: биты 31 - 16 смещения (0 для 16-битных шлюзов и шлюза задачи)
байт 5: (байт доступа)
	бит 7 - бит присутствия сегмента
	биты 6-5: DPL - уровень привилегий дескриптора
	бит 4: О
	биты 3-0:'тип шлюза (4, 5, 6, 7, С, Е, F)
байт 4: биты 7-5: 000
		биты 4-0: 00000 или (для шлюза вызова) число двойных слов, которые будут скопированы из стека вызывающей задачи в стек вызываемой
байты 3-2: селектор сегмента
байты 1-0: биты 15-0 смещения (0 для шлюза задачи)


0 Зарезервированный тип

1 Свободный 16-битный TSS
2 Дескриптор таблицы LDT
3 Занятый 16-битный TSS
4 16-битный шлюз вызова
5 Шлюз задачи
6 16-битный шлюз прерывания
7 16-битный шлюз ловушки
8 Зарезервированный тип
9 Свободный 32-битный TSS
А Зарезервированный тип
В Занятый 32-битный TSS
С 32-битный шлюз вызова
D Зарезервированный тип
Е 32-битный шлюз прерывания
F 32-битный шлюз ловушки
*/








#pragma pack(1)
typedef struct IDT{
	unsigned char byte0;
	unsigned char byte1;
	unsigned char byte2;
	unsigned char byte3;
	unsigned char byte4;
	unsigned char byte5;
	unsigned char byte6;
	unsigned char byte7;
} IDT;

struct {
	unsigned short size;
	void* gdt_addr;
} LIDT_arg;

#pragma pack(0)


IDT* idt;
IDT idt_table[256];
#include "idt-handlers.inc"




void idt_init(){
	//idt = (IDT*)IDT_ADDR;
	idt = (IDT*)&idt_table;
	printfln("IDT_sz=%d",sizeof(IDT));
	for (int i=0;i<256;i++){
		idt[i].byte5 = 0;
	}
	LIDT_arg.gdt_addr = idt;
	LIDT_arg.size = 256*8-1;
	__asm__("lidt LIDT_arg");

	idt_set_handler(0,IDT_GATE_TRAP,int_0);
	idt_set_handler(1,IDT_GATE_TRAP,int_1);
	idt_set_handler(2,IDT_GATE_TRAP,int_2);
	idt_set_handler(3,IDT_GATE_TRAP,int_3);
	idt_set_handler(4,IDT_GATE_TRAP,int_4);
	idt_set_handler(5,IDT_GATE_TRAP,int_5);
	idt_set_handler(6,IDT_GATE_TRAP,int_6);
	idt_set_handler(7,IDT_GATE_TRAP,int_7);
	idt_set_handler(8,IDT_GATE_TRAP,int_8);
	idt_set_handler(9,IDT_GATE_TRAP,int_9);
	idt_set_handler(10,IDT_GATE_TRAP,int_10);
	idt_set_handler(11,IDT_GATE_TRAP,int_11);
	idt_set_handler(12,IDT_GATE_TRAP,int_12);
	idt_set_handler(13,IDT_GATE_TRAP,int_13);
	idt_set_handler(14,IDT_GATE_TRAP,int_14);
	idt_set_handler(15,IDT_GATE_TRAP,int_15);
	idt_set_handler(16,IDT_GATE_TRAP,int_16);
	idt_set_handler(17,IDT_GATE_TRAP,int_17);
	idt_set_handler(18,IDT_GATE_TRAP,int_18);

}

void idt_set_handler(unsigned int index,unsigned int type,void* addr){
	idt[index].byte7 = (unsigned int)addr>>24;
	idt[index].byte6 = ((unsigned int)addr>>16)&0xFF;
	idt[index].byte5 = (0b1110<<4)|(type&0xF);
	idt[index].byte4 = 0;
	idt[index].byte3 = 0;
	idt[index].byte2 = 8;
	idt[index].byte1 = ((unsigned int)addr>>8)&0xFF;
	idt[index].byte0 = (unsigned int)addr&0xFF;
}

void idt_set_handler_ring(unsigned int index,unsigned int type,unsigned char ring,void* addr){
	ring = (ring<<1)&0b110;
	printfln("ring=%h",ring);
	idt[index].byte7 = (unsigned int)addr>>24;
	idt[index].byte6 = ((unsigned int)addr>>16)&0xFF;
	idt[index].byte5 = ((0b1000|ring)<<4)|(type&0xF);
	idt[index].byte4 = 0;
	idt[index].byte3 = 0;
	idt[index].byte2 = 8;
	idt[index].byte1 = ((unsigned int)addr>>8)&0xFF;
	idt[index].byte0 = (unsigned int)addr&0xFF;
}




void int_handler(unsigned int intn,unsigned int errcode){
	__asm__("pusha");
	//printf("Int%h,e=%h",intn,errcode);
	//__asm__("hlt");
	__asm__("popa");
}




















