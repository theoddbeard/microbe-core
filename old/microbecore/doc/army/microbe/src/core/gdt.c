#include "gdt.h"
#include "offset.h"
#include "console.h"
#pragma pack(1)
typedef struct GDT{
	unsigned char byte0;
	unsigned char byte1;
	unsigned char byte2;
	unsigned char byte3;
	unsigned char byte4;
	unsigned char byte5;
	unsigned char byte6;
	unsigned char byte7;







} GDT;




struct LGDT{
	unsigned short size;
	void* gdt_addr;

} LGDT_arg;
#pragma pack(0)
GDT* gdt;
GDT gdt_table[20];


void gdt_init(){
	printfln("sz=%d",sizeof(GDT));
	//gdt = (GDT*)GDT_ADDR;
	gdt = (GDT*)&gdt_table;
	//Code 0 - segment 1
	gdt[1].byte7 = 0x00;
	gdt[1].byte6 = 0b11001111;
	gdt[1].byte5 = 0b10011010;
	gdt[1].byte4 = 0x00;
	gdt[1].byte3 = 0x00;
	gdt[1].byte2 = 0x00;
	gdt[1].byte1 = 0xFF;
	gdt[1].byte0 = 0xFF;


	//Data 0 -  segment 2
	gdt[2].byte7 = 0x00;
	gdt[2].byte6 = 0b11001111;
	gdt[2].byte5 = 0b10010010;
	gdt[2].byte4 = 0x00;
	gdt[2].byte3 = 0x00;
	gdt[2].byte2 = 0x00;
	gdt[2].byte1 = 0xFF;
	gdt[2].byte0 = 0xFF;

	//Stack 0 - segment 3 0x33
	gdt[3].byte7 = 0x00;
	gdt[3].byte6 = 0b11001111;
	gdt[3].byte5 = 0b10010110;
	gdt[3].byte4 = 0x00;
	gdt[3].byte3 = 0x00;
	gdt[3].byte2 = 0x00;
	gdt[3].byte1 = 0xFF;
	gdt[3].byte0 = 0xFF;


	//Code 3 - segment 4 0x23
	gdt[4].byte7 = 0x00;
	gdt[4].byte6 = 0b11001111;
	gdt[4].byte5 = 0b11111010;
	gdt[4].byte4 = 0x00;
	gdt[4].byte3 = 0x00;
	gdt[4].byte2 = 0x00;
	gdt[4].byte1 = 0xFF;
	gdt[4].byte0 = 0xFF;

	//Data 3 - segment 5 0x2B
	gdt[5].byte7 = 0x00;
	gdt[5].byte6 = 0b11001111;
	gdt[5].byte5 = 0b11110010;
	gdt[5].byte4 = 0x00;
	gdt[5].byte3 = 0x00;
	gdt[5].byte2 = 0x00;
	gdt[5].byte1 = 0xFF;
	gdt[5].byte0 = 0xFF;

	//Stack 3 - segment 6 0x33
	gdt[6].byte7 = 0x00;
	gdt[6].byte6 = 0b11001111;
	gdt[6].byte5 = 0b11110110;
	gdt[6].byte4 = 0x00;
	gdt[6].byte3 = 0x00;
	gdt[6].byte2 = 0x00;
	gdt[6].byte1 = 0xFF;
	gdt[6].byte0 = 0xFF;

	void* tss_addr;
	unsigned int tss_size;
	//TSS - segment 0x3B
	gdt[7].byte7 = 0xFF;
	gdt[7].byte6 = 0b01000000;
	gdt[7].byte5 = 0b11101001;
	gdt[7].byte4 = 0xEF;
	gdt[7].byte3 = 0xB0;
	gdt[7].byte2 = 0x00;
	gdt[7].byte1 = 0x00;
	gdt[7].byte0 = 0x68;



	LGDT_arg.gdt_addr = gdt;
	LGDT_arg.size = 63;
	__asm__("lgdt LGDT_arg ");


}




