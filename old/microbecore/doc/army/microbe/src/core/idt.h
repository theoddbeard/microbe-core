#define IDT_GATE_TRAP 0xF
#define IDT_GATE_INT 0xE
#define IDT_GATE_CALL 0xC

void idt_init();

void idt_set_handler(unsigned int index,unsigned int type,void* addr);
void idt_set_handler_ring(unsigned int index,unsigned int type,unsigned char ring,void* addr);
