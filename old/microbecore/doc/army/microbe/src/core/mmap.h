void pmap_init(unsigned int memsz,void* freePtr);
void pmap_mount_page(void* paddr,void* vaddr);
void* pmap_get_page();
void pmap_free_page(void* page);

void pager_init();
