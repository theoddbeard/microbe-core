#include "pic.h"
#include "idt.h"
#include "console.h"

void kbd_int_handler(unsigned short scancode){
	if (scancode<=0x76) printfln("%h",scancode);
}

void irq1();

__asm__("irq1:");
__asm__("pushl %eax");
__asm__("xorw %ax,%ax");
__asm__("inb $0x60,%al");
__asm__("pushw %ax");
__asm__("call kbd_int_handler");
__asm__("addl $2,%esp");
__asm__("popl %eax");
__asm__("iret");





void kbd_init(){
	idt_set_handler(0x21,IDT_GATE_INT,irq1);
	pic_enable_irq(0x1);


}



