
#include "console.h"
#include "mmap.h"
#include "gdt.h"
#include "idt.h"
#include "tss.h"
#include "pic.h"
#include "malloc.h"

#include "syscalls.h"

#include "keyboard.h"
#include "lpt.h"

typedef struct LCallPtr{
	void* addr;
	unsigned short segment;


}LCallPtr;

LCallPtr startup_ptr;





void panic();
void startup();

void ring3(){
	printfln("ring3");
	int* a = (int*)0xA0000000;
	*a = 10;


	for(;;);
}

void init(unsigned int memsz,void* memfree,void* callback){
	console_init(80,25);
	console_setColor(EGAWhite);

	unsigned int mmap_entry_need = memsz>>12;
	printfln("Starting core...");
	printfln("memsz=%d bytes (%d pages),mem_free at %h",memsz,(memsz>>12),memfree);
	printfln("callback at %h",callback);


	pmap_init(memsz,memfree);

	void* tables = pmap_get_page();
	pmap_mount_page(tables,(void*)0xFFEFB000);

	gdt_init();
	tss_init();
	tss_flush();

	idt_init();
	pager_init();

	pic_init();

	__asm__("sti");

	syscalls_init();

/*
	__asm__("pusha");
	__asm__("movl $0xF0,%eax");
	__asm__("movb 'S',%bl");
	__asm__("int $0x50");
	__asm__("popa");
*/
	//panic(__LINE__);



	//__asm__("cli");
	__asm__("movw $0x2B, %ax");
	__asm__("movw %ax,%ds");
	__asm__("movw %ax,%es");
	__asm__("movw %ax,%fs");
	__asm__("movw %ax,%gs");

	__asm__("movl %esp,%eax");
	__asm__("pushl $0x2B");
	__asm__("pushl %eax");
	__asm__("pushf");
	__asm__("movl %0,%%eax"::"l"(callback));
	__asm__("pushl $0x23");
	__asm__("pushl %eax");
	__asm__("iret");




	// Jump to startup

	//startup_ptr.addr = &startup;
	startup_ptr.addr = (void*)0xFF000104;
	startup_ptr.segment = 8;
	__asm__(" pushl %eax");
	__asm__(" movl $0x10, %eax");
	__asm__(" movw %ax,%ds");
	__asm__(" movw %ax, %ss");
	__asm__(" movw %ax, %es");
	__asm__(" movw %ax, %fs");
	__asm__(" movw %ax, %gs");
	__asm__(" popl %eax");
	__asm__("lcall startup_ptr");





	for(;;);
}




void startup(){
	//printfln("GDT");

	__asm__(" pushl %eax");
	__asm__(" movl $0x10, %eax");
	__asm__(" movw %ax,%ds");
	__asm__(" movw %ax, %ss");
	__asm__(" movw %ax, %es");
	__asm__(" movw %ax, %fs");
	__asm__(" movw %ax, %gs");
	__asm__(" popl %eax");



	__asm__("pushw $0x23");
	__asm__("pushl $0xff0000f0");

	__asm__("retf");

	panic(__LINE__);
}

void panic(int line){
	printf("Panic at %d",line);
    for (;;);

}
