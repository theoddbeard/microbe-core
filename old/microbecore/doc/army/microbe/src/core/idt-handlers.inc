#define INTHANDLER(num) __asm__("int_"#num":\r\n  pushl 0 \r\n pushl $"#num"\r\n call int_handler\r\n addl $8, %esp \r\n  iret");
#define INTHANDLER_ERRCODE(num) __asm__("int_"#num":\r\n pushl $"#num"\r\n call int_handler\r\n addl $8, %esp \r\n  \r\n iret");

INTHANDLER(0);
INTHANDLER(1);
INTHANDLER(2);
INTHANDLER(3);
INTHANDLER(4);
INTHANDLER(5);
INTHANDLER(6);
INTHANDLER(7);
INTHANDLER_ERRCODE(8);
INTHANDLER(9);
INTHANDLER_ERRCODE(10);
INTHANDLER_ERRCODE(11);
INTHANDLER_ERRCODE(12);
INTHANDLER_ERRCODE(13);
INTHANDLER_ERRCODE(14);
INTHANDLER(15);
INTHANDLER(16);
INTHANDLER_ERRCODE(17);
INTHANDLER(18);
INTHANDLER(19);
INTHANDLER(20);
INTHANDLER(21);
INTHANDLER(22);
INTHANDLER(23);
INTHANDLER(24);
INTHANDLER(25);
INTHANDLER(26);
INTHANDLER(27);
INTHANDLER(28);
INTHANDLER(29);
INTHANDLER(30);
INTHANDLER(31);

void int_0();
void int_1();
void int_2();
void int_3();
void int_4();
void int_5();
void int_6();
void int_7();
void int_8();
void int_9();
void int_10();
void int_11();
void int_12();
void int_13();
void int_14();
void int_15();
void int_16();
void int_17();
void int_18();
void int_19();
void int_20();
void int_21();
void int_22();
void int_23();
void int_24();
void int_25();
void int_26();
void int_27();
void int_28();
void int_29();
void int_30();
void int_31();
