typedef struct Process{
	unsigned int flags;
	unsigned int cr3;
	void* process_stack;
	unsigned int sp0;
	unsigned int sp3;
	void* parasite;
	void* next;
	void* prev;

} Process;

Process* process_current;
