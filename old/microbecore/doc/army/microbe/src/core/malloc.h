void heap_init(void* memptr,unsigned int memsz);
void* malloc(unsigned int memsz);
void mfree(void* memptr);
void heapstat();
