#include "elf.h"
#include "multiboot.h"
#include "console.h"
#include "pages.h"

typedef void(*func)(unsigned int memsz,void* freemem,void*);
unsigned int mem_total = 0;
void* freemem = 0;

void panic(unsigned int line){
    printfln("Panic at %d!",line);
    for (;;);
}
    

void memcpy(char* src,char* dest,unsigned int count){
	printfln("copy from %h to %h",src,dest);
	for (unsigned int i=0;i<count;i++){
		dest[i] = src[i];
	}
}


void loadModule(moduleInfo* module);
void loadElfModule(elfHeader* header);
void main();

pageEntry* mainCatalog;
void start(multibootInfo* mbInfo){
	console_init(80,25);
	console_setColor(EGAWhite);
	printfln("%s","Starting microbecore setup program...");

	mem_total = (mbInfo->mem_lower<<10)+(mbInfo->mem_upper<<10);

	printfln("Memory size = %dMb",(mem_total>>20)+1);

	moduleInfo* mods = mbInfo->mods_addr;


	moduleInfo* lastmod = &mods[mbInfo->mods_count-1];


	freemem = (void*)(((unsigned int)lastmod->mod_end-1)&0xFFFFF000)+0x1000;
	printfln("Last module ends at %h",lastmod->mod_end);
	printfln("Free memory begins at %h",freemem);

	printfln("Init page memory...");
        
        pages_init(freemem);
        
        pageEntry* cat_1023 = pages_get_free();
        pages_reset_catalog(cat_1023);
        pageEntry* cat_1022 = pages_get_free();
        pages_reset_catalog(cat_1022);
        pageEntry* cat_1021 = pages_get_free();
        pages_reset_catalog(cat_1021);
        pageEntry* cat_1020 = pages_get_free();
        pages_reset_catalog(cat_1020);
        pageEntry* cat_1019 = pages_get_free();
        pages_reset_catalog(cat_1019);
        
        mainCatalog = pages_get_maincatalog();
        mainCatalog[1019] = (unsigned int)cat_1019|PAGE_ATTR;
        mainCatalog[1020] = (unsigned int)cat_1020|PAGE_ATTR;
        mainCatalog[1021] = (unsigned int)cat_1021|PAGE_ATTR;
        mainCatalog[1022] = (unsigned int)cat_1022|PAGE_ATTR;
        mainCatalog[1023] = (unsigned int)cat_1023|PAGE_ATTR;
        
        cat_1019[1019] = (unsigned int)cat_1019|PAGE_ATTR;
        cat_1019[1023] = (unsigned int)mainCatalog|PAGE_ATTR;
        
        cat_1023[1020] = (unsigned int)cat_1020|PAGE_ATTR;
        cat_1023[1021] = (unsigned int)cat_1021|PAGE_ATTR;
        cat_1023[1022] = (unsigned int)cat_1022|PAGE_ATTR;
        cat_1023[1023] = (unsigned int)cat_1023|PAGE_ATTR;
        
        for (int i=0;i<4000;i++){
            void* addr = (void*)(i*0x1000);
            pages_mount(addr,addr);
        }
        
        moduleInfo* modules = mbInfo->mods_addr;
        
        loadModule(modules);
        
        
        elfHeader* coreModule = (elfHeader*)modules->mod_start;
        func entry = coreModule->e_entry;
        
        

        __asm__("pushl %eax");
        __asm__("movl mainCatalog, %eax");
        __asm__("movl %eax,%cr3");
        
        __asm__("movl %cr0,%eax");
        __asm__("orl $0x80000000,%eax");
        __asm__("movl %eax,%cr0");
        __asm__("popl %eax");
        
        pages_memstat();

        entry(mem_total,pages_get_free(),&main);
        panic(100);
        
        
}

void loadModule(moduleInfo* module){
	printfln("Loading module %s",module->cmdLine);
    loadElfModule((elfHeader*)module->mod_start);
}

void loadElfModule(elfHeader* elf){
    unsigned int base = (unsigned int)elf;

    printfln("offset=%h",base);
    printfln("entry=%h",elf->e_entry);

    elfProgHdr* progHdr = (elfProgHdr*)(base+elf->e_phoff);
    unsigned int progCount = elf->e_phnum;
    





    for (unsigned int i=0;i<progCount;i++){
    	printfln("Section %d,type=%h,addr=%h,filesz=%h,%vaddr=%h,memsz=%h",i,progHdr[i].p_type,progHdr[i].p_offset,progHdr[i].p_filesz,progHdr[i].p_vaddr,progHdr[i].p_memsz);
    	if (progHdr[i].p_type==PT_LOAD){
    		printfln("Loading section...");
    		unsigned int pages_need = ((progHdr[i].p_memsz+0xFFF)&0xFFFFF000)>>12;
    		unsigned int need_to_copy = progHdr[i].p_filesz;
    		void* dest_addr = progHdr[i].p_vaddr;
    		void* src_addr = (void*)((unsigned int)elf+progHdr[i].p_offset);

    		printfln("mem_pages_need=%h",pages_need);

    		for (unsigned int j=0;j<pages_need;j++){
    			char* page = (char*)pages_get_free();
    			pages_mount(page,dest_addr);
    			printfln("mount %h to %h",page,dest_addr);

    			if (need_to_copy>=0x1000){
    				memcpy(src_addr,page,0x1000);
    				src_addr+=0x1000;
    				need_to_copy-=0x1000;
    				dest_addr+=0x1000;
    			} else {
    				memcpy(src_addr,page,need_to_copy);
    				src_addr+=need_to_copy;
    				need_to_copy-=need_to_copy;
    				dest_addr+=need_to_copy;
    			}


    		}
    	}

    }
    
}

#include "syscalls.h"

void main(){
	//__asm__("movl $100,%eax");
	//__asm__("int $0x50");

	int* a = (int*)0xA0000000;
	*a = 10;
	syscall_f0('T');
	syscall_f0('P');
	for (;;);

}





