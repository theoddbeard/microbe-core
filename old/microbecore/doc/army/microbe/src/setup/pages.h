typedef unsigned int pageEntry;
/*
      биты 31 – 12: биты 31 – 12 физического адреса (таблицы страниц или самой страницы)
      биты 11 – 9: доступны для использования операционной системой
      бит 8: G — «глобальная страница» — страница не удаляется из буфера TLB при переключении задач или перезагрузке регистра CR3 (только на Pentium Pro, если установлен бит PGE регистра CR4)
      бит 7: PS — размер страницы. 1 — для страницы размером 2 или 4 мегабайта, иначе — 0
      бит 6: D — «грязная страница» — устанавливается в 1 при записи в страницу; всегда равен нулю для элементов каталога страниц
      бит 5: А — бит доступа (устанавливается в 1 при любом обращении к таблице страниц или отдельной странице)
      бит 4: PCD — бит запрещения кэширования
      бит 3: PWT — бит разрешения сквозной записи
      бит 2: U — страница/таблица доступна для программ с CPL = 3
      бит 1: W — страница/таблица доступна для записи
      бит 0: Р — страница/таблица присутствует. Если этот бит — 0, остальные биты элемента система может использовать по своему усмотрению, например, чтобы хранить информацию о том, где физически находится отсутствующая страница
 */


#define ATTR_CATALOG    0b000000001011
#define PAGE_ATTR       0b000000001111

void pages_init(void*);
void pages_reset_catalog(pageEntry* );
pageEntry* pages_get_free();
void pages_init_maincatalog();
pageEntry* pages_get_maincatalog();
void pages_mount(void* ,void* );
void pages_memstat();