	.file	"syscalls.c"
	.text
.globl syscall_f0
	.type	syscall_f0, @function
syscall_f0:
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%ebx
	subl	$4, %esp
	movl	8(%ebp), %eax
	movb	%al, -8(%ebp)
#APP
# 2 "syscalls.c" 1
	pushl %eax
# 0 "" 2
# 3 "syscalls.c" 1
	pushl %ebx
# 0 "" 2
# 4 "syscalls.c" 1
	pushl %ecx
# 0 "" 2
# 5 "syscalls.c" 1
	pushl %edx
# 0 "" 2
# 8 "syscalls.c" 1
	movl $0xCF, %edx
# 0 "" 2
# 9 "syscalls.c" 1
	movl $0xCE, %ecx
# 0 "" 2
# 10 "syscalls.c" 1
	xorl %ebx,%ebx
# 0 "" 2
#NO_APP
	movzbl	-8(%ebp), %eax
	movl	%eax, %ebx
#APP
# 11 "syscalls.c" 1
	movb %bl,%bl
# 0 "" 2
# 12 "syscalls.c" 1
	movl $0xF0,%eax
# 0 "" 2
# 13 "syscalls.c" 1
	int $0x50
# 0 "" 2
# 14 "syscalls.c" 1
	popl %edx
# 0 "" 2
# 15 "syscalls.c" 1
	popl %ecx
# 0 "" 2
# 16 "syscalls.c" 1
	popl %ebx
# 0 "" 2
# 17 "syscalls.c" 1
	popl %eax
# 0 "" 2
#NO_APP
	addl	$4, %esp
	popl	%ebx
	popl	%ebp
	ret
	.size	syscall_f0, .-syscall_f0
	.ident	"GCC: (Ubuntu 4.4.3-4ubuntu5) 4.4.3"
	.section	.note.GNU-stack,"",@progbits
