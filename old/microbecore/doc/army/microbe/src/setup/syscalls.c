void syscall_f0(char arg){
	__asm__("pushl %eax");
	__asm__("pushl %ebx");
	__asm__("pushl %ecx");
	__asm__("pushl %edx");


	__asm__("movl $0xCF, %edx");
	__asm__("movl $0xCE, %ecx");
	__asm__("xorl %ebx,%ebx");
	__asm__("movb %0,%%bl"::"b"(arg));
	__asm__("movl $0xF0,%eax");
	__asm__("int $0x50");
	__asm__("popl %edx");
	__asm__("popl %ecx");
	__asm__("popl %ebx");
	__asm__("popl %eax");
}


