#include "pages.h"
#include "console.h"



pageEntry* mainCatalog;
pageEntry* lastfree;

void pages_memstat(){
    printfln("Free page=%h",lastfree);
}

void pages_init(void* addr){
    lastfree = (pageEntry*)(((unsigned int)addr+0xFFF)&0xFFFFF000);
    printfln("First free page at %h",lastfree);
    pages_init_maincatalog();
    
}

void pages_reset_catalog(pageEntry* catalog){
    for (short i=0;i<1024;i++){
        catalog[i] = 0;
    }
}

pageEntry* pages_get_free(){
    pageEntry* result = lastfree;
    lastfree+=1024;
    //pages_memstat();
    return result;        
}

void pages_init_maincatalog(){
    mainCatalog = pages_get_free();
    pages_reset_catalog(mainCatalog);
}

pageEntry* pages_get_maincatalog(){
    return mainCatalog;
}

char pages_check_catalog(unsigned short index){
    return mainCatalog[index]&1;
}

void pages_mount(void* paddr,void* vaddr){
    paddr = (void*)((unsigned int)paddr&0xFFFFF000);
    vaddr = (void*)((unsigned int)vaddr&0xFFFFF000);
    unsigned short cat_index = (unsigned int)vaddr>>22;
    if (pages_check_catalog(cat_index)==0){        
        printfln("Creating new catalog...");
        pageEntry* new_catalog = pages_get_free();
        pages_reset_catalog(new_catalog);
        mainCatalog[cat_index] = (unsigned int)new_catalog|PAGE_ATTR;
    }
    pageEntry* catalog = (pageEntry*)(mainCatalog[cat_index]&0xFFFFF000);
    
    unsigned short entry_index = ((unsigned int)vaddr>>12)&0x3FF;

    //printfln("p=%h,v=%h,c=%h,e=%h",paddr,vaddr,cat_index,entry_index);
    catalog[entry_index] = (unsigned int)paddr|PAGE_ATTR;
}









