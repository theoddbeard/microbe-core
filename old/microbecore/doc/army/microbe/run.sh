#!/bin/sh
./mkimg.sh

CMD=start$1
echo $CMD
if [ $CMD = startqemu ]
then
    echo qemu
    qemu -m 32 -fda img/image.img 
else
    echo bochs
    bochs -q -f microbecore.conf
fi


