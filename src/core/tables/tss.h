/*
 * tss.h
 *
 *  Created on: 28 сент. 2016 г.
 *      Author: irokez
 */

#ifndef TABLES_TSS_H_
#define TABLES_TSS_H_
#include "../stdlib/stdlib.h"
void TSS_Init(void* __volatile__ tss_addr,void* __volatile__ kstack,void* __volatile__ esp);
uint32 TSS_GetSize();
void TSS_flush();


#endif /* TABLES_TSS_H_ */
