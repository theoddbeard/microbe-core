/*
 * tss.c
 *
 *  Created on: 28 сент. 2016 г.
 *      Author: irokez
 */
#include "tss.h"

typedef struct {
	uint32 previous_tss;
	uint32 ESP0;  //+4
	uint32 SS0;   //+8
	uint32 EPS1;  //+12
	uint32 SS1;   //+16
	uint32 ESP2;  //+20
	uint32 SS2;   //+24
	uint32 CR3;   //+28
	uint32 EIP;  //+32
	uint32 EFLASGS;//+36
	uint32 EAX;   //+40
	uint32 ECX;   //+44
	uint32 EDX;   //+48
	uint32 EBX;   //+52
	uint32 ESP;   //+56 (0x34)
	uint32 EBP;
	uint32 ESI;
	uint32 EDI;
	uint32 ES;
	uint32 CS;
	uint32 SS;
	uint32 DS;
	uint32 FS;
	uint32 GS;
	uint32 LDTR;
	uint32 tss_flags;
	uint32 iomap_base;
}   __attribute__ ((__packed__)) TSS;


void TSS_Init(void* __volatile__ tss_addr,void* __volatile__ kstack,void* __volatile__ esp){
	TSS* tss = (TSS*) tss_addr;
	tss->previous_tss = 0;
	tss->ESP0 = (uint32)kstack;
	tss->SS0 = 0x10;
	tss->SS = 0x23;
	tss->ESP = (uint32)esp;
	tss->iomap_base = sizeof(TSS);
}


uint32 TSS_GetSize(){
	return sizeof(TSS);
}

void TSS_flush(){
	asm("pushl %eax");
	asm("movw $0x2B,%ax");
	asm("ltr %ax");
	asm("popl %eax");
}



