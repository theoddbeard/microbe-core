/*
 * idt.h
 *
 *  Created on: 27 янв. 2016 г.
 *      Author: theoddbeard
 */

#ifndef TABLES_IDT_H_
#define TABLES_IDT_H_

#include "../stdlib/stdlib.h"

#define INT_HANDLER_INT 0xE
#define INT_HANDLER_TRAP 0xF

#define INTPARAMS unsigned int intn,unsigned int errcode,unsigned int addr,unsigned short segment, unsigned int addrE



typedef void(*IntHandler)(INTPARAMS);



void IDT_init();
void IDT_setHandler(unsigned char intNumber, unsigned short segment,void* handler,uint8 handlerType);
void IDT_setIntHandler(void* addr, uint32 intn);



#endif /* TABLES_IDT_H_ */
