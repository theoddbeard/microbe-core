/*
 * c
 *
 *  Created on: 28 сент. 2016 г.
 *      Author: sysadmin
 */
#include "../stdlib/stdlib.h"

#pragma pack(1)
typedef struct GdtEntry{
	uint8  byte0;
	uint8 byte1;
	uint8 byte2;
	uint8 byte3;
	uint8 byte4;
	uint8 byte5;
	uint8 byte6;
	uint8 byte7;
} __attribute__((packed)) GdtEntry;


struct{
	uint16 size;
	void* gdtAddt;
} LGDT_arg;

#pragma pack(0)

GdtEntry GDT[6]__attribute__ ((aligned (4096))) = {
		{
			.byte0 = 0,
			.byte1 = 0,
			.byte2 = 0,
			.byte3 = 0,
			.byte4 = 0,
			.byte5 = 0,
			.byte6 = 0,
			.byte7 = 0
		},

		{
			.byte0 = 0xFF,
			.byte1 = 0xFF,
			.byte2 = 0x00,
			.byte3 = 0x00,
			.byte4 = 0x00,
			.byte5 = 0b10011010,
			.byte6 = 0b11001111,
			.byte7 = 0x00
		},
		{
			//RING0 DATA 0x10
			.byte0 = 0xFF,
			.byte1 = 0xFF,
			.byte2 = 0x00,
			.byte3 = 0x00,
			.byte4 = 0x00,
			.byte5 = 0b10010010,
			.byte6 = 0b11001111,
			.byte7 = 0x00
		},
		{
			//RING3 CODE 0x1B

			.byte0 = 0xFF,
			.byte1 = 0xFF,
			.byte2 = 0x00,
			.byte3 = 0x00,
			.byte4 = 0x00,
			.byte5 = 0b11111010,
			.byte6 = 0b11001111,
			.byte7 = 0x00
		},
		{
			//RING3 DATA 0x23
			.byte0 = 0xFF,
			.byte1 = 0xFF,
			.byte2 = 0x00,
			.byte3 = 0x00,
			.byte4 = 0x00,
			.byte5 = 0b11110010,
			.byte6 = 0b11001111,
			.byte7 = 0x00
		}

};

void GDT_Init(){
	LGDT_arg.gdtAddt = &GDT;
	LGDT_arg.size = sizeof(GDT)-1;
	asm("lgdt LGDT_arg");
}

void GDT_AppendTSS(void* TSSaddr, uint32 TSSsize){

	//TSS Segment 0x2B
	GDT[5].byte7 = ((uint32)TSSaddr>>24)&0xFF;
	//GDT[5].byte6 = 0b10000000;
	GDT[5].byte6 = 0b01000000|((TSSsize>>16)&0xF);
	//GDT[5].byte5 = 0b11101001;
	GDT[5].byte5 = 0b11101001;
	GDT[5].byte4 = ((uint32)TSSaddr>>16)&0xFF;
	GDT[5].byte3 = ((uint32)TSSaddr>>8)&0xFF;
	GDT[5].byte2 = (uint32)TSSaddr&0xFF;
	GDT[5].byte1 = (TSSsize>>8)&0xFF;
	GDT[5].byte0 = (TSSsize)&0xFF;



}
