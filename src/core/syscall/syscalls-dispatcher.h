/*
 * syscalls-dispatcher.h
 *
 *  Created on: 11 окт. 2016 г.
 *      Author: sysadmin
 */

#ifndef SYSCALL_SYSCALLS_DISPATCHER_H_
#define SYSCALL_SYSCALLS_DISPATCHER_H_
#include "../stdlib/stdlib.h"

void* Syscall_GetIntHandler();

#endif /* SYSCALL_SYSCALLS_DISPATCHER_H_ */
