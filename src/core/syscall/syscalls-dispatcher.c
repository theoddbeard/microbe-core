/*
 * syscalls-dispatcher.c
 *
 *  Created on: 11 окт. 2016 г.
 *      Author: sysadmin
 */

#include "syscalls-dispatcher.h"
#include "syscalls.h"
#include "../console.h"
#include "../process/process.h"
#include "../process/process_carousel.h"
#include "../process/space.h"
#include "../memory/pager/pager.h"

typedef struct SyscallResult{
	unsigned int __volatile__ eax;
	unsigned int __volatile__ ebx;
	unsigned int __volatile__ ecx;
	unsigned int __volatile__ edx;
	unsigned int __volatile__ edi;
	unsigned int __volatile__ esi;
} SyscallResult;

void syscalls_int_handler();
__asm__("syscalls_int_handler:");
//__asm__("pusha");
__asm__("pushf");
__asm__("pushl %esi");
__asm__("pushl %edi");
__asm__("pushl %edx");
__asm__("pushl %ecx");
__asm__("pushl %ebx");
__asm__("pushl %eax");


__asm__("call syscalls_dispatcher");
__asm__("popl %eax");
__asm__("popl %ebx");
__asm__("popl %ecx");
__asm__("popl %edx");
__asm__("popl %edi");
__asm__("popl %esi");
__asm__("popf");
//__asm__("popa");
__asm__("iret");

void* Syscall_GetIntHandler(){
	return &syscalls_int_handler;
}
//SyscallResult __attribute__((noinline)) syscall_BAREBONE(void* entry, void* stack){
//	SyscallResult __volatile__ result = {
//					.eax = 0,
//					.ebx = 0,
//					.ecx = 0,
//					.edx = 0
//			};
//  return result;
//}

SyscallResult __attribute__((noinline)) syscall_kprintf(void* ptr){
	SyscallResult __volatile__ result = {
				.eax = 0,
				.ebx = 0,
				.ecx = 0,
				.edx = 0
		};
	printfln(ptr);
	return result;
}


SyscallResult __attribute__((noinline)) syscall_space_create(){
	SyscallResult __volatile__ result = {
					.eax = 0,
					.ebx = 0,
					.ecx = 0,
					.edx = 0
			};
	uint32 spaceId = Space_Create();
	result.eax = spaceId;
		return result;
}



SyscallResult __attribute__((noinline)) syscall_process_create(uint32 spaceId,void* entry, void* stack){
	SyscallResult __volatile__ result = {
					.eax = 0,
					.ebx = 0,
					.ecx = 0,
					.edx = 0
			};

	uint32 slot = Process_Create(spaceId, entry, stack);
	ProcessCarousel_Load(slot);
	result.eax = slot;
	return result;
}

SyscallResult __attribute__((noinline)) syscall_process_switch(uint32 spaceId){
	SyscallResult __volatile__ result = {
					.eax = 0,
					.ebx = 0,
					.ecx = 0,
					.edx = 0
			};

	Process_Switch(spaceId);

	return result;
}


SyscallResult __attribute__((noinline)) syscall_memory_alloc(void* target,uint32 c){
	SyscallResult __volatile__ result = {
					.eax = 0,
					.ebx = 0,
					.ecx = 0,
					.edx = 0
			};
	uint32 count = c>>12;
	uint32 i = 0;
	for(i=0;i<count;i++){
		void* page = Pager_GetPage();
		if (page!=0){
			Pager_Mount(page, target+(i*0x1000));
		} else {
			break;
		}

	}
	result.eax = i;
	return result;
}

SyscallResult __attribute__((noinline)) syscall_memory_release(void* target, uint32 count){
	SyscallResult __volatile__ result = {
					.eax = 0,
					.ebx = 0,
					.ecx = 0,
					.edx = 0
			};
	uint32 c = (count+0xFFF)>>12;
	uint32 i=0;
	for (i=0;i<c;i++){
		Pager_Release(target);
		target = (void*)((uint32)target+0x1000);
	}
	result.eax = i;
	return result;
}

SyscallResult __attribute__((noinline)) syscall_mem_mount(void* phys, void* dst,uint32 count){
	SyscallResult __volatile__ result = {
					.eax = 0,
					.ebx = 0,
					.ecx = 0,
					.edx = 0
			};
	uint32 c = (count+0xFFF)>>12;
	uint32 i = 0;
	void* sphys = (void*)((uint32)phys&0xFFFFF000);
	void* sdst = (void*)((uint32)dst&0xFFFFF000);
	uint32 pageIndex = (uint32)phys>>12;
	for (i=0;i<c;i++){
			if (Pager_GetPageByIndex(pageIndex)==1){
				printfln("!#");
				Pager_Mount(sphys, sdst);
			} else {
				printfln("!@");
			}
	}
	return result;
}






SyscallResult __attribute__((noinline)) syscall_mem_gift(uint32 dstSpaceId, void* src, void* dst, uint32 count){
	SyscallResult __volatile__ result = {
					.eax = 0,
					.ebx = 0,
					.ecx = 0,
					.edx = 0
			};


	if(Space_CheckForAccess(dstSpaceId)){
		uint32 c = (count+0xFFF)>>12;
		printf("src=%h,dst=%h,cnt=%h=%h",src,dst,count,c);
		Space_Memory_Gift(dstSpaceId, src, dst, c);
		printfln(".");
		result.eax = 0xDEADC0DE;
	} else {
		result.eax = 0;
	}

	return result;
}
void __attribute__((noinline)) __attribute__((optimize("-fno-omit-frame-pointer")))
out_vga(uint16 iport, uint16 vport, uint16 index, uint16 value){
	asm("pushl %eax");
	asm("pushl %edx");
	asm("xor %ax,%ax");
	asm("xor %dx,%dx");
	asm("movw %0, %%dx"::"m"(iport));
	asm("movb %0, %%al"::"m"(index));
	asm("outw %ax, %dx");

	asm("xor %ax,%ax");
	asm("xor %dx,%dx");
	asm("movw %0, %%dx"::"m"(vport));
	asm("movb %0, %%al"::"m"(value));
	asm("outw %ax, %dx");

	asm("popl %edx");
	asm("popl %eax");
}

void __attribute__((noinline)) __attribute__((optimize("-fno-omit-frame-pointer")))
out_vga_port(uint16 port, uint16 value){
	asm("pushl %eax");
	asm("pushl %edx");
	asm("xor %ax,%ax");
	asm("xor %dx,%dx");
	asm("movw %0, %%dx"::"m"(port));
	asm("movb %0, %%al"::"m"(value));
	asm("outw %ax, %dx");
	asm("popl %edx");
	asm("popl %eax");

}
SyscallResult __attribute__((noinline)) syscall_TEST_VGA(){
	SyscallResult __volatile__ result = {
					.eax = 0,
					.ebx = 0,
					.ecx = 0,
					.edx = 0
			};

	/*
	 * контроллера атрибутов (03C0h – 03C1h)
	 * графического контроллера (03CEh – 03CFH)
	 * CRT (03D4h – 03D5H)
	 *
	 */
	//w_360
	out_vga(0x3d4, 0x3d5, 0x11, 0xe);
	out_vga(0x3d4, 0x3d5, 0x0,  0x5f);
	out_vga(0x3d4, 0x3d5, 0x1,  0x4f);
	out_vga(0x3d4, 0x3d5, 0x2,  0x50);
	out_vga(0x3d4, 0x3d5, 0x3,  0x82);
	out_vga(0x3d4, 0x3d5, 0x4,  0x54);
	out_vga(0x3d4, 0x3d5, 0x5,  0x80);
	out_vga(0x3d4, 0x3d5, 0x13, 0x28);
	//h_200
	out_vga(0x3d4, 0x3d5, 0x6, 0xbf);
	out_vga(0x3d4, 0x3d5, 0x7, 0x1f);
	out_vga(0x3d4, 0x3d5, 0x10,0x9c);
	out_vga(0x3d4, 0x3d5, 0x11,0x0e);
	out_vga(0x3d4, 0x3d5, 0x12,0x8f);
	out_vga(0x3d4, 0x3d5, 0x15, 0x96);
	out_vga(0x3d4, 0x3d5, 0x16, 0xb9);
	//out_vga(0x3d4, 0x3d5, 0x13, 0x28);

	out_vga(0x3ce, 0x3cf, 0x5, 0b0100010);
	out_vga(0x3ce, 0x3cf, 0x6, 0b1101);
/*
	out_vga(0x3d4, 0x3d5, 0x0, 0xFF);
	out_vga(0x3d4, 0x3d5, 0x1, 0xFF);
	out_vga(0x3d4, 0x3d5, 0x6, 320&0b00111111);
	out_vga(0x3d4, 0x3d5, 0x12, 200);
	out_vga(0x3d4, 0x3d5, 0x13, 0xFF);
	out_vga(0x3d4, 0x3d5, 0x17, 0b0000000);
*/


//	asm("pushl %eax");
//	asm("pushl %edx");
//
//
//	asm("movw $0x3ce, %dx");
//	asm("movb $0b00000001, %ah");
//	asm("movb $0x6,%al");
//	asm("outw %ax, %dx");
//
//	asm("movw $0x3d4, %dx");
//	asm("movb $0x0, %al");
//
//	asm("outb %al, %dx ");
//	asm("movw $0x3d5, %dx");
//	asm("movw $320, %ax");
//	asm("outw %ax,%dx");
//
//
//
//	asm("popl %edx");
//	asm("popl %eax");

	return result;
}


void __attribute__((cdecl)) syscalls_dispatcher(uint32 __volatile__ eax,uint32 __volatile__ ebx, uint32 __volatile__ ecx, uint32 __volatile__ edx, uint32 __volatile__ edi, uint32 __volatile__ esi){
	SyscallResult __volatile__ result = {
				.eax = 0,
				.ebx = 0,
				.ecx = 0,
				.edx = 0,
				.edi = 0,
				.esi = 0
	};

	switch(eax){
	case 0x00000001:result = syscall_kprintf((void*)ebx);break;
	case SYSCALL_SPACE_CREATE:result = syscall_space_create();break;
	case SYSCALL_PROCESS_CREATE:result = syscall_process_create(ebx,(void*)ecx, (void*)edx);break;
	case SYSCALL_PROCESS_SWITCH:result = syscall_process_switch(ebx);break;
	case SYSCALL_MEMORY_ALLOC:result = syscall_memory_alloc((void*)ebx, ecx);break;
	case SYSCALL_MEMORY_GIFT:result = syscall_mem_gift(ebx, (void*)ecx, (void*)edx, edi);break;
	case SYSCALL_MEMORY_RELEASE:result = syscall_memory_release((void*) ebx, ecx);break;
	case SYSCALL_MEMORY_MOUNT:result = syscall_mem_mount((void*)ebx, (void*)ecx, edx);break;
	//case SYSCALL_TEST_VGA:result = syscall_TEST_VGA();break;
	default:printfln("Syscall %h,%h,%h,%h",eax,ebx,ecx,edx);break;
	}

	eax = result.eax;
	ebx = result.ebx;
	ecx = result.ecx;
	edx = result.edx;
	edi = result.edi;
	esi = result.esi;
	return;
}
