/*
 * space.c
 *
 *  Created on: 3 окт. 2016 г.
 *      Author: irokez
 */

#include "space.h"
#include "../memory/pager/pager.h"
#include "../memory/malloc.h"
#include "../memory-map.h"
#include "../config.h"
#include "../console.h"


typedef Space* PSpace;


PSpace spaces[MAX_SPACES];

Space* Space_Get(uint32 slot){
	return spaces[slot];
}
uint32 findSpaceSlot(){
	for (uint32 i=1;i<MAX_SPACES;i++){
		if (spaces[i]==NULL){
			spaces[i] = (void*)1;
			return i;
		}
	}
	return 0;
}



void Space_Init(){
	for (uint16 i=0;i<MAX_SPACES;i++){
		spaces[i] = NULL;
	}

	Space* space = malloc(16);
	space->cr3 = Pager_GetCR3();

	uint32* spaceCtxPage = Pager_GetPage();
	Pager_Mount(spaceCtxPage, SPACE_CONTEXT);
	CurrentSpaceCtx->id = 0;
	CurrentSpaceCtx->parent = -1;

	spaces[0] = space;
	printf("SA=%h",space);
}


uint32 Space_Create(){
	uint32* cr3 = Pager_GetPage();
	uint32* cat3ff = Pager_GetPage();
	uint32* cat3fe = Pager_GetPage();
	uint32* ctxPage = Pager_GetPage();

	Pager_Mount(cr3, PROCESS_FOREIGN_CR3);
	Pager_Mount(cat3ff, PROCESS_FOREIGN_3FF);
	Pager_Mount(cat3fe, PROCESS_FOREIGN_3FE);
	Pager_Mount(ctxPage, PROCESS_FOREIGN_SPACE_CONTEXT);

	Pager_InitRoot(PROCESS_FOREIGN_CR3,PROCESS_FOREIGN_3FF,(uint32)cat3ff);

	Pager_SetRootItem(PROCESS_FOREIGN_CR3, PROCESS_FOREIGN_3FF, cat3fe, 0x3fe);

	Pager_SetPageItem(PROCESS_FOREIGN_3FE, cr3, 0x3ff);
	Pager_SetPageItem(PROCESS_FOREIGN_3FE, ctxPage, 0);


	uint32* f = (uint32*)PROCESS_FOREIGN_CR3;
	printfln("Space cr3=%h,3ff=%h,%h,3fe=%h",cr3,cat3ff,f[0x3ff],cat3fe);
	Space* space = malloc(sizeof(Space));
	space->cr3 = cr3;
	space->cat3ff = cat3ff;
	space->cat3fe = cat3fe;
	uint32 slot = findSpaceSlot();
	spaces[slot] = space;

	SpaceContext* spaceCtx = PROCESS_FOREIGN_SPACE_CONTEXT;

	spaceCtx->id = slot;
	spaceCtx->parent = CurrentSpaceCtx->id;
	space->parent = CurrentSpaceCtx->id;

	Pager_Unmount(PROCESS_FOREIGN_CR3);
	Pager_Unmount(PROCESS_FOREIGN_3FF);
	Pager_Unmount(PROCESS_FOREIGN_3FE);
	Pager_Unmount(PROCESS_FOREIGN_SPACE_CONTEXT);
	console_disableScreen();
	return slot;
}


bool Space_CheckForAccess(uint32 dstSpaceId){
	Space* space = spaces[dstSpaceId];
	if (space==NULL||space==(void*)1){
		return false;
	}
//	printf("Check dst space %h,p=%h,c=%h",dstSpaceId,space->parent,CurrentSpaceCtx->id);
	if (space->parent==CurrentSpaceCtx->id){
		return true;
	}
	return false;
}

void Space_Memory_Gift(uint32 dstSpaceId,void* src, void* dst, uint32 pagesCount){
	Space* space = spaces[dstSpaceId];
	Pager_Mount(space->cr3, PROCESS_FOREIGN_CR3);
	Pager_Mount(space->cat3ff,PROCESS_FOREIGN_3FF);
	src = (void*)((uint32)src&0xFFFFF000);
	dst = (void*)((uint32)dst&0xFFFFF000);
	for (uint32 i=0;i<pagesCount;i++){
		Pager_MountForeign(PROCESS_FOREIGN_CR3, PROCESS_FOREIGN_3FF, src, dst);
		src = (void*)((uint32)src+0x1000);
		dst = (void*)((uint32)dst+0x1000);
	}
	Pager_Unmount(PROCESS_FOREIGN_CR3);
	Pager_Unmount(PROCESS_FOREIGN_3FF);
	Pager_Unmount(PROCESS_FOREIGN_CATALOG);

}






