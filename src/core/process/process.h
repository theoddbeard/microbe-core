/*
 * process.h
 *
 *  Created on: 3 окт. 2016 г.
 *      Author: irokez
 */

#ifndef PROCESS_PROCESS_H_
#define PROCESS_PROCESS_H_
#include "../stdlib/stdlib.h"





void Process_Init();
uint32 Process_Create(uint32 spaceId,void* entry, void* stack);
void Process_Switch(uint32 spaceId);
#endif /* PROCESS_PROCESS_H_ */
