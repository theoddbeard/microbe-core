/*
 * process_carousel.h
 *
 *  Created on: 4 окт. 2016 г.
 *      Author: sysadmin
 */

#ifndef PROCESS_PROCESS_CAROUSEL_H_
#define PROCESS_PROCESS_CAROUSEL_H_
#include "../stdlib/stdlib.h"
void ProcessCarousel_Init(uint32 firstProcess);
void ProcessCarousel_Load(uint32 processId);
void ProcessCarousel_Switch();


#endif /* PROCESS_PROCESS_CAROUSEL_H_ */
