/*
 * space.h
 *
 *  Created on: 3 окт. 2016 г.
 *      Author: irokez
 */

#ifndef PROCESS_SPACE_H_
#define PROCESS_SPACE_H_
#include "../stdlib/stdlib.h"

typedef struct{
	uint32 id;
	uint32 parent;
} SpaceContext;

typedef struct Space{
	uint32* __volatile__ cr3;
	uint32* __volatile__ cat3fe;
	uint32* __volatile__ cat3ff;
	uint32 __volatile__ parent;

} Space;

#define CurrentSpaceCtx ((SpaceContext*)(SPACE_CONTEXT))

void Space_Init();
uint32 Space_Create();
bool Space_CheckForAccess(uint32 dstSpaceId);
Space* Space_Get(uint32 slot);
void Space_Memory_Gift(uint32 dstSpaceId,void* src, void* dst, uint32 pagesCount);

#endif /* PROCESS_SPACE_H_ */
