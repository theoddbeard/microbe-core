/*
 * process_carousel.c
 *
 *  Created on: 4 окт. 2016 г.
 *      Author: sysadmin
 */

#include "process_carousel.h"
#include "../config.h"
#include "process.h"
#include "../memory/malloc.h"
#include "../console.h"

typedef struct ProcessCarouselItem{
	uint32 __volatile__ processId;
	uint32 __volatile__ processState;
	void* __volatile__ next;
	void* __volatile__ prev;

} ProcessCarouselItem;

ProcessCarouselItem* first;
ProcessCarouselItem* last;
ProcessCarouselItem* current;

ProcessCarouselItem* __volatile__ carousel[MAX_PROCESSES];

uint32 __volatile__ processCount = 0;
uint32 __volatile__ currentProcess = 0;


void INT_0_HANDLER();
ASM("INT_O_HANDLER:");

ASM("iret");

uint32 findSlot(){
	for(uint32 i=1;i<MAX_PROCESSES;i++){
		if (carousel[i]==NULL){
			carousel[i] = (void*)1;
			return i;
		}
	}
	return 0;
}

void ProcessCarousel_Init(uint32 firstProcess){
	for(uint32 i=0;i<MAX_PROCESSES;i++){
		carousel[i] = NULL;
	}

	ProcessCarouselItem* item = malloc(sizeof(ProcessCarouselItem));
	item->processId = firstProcess;
	item->next = item;
	item->prev = item;
	carousel[0] = item;
	currentProcess = 0;
	processCount=1;

	first = item;
	last = item;
	current = item;
}

void ProcessCarousel_Load(uint32 processId){
	uint32 slot=findSlot();
	if (slot==0){
		printf("SlotNF");
		return;
	}

	ProcessCarouselItem* item = malloc(sizeof(ProcessCarouselItem));
	item->processId = processId;
	item->prev = last;
	item->next = first;
	last->next = item;
	last = item;
	carousel[slot] = item;
	printf("CSlot=%h:%h,%h",carousel[slot],slot,item);
	processCount=processCount+1;
}

void ProcessCarousel_Switch(){
	ProcessCarouselItem* next = current->next;
	if (next!=current){
		current = next;
		Process_Switch(current->processId);
	}

	return;
//	return;
	if (processCount<2){
		//printf("&");
		return;
	}
	uint32 __volatile__ proc = currentProcess;
	while(true){
		asm("cli");
		proc = proc+1;
		if (proc>=MAX_PROCESSES){
			//printf("@");
			proc = 0;
		}

		if (proc==currentProcess){
			//printf("^");
			return;
		}
		if (carousel[proc]==NULL||carousel[proc]==(void*)1){
			//printf("*%h:%d",carousel[proc],proc);
			continue;
		}

		ProcessCarouselItem* __volatile__ process = carousel[proc];
		currentProcess = proc;
		Process_Switch(process->processId);
//		asm("sti");
		break;
	}
}




