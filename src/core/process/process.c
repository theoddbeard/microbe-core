/*
 * process.c
 *
 *  Created on: 3 окт. 2016 г.
 *      Author: irokez
 */

#include "../memory-map.h"
#include "process.h"
#include "../memory/malloc.h"
#include "../memory/pager/pager.h"
#include "../tables/tss.h"
#include "../console.h"
#include "../config.h"
#include "space.h"

typedef struct Process {
	uint32 cat3fd;
	uint32 spaceId;
	uint32 stack;
	uint32 align[1];
} Process;

typedef struct process_init_stack{
//	uint32 __volatile__ eax;
//	uint32 __volatile__ ebx;
//	uint32 __volatile__ ecx;
//	uint32 __volatile__ edx;
//
//	uint32 __volatile__ esi;
//	uint32 __volatile__ edi;
//	uint32 __volatile__ ebp;
//	uint32 __volatile__ eflags;

	uint32 __volatile__ ds;
	uint32 __volatile__ es;
	uint32 __volatile__ fs;
	uint32 __volatile__ gs;

	uint32 __volatile__ init_pointer;

	uint32 __volatile__ iret_eip;
	uint32 __volatile__ iret_cs;
	uint32 __volatile__ iret_eflags;
	uint32 __volatile__ iret_esp;
	uint32 __volatile__ iret_ss;

} __attribute__ ((__packed__)) process_init_stack;




Process* __volatile__ processes[MAX_PROCESSES];



void Process_Init(){
	for (uint16 i=0;i<MAX_PROCESSES;i++){
		processes[i] = NULL;
	}

	Process* proc = malloc(sizeof(Process));
	proc->spaceId = 0;
	proc->cat3fd = Pager_Get3FD();

	processes[0] = proc;


}



uint32 findFreeSlot(){
	for (uint32 i=1;i<MAX_PROCESSES;i++){
		if (processes[i]==NULL){
			return i;
		}
	}
	return 0;
}

void process_init_stub();
ASM("process_init_stub:");
//ASM("movl $0x1234ABCD, %eax");
ASM("sti");
ASM("iret");

uint32
__attribute__((noinline))
__attribute__((optimize("-fno-omit-frame-pointer")))
printEflags(){
		uint32 __volatile__  eflags = 0;
		asm("pushl %eax");
		asm("pushf");
		asm("popl %eax");
		asm("movl %%eax,%0":"=m"(eflags));
		asm("popl %eax");
		//printfln("Eflags=%h",eflags);
		return eflags;
}
uint32 Process_Create(uint32 spaceId,void* entry, void* stack){
	if (spaceId==0){
		spaceId = CurrentSpaceCtx->id;
	}
	Process* proc = malloc(sizeof(Process));
	proc->spaceId = spaceId;
	proc->cat3fd = (uint32)Pager_GetPage();

	void* kstack = Pager_GetPage();
	void* tss = Pager_GetPage();
//	void* tss2 = Pager_GetPage();


	Pager_Mount(tss, PROCESS_FOREIGN_TSS);
	Pager_Mount(kstack, PROCESS_FOREIGN_KSTACK);

	//Init TSS
	//TSS_Init(PROCESS_FOREIGN_TSS, PROCESS_KERNEL_STACK_TOP);

	//Init KStack

	uint32  eflags = 0;
	eflags = printEflags();
	//printfln("Eflags=%h,kstack=%h,tss=%h",eflags,kstack,tss);
	eflags = eflags&0xFFFFFDFF;

	process_init_stack* __volatile__ procInitStack = (void*)((uint32)(PROCESS_FOREIGN_KSTACK_TOP)-sizeof(process_init_stack)-8);
	void* __volatile__ procInitStackAddr = (void*)((uint32)(PROCESS_KERNEL_STACK_TOP)-sizeof(process_init_stack)-8);
	printfln("InitStack=%h,%h",procInitStack,procInitStackAddr);
//	procInitStack->eax = 0x1234ABCD;
//	procInitStack->ebx = 0;
//	procInitStack->ecx = 0;
//	procInitStack->edx = 0;
//
//	procInitStack->esi = 0;
//	procInitStack->edi = 0;
//	procInitStack->ebp = 0;
//	procInitStack->eflags = eflags;

	procInitStack->ds = 0x23;
	procInitStack->es = 0x23;
	procInitStack->fs = 0x23;
	procInitStack->gs = 0x23;

	eflags = eflags|0x200;
	//eflags = eflags&0xFFFFFDFF;

	procInitStack->init_pointer = (uint32)(&process_init_stub);
	procInitStack->iret_eip = (uint32)entry;
	procInitStack->iret_cs = 0x1b;
	procInitStack->iret_eflags = eflags;
	procInitStack->iret_esp = (uint32)stack;
	procInitStack->iret_ss = 0x23;

 /*
  *
  *
  *
  *
  *
  */

	TSS_Init(PROCESS_FOREIGN_TSS, PROCESS_KERNEL_STACK_TOP,(void*)procInitStackAddr);

	Pager_Mount((void*)proc->cat3fd,PROCESS_FOREIGN_3FD);
	Pager_SetPageItem(PROCESS_FOREIGN_3FD, kstack,0x2);
	Pager_SetPageItem(PROCESS_FOREIGN_3FD, tss, 0x0);
//	Pager_SetPageItem(PROCESS_FOREIGN_3FD, tss2, 0x1);
	Pager_Unmount(PROCESS_FOREIGN_3FD);
	Pager_Unmount(PROCESS_FOREIGN_TSS);
	Pager_Unmount(PROCESS_FOREIGN_KSTACK);

	uint32 slot = findFreeSlot();
	processes[slot] = proc;
	return slot;
}

ASM("PROCESS_SWITCH:");
//procInitStack->eax = 0;
//procInitStack->ebx = 0;
//procInitStack->ecx = 0;
//procInitStack->edx = 0;
//
//procInitStack->esi = 0;
//procInitStack->edi = 0;
//procInitStack->ebp = 0;
//procInitStack->eflags = eflags;
//
//procInitStack->ds = 0;
//procInitStack->es = 0;
//procInitStack->fs = 0;
//procInitStack->gs = 0;

//SAVE REGISTERS
ASM("cli");
ASM("pushl %gs");
ASM("pushl %fs");
ASM("pushl %es");
ASM("pushl %ds");

//ASM("pushfl");
//ASM("pushl %ebp");
//ASM("pushl %edi");
//ASM("pushl %esi");
//
//ASM("pushl %edx");
//ASM("pushl %ecx");
//ASM("pushl %ebx");
//ASM("pushl %eax");


//SAVE ESP0 TO TSS
ASM("movl %esp, 0xFF400038");

//ASM("movl %cr3, %eax");
//ASM("orl $0x17, %eax");
ASM("movl %eax, %cr3");

//SWITCH 3FD
ASM("orl $0x17,%ebx");
ASM("movl %ebx,0xFFFFFFF4");
ASM("movl %ebx,0xFFBFFFF4");

//ASM("movl %cr3 ,%eax");
ASM("movl %eax, %cr3");

//
//ASM("pushl %eax");
//ASM("movw $0x2B,%ax");
//ASM("ltr %ax");
//ASM("popl %eax");


//RESTORE ESP0 FROM TSS
ASM("movl 0xFF400038, %esp");


//RESTORE REGISTERS
//ASM("popl %eax");
//ASM("popl %ebx");
//ASM("popl %ecx");
//ASM("popl %edx");
//
//ASM("popl %esi");
//ASM("popl %edi");
//ASM("popl %ebp");
//ASM("popfl");



ASM("popl %ds");
ASM("popl %es");
ASM("popl %fs");
ASM("popl %gs");




//ASM("sti");
ASM("ret");

void Process_Switch(uint32 processId){
	Process* proc = processes[processId];
	Space* space = Space_Get(proc->spaceId);
	//process_init_stack* initStack = (void*)((uint32)(PROCESS_FOREIGN_KSTACK_TOP)-sizeof(process_init_stack));
	//printfln("Switch to process %h,3fd=%h,cr3=%h",processId,proc->cat3fd,space->cr3);
	asm("pushl %eax");
	asm("pushl %ebx");
	asm("addl $0x8, %esp");
	asm("movl %0,%%eax"::"m"(space->cr3));
	asm("movl %0,%%ebx"::"m"(proc->cat3fd));
	asm("subl $0x8, %esp");

	asm("call PROCESS_SWITCH");

	asm("popl %ebx");
	asm("popl %eax");

}

