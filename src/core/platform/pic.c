/*
 * pic.c
 *
 *  Created on: 29 сент. 2016 г.
 *      Author: sysadmin
 */
#include "pic.h"


void PIC_Init(){
	asm("pushl %eax");
	//ICW1
	asm("movb $0b00010101,%al");
	asm("outb %al,$0x20");
	asm("outb %al,$0xA0");
	//ICW2
	asm("movb $0x20,%al");
	asm("outb %al,$0x21");
	asm("movb $0x28,%al");
	asm("outb %al,$0xA1");

	//ICW3
	asm("movb $0x4, %al");
	asm("outb %al,$0x21");
	asm("movb $0x4, %al");
	asm("outb %al,$0xA1");
	//ICW4
	asm("movb $0b00011111, %al");
	asm("outb %al,$0x21");
	asm("movb $0b00011011, %al");
	asm("outb %al,$0x21");

	//Disable IRQ
	asm("movb $0b11111111,%al");
	asm("outb %al,$0x21");
	asm("movb $0b11111111,%al");
	asm("outb %al,$0xA1");

	asm("popl %eax");
}


void PIC_EnableIrq(uint16 index){
	if (index<8){
		uint8 mask = ~(0x1<<index);
		asm("pushl %eax");
		asm("inb $0x21,%al");
		asm("movb %0,%%ah"::"b"(mask));
		asm("andb %ah,%al");
		asm("outb %al,$0x21");
		asm("popl %eax");

	} else {
		uint8 mask = ~(0x1<<(index-8));
		asm("pushl %eax");
		asm("inb $0xA1,%al");
		asm("movb %0,%%ah"::"b"(mask));
		asm("andb %ah,%al");
		asm("outb %al,$0xA1");
		asm("popl %eax");
	}
}

void PIC_DisableIrq(uint16 index){
	if (index<8){
		uint8 mask = 0x1<<index;
		asm("pushl %eax");
		asm("inb $0x21,%al");
		asm("movb %0,%%ah"::"b"(mask));
		asm("orb %ah,%al");
		asm("outb %al,$0x21");
		asm("popl %eax");
	} else {
		uint8 mask = 0x1<<(index-8);
		asm("pushl %eax");
		asm("inb $0xA1,%al");
		asm("movb %0,%%ah"::"b"(mask));
		asm("orb %ah,%al");
		asm("outb %al,$0xA1");
		asm("popl %eax");
	}
}



