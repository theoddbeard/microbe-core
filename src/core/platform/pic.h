/*
 * pic.h
 *
 *  Created on: 29 сент. 2016 г.
 *      Author: sysadmin
 */

#ifndef PLATFORM_PIC_H_
#define PLATFORM_PIC_H_
#include "../stdlib/stdlib.h"
void PIC_Init();
void PIC_EnableIrq(uint16 index);
void PIC_DisableIrq(uint16 index);


#endif /* PLATFORM_PIC_H_ */
