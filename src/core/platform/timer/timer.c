/*
 * timer.c
 *
 *  Created on: 3 февр. 2017 г.
 *      Author: sysadmin
 */

#include "timer.h"

void Timer_Init(){
	asm("pushl %eax");
	asm("pushl %edx");

	asm("movb $0b00000000,%al");
	asm("outb %al, $0x43");

	asm("movb $0xfF,%al");
	asm("outb %al,$0x40");
	asm("movb $0xff,%al");
	asm("outb %al,$0x40");

	asm("popl %eax");
	asm("popl %edx");
}



