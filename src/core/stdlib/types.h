#ifndef TYPES_H_
#define TYPES_H_

typedef unsigned int uint32;
typedef signed int int32;

typedef unsigned short uint16;
typedef signed short int16;

typedef unsigned char uint8;
typedef signed char int8;

typedef char bool;


#define uint32v	uint32 __volatile__
#define uint16v	uint16 __volatile__
#define uint8v	uint8 __volatile__

#define int32v	int32 __volatile__
#define int16v	int16 __volatile__
#define int8v	int8 __volatile__

#define false 0
#define true 1

#define NULL (void*)0


typedef struct String{
	char* buffer;
	uint32 length;
} String;


#endif /* TYPES_H_ */


