/*
 * memory-map.h
 *
 *  Created on: 28 сент. 2016 г.
 *      Author: sysadmin
 */

#ifndef MEMORY_MAP_H_
#define MEMORY_MAP_H_

/* 4k		=	0x1000
 * 4m		=	0x40 0000
 * 1m		=	0x10 0000
 * 1k		= 	0x400
 * 256m		=	0x1000 0000
 *
 * 0xF000 0000 - base core memory
 *
 *
 *
 *
 *
 * ===== PAGE LAYOUT ======
 *
 * Catalog 3ff 0xFFC0 0000 - 0xFFFF FFFF (4m)   - 1024 page catalogs
 * 		Item 3ff 0xFFFF F000 - 0xFFFF FFFF (4k) - Self-linked
 * 		Item 3fe 0xFFFF E000 - 0xFFFF EFFF (4k)
 * 		Item 3fd 0xFFFF D000 - 0xFFFF DFFF (4k)
 * Catalog 3fe 0xFF80 0000 - 0xFFBF FFFF (4m)	- Space context
 * 		Item 0   0xFF80 0000 - 0xFF80 0FFF		- Space Context
 *		Item 3ff 0xFFBF F000 - 0xFBFF FFFF (4k) - Root catalog
 * Catalog 3fd 0xFF40 0000 - 0xFF7F FFFF (4m)	- Process context
 * 		Item 1   0xFF40 0000 - 0xFF40 0FFF		- TSS
 * 		Item 2	 0xFF40 1000 - 0xFF40 1FFF      - TSS (2)
 * 		Item 3	 0xFF40 2000 - 0xFF40 2FFF		- Process Kernel Stack
 *		Item 4	 0xFF40 3000 - 0xFF40 3FFF		- Foreign CR3
 *		Item 5   0xFF40 4000 - 0xFF40 4FFF		- Foreign 3FF catalog
 *		Item 6   0xFF40 5000 - 0xFF40 5FFF		- Foreign 3FE catalog
 *		Item 7   0xFF40 6000 - 0xFF40 6FFF		- Foreign 3FD catalog
 *		Item 8   0xFF40 7000 - 0xFF40 7FFF		- Foreign TSS
 *		Item 9	 0xFF40 8000 - 0xFF40 8FFF		- Foreign KSTACK
 *		Item 10	 0xFF40 9000 - 0xFF40 9FFF		- Foreign space context
 */

#define SPACE_CONTEXT					(void*)(0xFF800000)

#define PROCESS_TSS_ADDR			  	(void*)(0xFF400000)
#define PROCESS_TSS2_ADDR			  	(void*)(0xFF401000)
#define PROCESS_KERNEL_STACK			(void*)(0xFF402000)
#define PROCESS_KERNEL_STACK_TOP 		(void*)(0xFF402FF0)

#define PROCESS_FOREIGN_CR3				(void*)(0xFF403000)
#define PROCESS_FOREIGN_3FF				(void*)(0xFF404000)
#define PROCESS_FOREIGN_3FE				(void*)(0xFF405000)
#define PROCESS_FOREIGN_3FD				(void*)(0xFF406000)
#define PROCESS_FOREIGN_TSS				(void*)(0xFF407000)
#define PROCESS_FOREIGN_KSTACK			(void*)(0xFF408000)
#define PROCESS_FOREIGN_KSTACK_TOP		(void*)(0xFF408FF0)
#define PROCESS_FOREIGN_SPACE_CONTEXT	(void*)(0xFF409000)
#define PROCESS_FOREIGN_CATALOG			(void*)(0xFF40A000)

#endif /* MEMORY_MAP_H_ */
