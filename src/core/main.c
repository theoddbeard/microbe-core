/*
 * main.c
 *
 *  Created on: 27 сент. 2016 г.
 *      Author: sysadmin
 */
#include "stdlib/stdlib.h"
#include "console.h"
#include "memory/pager/pager.h"
#include "memory/pager/page-dispatcher.h"
#include "tables/gdt.h"
#include "tables/idt.h"
#include "tables/tss.h"
#include "memory-map.h"
#include "platform/pic.h"
#include "memory/malloc.h"
#include "process/space.h"
#include "process/process.h"
#include "process/process_carousel.h"
#include "syscall/syscalls-dispatcher.h"
#include "platform/timer/timer.h"

void GPHandler(INTPARAMS){
	printfln("#GP %h, %h,%h",errcode,segment,addr);
	asm("hlt");
}



void Int0Handler(INTPARAMS){
	ProcessCarousel_Switch();
//	printf("#");
}

void Int1Handler(INTPARAMS){
	//ProcessCarousel_Switch();

/*
	asm("pushl %eax");
	asm("inb $0x60,%al");
	asm("addl $4,%esp");
	//asm("movb %%al,%0":"=m"(a));
	asm("subl $4,%esp");
	asm("inb $0x61,%al");
	asm("pushl %eax");
	asm("orb 0x80,%al");
	asm("outb %al, $0x61");
	asm("popl %eax");
	asm("outb %al, $0x61");
	asm("popl %eax");

	printf("#");
	*/
}

typedef struct Kstack_init{
	uint32 __volatile__ iret_eip;
	uint32 __volatile__ iret_cs;
	uint32 __volatile__ iret_eflags;
	uint32 __volatile__ iret_esp;
	uint32 __volatile__ iret_ss;
} __attribute__ ((__packed__)) Kstack_init;

__attribute__((optimize("-fno-omit-frame-pointer")))
void returnToInit(void* entry, void* stack){
	printfln("Return to R0: %h, %h",entry,stack);
	Kstack_init* __volatile__ kstack = (Kstack_init*)((uint32)(PROCESS_KERNEL_STACK_TOP)-sizeof(Kstack_init));
	uint32 eflags = 0;
	asm("sti");
	asm("pushl %eax");
	asm("pushf");
	asm("popl %eax");
	asm("movl %%eax,%0":"=l"(eflags));
	asm("movw $0x23, %ax");
	asm("movw %ax, %ds");
	asm("movw %ax, %es");
	asm("movw %ax, %fs");
	asm("movw %ax, %gs");
	asm("popl %eax");
	eflags = eflags|0x200;
	kstack->iret_eflags = eflags;
	kstack->iret_eip = (uint32)entry;
	kstack->iret_esp = (uint32)stack;
	kstack->iret_cs = 0x1b;
	kstack->iret_ss = 0x23;

	asm("movl %0, %%eax"::"m"(kstack));
	asm("movl %eax, %esp");

	//asm("sti");
	asm("iret");

	for(;;);
}

uint32 delay(){
    uint32 a = 1;
    for(uint32 i=0;i<(65535*10);i++){
	a = a+i;
	asm("pushl %eax");
	asm("pushl %ebx");
	asm("movl $0xFABA, %eax");
	asm("movl $0x1234, %ebx");
	asm("mull %ebx");
	asm("popl %ebx");
	asm("popl %eax");
    }
    return a;
}

uint32 init(uint32 __volatile__ freeMemory,uint32 __volatile__ totalMemory,void* __volatile__ initProcessEntry, void* __volatile__ initProcessStack){
	console_init(80,25);
	printfln("Free memory=%h,total=%h",freeMemory,totalMemory);
	void* freeBarier = Pager_Init(freeMemory, totalMemory);
	GDT_Init();
	IDT_init();

	void* page_kstack = Pager_GetPage();
	void* page_tss = Pager_GetPage();
//	void* page_tss2 = Pager_GetPage();

	Pager_Mount(page_kstack, PROCESS_KERNEL_STACK);
	Pager_Mount(page_tss, PROCESS_TSS_ADDR);
//	Pager_Mount(page_tss2, PROCESS_TSS2_ADDR);

	TSS_Init(PROCESS_TSS_ADDR, PROCESS_KERNEL_STACK_TOP,0);
	GDT_AppendTSS(PROCESS_TSS_ADDR, TSS_GetSize());
	TSS_flush();

	PIC_Init();

	IDT_setIntHandler(&GPHandler, 0xd);
	IDT_setIntHandler(PageDispatcher_GetIntHandler(), 0xE);
	IDT_setHandler(0x50, 0x8, Syscall_GetIntHandler(), INT_HANDLER_TRAP);
	IDT_setIntHandler(&Int0Handler,0x20);
	//IDT_setIntHandler(&Int1Handler,0x21);

	asm("sti");

	uint32 heapSize = 0xFF400000-(uint32)freeBarier-0x2000;
	printfln("Free barier=%h,heapSize=%h",freeBarier,heapSize);

	//uint32* __volatile__ a = (uint32*)0xF4000000;
	asm("movl $0x1234abcd, 0xFA000000");
	//printf("A=%h",*a);
	Heap_Init((void*)0xFA000000, 0x1000000);


	Space_Init();
	Process_Init();
	ProcessCarousel_Init(0);

	printfln("Touch init:e=%h, s=%h",initProcessEntry,initProcessStack);
	//printfln("kstack size=%d",sizeof(Kstack_init));




	Timer_Init();
	PIC_EnableIrq(0);
	//PIC_EnableIrq(1);
	returnToInit(initProcessEntry, initProcessStack);

	uint8 a = 0;

	for (;;){
//		a = 0;
//		asm("pushl %eax");
//		asm("inb $0x64,%al");
//		asm("andb $0b01, %al");
//		asm("cmp $0x0, %al");
//		asm("jnz NOKEY");
//		asm("inb $0x60,%al");
//		asm("addl $4,%esp");
//		asm("movb %%al,%0":"=m"(a));
//		asm("subl $4,%esp");

//		asm("inb $0x61,%al");
//		asm("pushl %eax");
//		asm("orb 0x80,%al");
//		asm("outb %al, $0x61");
//		asm("popl %eax");
//		asm("outb %al, $0x61");

//		asm("NOKEY:");
//		asm("popl %eax");
//		if (a>0){
//			printf("#%h",a);
//			delay();
//		}

	}
//
//	uint32*  a = malloc(sizeof(0x20));
//	printfln("A=%h",a);
//	*a = 0x42424242;
//	uint32  b = *a;
//	printfln("42=%h %h",b,a);
//
//	uint32* __volatile__ a = (uint32*)0xFA000000;
//	printfln("Put 42");
//	//*a = 0x42424242;
//	asm("movl $0x42424242, 0xFA000000");
//	asm("movl $0x43434343, 0xFA001000");
//	printfln("Pop 42");
//	uint32 __volatile__ b = *a;
//	uint32 __volatile__ c = *((uint32*)0xFA001000);
//	printfln("42=%h, 43=%h",b,c);

	//for(;;);
	return 42;
}
