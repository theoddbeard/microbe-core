/*
 * new_malloc.c
 *
 *  Created on: 27 апр. 2016 г.
 *      Author: sysadmin
 */

#include  "malloc.h"
#include "../console.h"
typedef struct DirtyBlock{
	uint32 size;
} DirtyBlock;

typedef struct HeapBlock{
	void* __volatile__ nextEntry;
	void* __volatile__ prevEntry;
	uint32 __volatile__ size;
	uint32 __volatile__ __align;
} HeapBlock;

void*  heap_start;
uint32 heap_size;

void* heap;

void Heap_Init(void* addr,uint32 size){
	heap = addr;
	HeapBlock* h = heap;
	h->nextEntry = heap;
	h->prevEntry = heap;
	h->size = size-sizeof(DirtyBlock);
	heap_start = addr;
	heap_size = size;
//	printfln("IHeap=%h",h->nextEntry);
}

void* malloc(uint32 memsz){
	memsz = (memsz+0xF)&0xFFFFFFF0;
	HeapBlock* startBlock = heap;
	HeapBlock* block = heap;
//	printfln("Heap=%h",heap);
	while(1){
		if (block->size>=memsz){
			if (block->size>=memsz+sizeof(HeapBlock)*2){
				//HeapBlock* dirtyBlock = block;

				HeapBlock* cutted = (HeapBlock*)((uint32)block+memsz+sizeof(DirtyBlock));
				cutted->size = block->size-memsz-sizeof(DirtyBlock);
				if (block->nextEntry!=block){
					cutted->nextEntry = block->nextEntry;
				} else {
					cutted->nextEntry = cutted;
				}

				if (block->prevEntry!=block){
					cutted->prevEntry = block->prevEntry;
				} else {
					cutted->prevEntry = cutted;
				}

				DirtyBlock* dirtyBlock = (DirtyBlock*)block;
				dirtyBlock->size = memsz;
				//dirtyBlock->nextEntry = 0;
				heap = cutted;
//				printfln("Cut");
				return (void*)((uint32)dirtyBlock+sizeof(DirtyBlock));

			} else {
				HeapBlock* nextFreeBlock = block->nextEntry;
				HeapBlock* prevFreeBlock = block->prevEntry;

				if (nextFreeBlock!=block){
					//Если это не последний блок

					if (nextFreeBlock->nextEntry==block){
						//Если предпоследний
						nextFreeBlock->nextEntry=nextFreeBlock;
						nextFreeBlock->prevEntry=nextFreeBlock;
					} else {
						nextFreeBlock->prevEntry = prevFreeBlock;
						prevFreeBlock->nextEntry = nextFreeBlock;
					}

					heap = nextFreeBlock;
				} else {
					//A если последний
					heap = 0;
				}

				uint32 sz = block->size;
				DirtyBlock* dirtyBlock = (DirtyBlock*)block;
				dirtyBlock->size = sz;

				printfln("Full");
				return (void*)((uint32)dirtyBlock+sizeof(DirtyBlock));

			}

		}
		if (block->nextEntry==block){
			return 0;
		}
		block = block->nextEntry;
	}
	return 0;
}

void free(void* mem){
	DirtyBlock* dirtyBlock = (DirtyBlock*)((uint32)mem-sizeof(DirtyBlock));

	if (heap!=0){
		//Если есть еще свободные блоки
		void* proposeBlockAfter = (void*)((uint32)mem+dirtyBlock->size);

		HeapBlock* block = heap;
		do {

			void* proposeBlockBefore = (void*)((uint32)block+block->size+sizeof(DirtyBlock));

			if (proposeBlockBefore==block){
				//Свободный блок перед - соединяем;
				block->size=block->size+dirtyBlock->size+sizeof(DirtyBlock);
				void* proposeNextBlock = (void*)((uint32)block+block->size+sizeof(DirtyBlock));
				if (proposeNextBlock==block->nextEntry){
					//Впереди еще свободный блок - соединяем
					HeapBlock* nextBlock = block->nextEntry;
					block->size = block->size+nextBlock->size+sizeof(DirtyBlock);
					block->nextEntry = nextBlock->nextEntry;

					if (block->prevEntry==nextBlock){
						block->prevEntry = block;
					}

					HeapBlock* blockAfterNext = nextBlock->nextEntry;
					blockAfterNext->prevEntry = block;

				}

				return;

			}


			if (block==proposeBlockAfter){
				//Совбодный блок после грязного - соединяем
				HeapBlock* defragmentblock = (HeapBlock*) dirtyBlock;
				defragmentblock->size = block->size+dirtyBlock->size+sizeof(DirtyBlock);
				defragmentblock->nextEntry = block->nextEntry;
				defragmentblock->prevEntry = block->prevEntry;

				HeapBlock* prevBlock = block->prevEntry;

				void* pointPre = (void*)((uint32)prevBlock+prevBlock->size+sizeof(DirtyBlock));
				if (pointPre==defragmentblock){
					prevBlock->size = prevBlock->size+block->size+sizeof(DirtyBlock);
					prevBlock->nextEntry = block->nextEntry;
				}

				return;

			}


			block = block->nextEntry;




		} while(block->nextEntry!=block);


		return;
	}

	uint32 sz = dirtyBlock->size;
	HeapBlock* block = (HeapBlock*) dirtyBlock;
	block->size = sz;
	block->prevEntry = block;
	block->nextEntry = block;
	heap = block;
}



