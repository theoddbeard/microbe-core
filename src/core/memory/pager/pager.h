/*
 * pager.h
 *
 *  Created on: 28 сент. 2016 г.
 *      Author: sysadmin
 */

#ifndef MEMORY_PAGER_PAGER_H_
#define MEMORY_PAGER_PAGER_H_
#include "../../stdlib/stdlib.h"
void* Pager_Init(uint32 availToAlloc,uint32 totalMemorySize);

void* Pager_GetPage();
bool Pager_GetPageByIndex(uint32 index);
void Pager_Unmount(void* vaddr);
void Pager_Release(void* vaddr);
void Pager_Mount(void* paddr, void* vaddr);

uint32* Pager_GetCR3();
uint32 Pager_Get3FD();


void Pager_SetPageItem(void* catalog,void* item, uint32 index);
void Pager_SetRootItem(uint32* cr3,uint32* cat3ff, void* item, uint32 index);
void Pager_InitRoot(uint32* rootToInit,uint32* cat3ffToInit,uint32 p_cat3ff);
void Pager_MountForeign(uint32* cr3, uint32* c3ff, void* src, void* dst);
#endif /* MEMORY_PAGER_PAGER_H_ */
