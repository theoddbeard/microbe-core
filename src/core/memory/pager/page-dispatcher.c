/*
 * page-dispatcher.c
 *
 *  Created on: 29 сент. 2016 г.
 *      Author: sysadmin
 */
#include "../../tables/idt.h"
#include "pager.h"
#include "../../console.h"

__attribute__((optimize("-fno-omit-frame-pointer")))
void int0xEHandler(INTPARAMS){

	void* errorAddr = 0;

	asm("pushl %eax");
	asm("movl %cr2, %eax");
	asm("movl %%eax,%0":"=m"(errorAddr));
	asm("popl %eax");

	//printfln("0xE: %h %d",errorAddr,errcode);
	//for(;;);

	if ((uint32)errorAddr>0xF0000000){

		uint32 vaddr = (uint32)errorAddr&0xFFFFF000;

		void* page = Pager_GetPage();
		//printfln("Mount kernel %h to %h",page,vaddr);

		Pager_Mount(page, (void*)vaddr);
		//for(;;);
		//Mount page to Kernel Space
		//printf("!");
	} else {
		//Call userspace dispatcher
		printfln("PAGE ERR at %h %h:%h,%h",errorAddr,segment,addr,errcode);
		asm("hlt");
	}
	//printfln("PAGE ERR at %h %h:%h,%h",errorAddr,segment,addr,errcode);

/*
	uint32 catalogIndex = GET_CATALOG_INDEX(errorAddr);
	uint32 pageIndex = GET_PAGE_INDEX(errorAddr);

	PageCatalog catalog = getCatalog(catalogIndex);
	if ((catalog[pageIndex]&0x1)==0){
		void* page = CMPreservePage();
		pager_mount(page,errorAddr);
	}
*/

}

void* PageDispatcher_GetIntHandler(){
	return &int0xEHandler;
}
