/*
 * pager.c
 *
 *  Created on: 28 сент. 2016 г.
 *      Author: sysadmin
 */
#include "pager.h"
#include "../../stdlib/stdlib.h"
#include "../../console.h"
#include "../../memory-map.h"

#define MAKE_PAGE_ENTRY(addr)		(uint32)(((uint32)(addr)&0xFFFFF000)|0b00011111)
#define MAKE_CATALOG_ENTRY(addr)	(uint32)(((uint32)(addr)&0xFFFFF000)|0b00011111)
#define TLB_FLUSH asm("pushl %eax\r\n movl %cr3, %eax\r\n movl %eax, %cr3\r\n popl %eax")
uint32* root = (uint32*)(0xFFBFF000);
uint32* c3ff = (uint32*)(0xFFFFF000);

#define GET_CATALOG_ADDR(num) (uint32*)(0xFFC00000+(0x1000*num))

int8 pageMap[1024*1024] __attribute__ ((aligned(8)));
void* Pager_GetPage(){
	for (uint32 i=0;i<1024*1024;i++){
		if (pageMap[i]==0){
			pageMap[i] = 1;
			return (void*)(i<<12);
		}
	}
	printfln("OUT OF MEMORY!");
	for(;;);
	return 0;
}

bool Pager_GetPageByIndex(uint32 index){
	//printf("p%h=%h",index,pageMap[index]);
	if (pageMap[index]==0){
		pageMap[index] = 1;
		return true;
	}
	return true;
}

void initCatalog(uint32* catalog){
	for(uint16 i=0;i<0x400;i++){
		catalog[i] = 0;
	}
}

void* Pager_Init(uint32 availToAlloc,uint32 totalMemorySize){
	uint32 lastPage = totalMemorySize>>12;
	uint32 avail = availToAlloc>>12;
	//printfln("Init page map, avail=%h, total=%h",availToAlloc,totalMemorySize);
	for(uint32 i=0;i<1024*1024;i++){
		if (i<avail){
			pageMap[i] = 1;
		} else{
			if (i<lastPage){
				pageMap[i] = 0;
			} else {
				pageMap[i] = -1;
			}
		}
	}

	uint32* rootCatalog = 0;
	asm("pushl %eax");
	asm("movl %cr3, %eax");
	asm("movl %%eax,%0":"=l"(rootCatalog));
	asm("popl %eax");
	printfln("Root catalog=%h",rootCatalog);
	//printfln("Get pages for catalog");
	uint32* catalog3FF = (uint32*)Pager_GetPage();
	uint32* catalog3FE = (uint32*)Pager_GetPage();
	uint32* catalog3FD = (uint32*)Pager_GetPage();

	initCatalog(catalog3FF);
	initCatalog(catalog3FE);
	initCatalog(catalog3FD);

	//printfln("Fill catalogs");
	rootCatalog[0x3FF] = MAKE_CATALOG_ENTRY(catalog3FF);
	rootCatalog[0x3FE] = MAKE_CATALOG_ENTRY(catalog3FE);
	rootCatalog[0x3FD] = MAKE_CATALOG_ENTRY(catalog3FD);


	catalog3FE[0x3FF] = MAKE_PAGE_ENTRY(rootCatalog);

	//printfln("Fill 3FF");
	for(uint16 i=0;i<0x400;i++){
		uint32 cr3Entry = rootCatalog[i]&0xFFFFF000;
		catalog3FF[i] = MAKE_PAGE_ENTRY(cr3Entry);
	}


	//printfln("Unmount catalogs");
	for(uint16 i=0;i<0x300;i++){
		uint32 cr3Entry = rootCatalog[i];
		if ((cr3Entry&0x1) !=0 ){
			//printfln("Unmount [%d] %h",i,cr3Entry);
			Pager_Release((void*)cr3Entry);
			//Pager_Unmount((void*)cr3Entry);
		}
	}
	TLB_FLUSH;
	uint32 freeBarier = 0;
	for (uint32 i=0xF0000000;i<0xFF400000;i+=0x1000){
		uint16 catN = (i>>22)&0x3FF;
		uint16 pageN = (i>>12)&0x3FF;

		if ((root[catN]&0x1)==0){
			freeBarier = i+0x4000;
			break;
		}

		uint32* catalog = GET_CATALOG_ADDR(catN);
		if ((catalog[pageN]&0x1)==0){
			freeBarier = i+0x4000;
			break;
		}
	}

	for (uint16 i=0x3C0;i<0x3FD;i++){
		if ((root[i]&0x1)==0){
			void* c = Pager_GetPage();
			root[i] = MAKE_CATALOG_ENTRY(c);
			c3ff[i] = MAKE_PAGE_ENTRY(c);
		}
	}
	//printfln("Free barier=%h",freeBarier);
	return (void*)freeBarier;

	//catalog3FD[3] = 0x10;

}
__attribute__((optimize("-fno-omit-frame-pointer")))
uint32* Pager_GetCR3(){
	uint32* cr3 = 0;
	asm("pushl %eax");
	asm("movl %cr3, %eax");
	asm("movl %%eax, %0":"=m"(cr3));
	asm("popl %eax");
	return cr3;
}


uint32 Pager_Get3FD(){
	return root[0x3fd]&0xFFFFF000;
}


void Pager_Unmount(void* vaddr){
	uint16 catalogN = ((uint32)vaddr>>22)&0x3FF;
	uint16 pageN = ((uint32)vaddr>>12)&0x3FF;
	uint32* catalog = GET_CATALOG_ADDR(catalogN);
	catalog[pageN] = 0;
	TLB_FLUSH;
}

void Pager_Release(void* vaddr){
	uint32 index = (uint32)vaddr>>12;
	uint16 catalogN = ((uint32)vaddr>>22)&0x3FF;
	uint16 pageN = ((uint32)vaddr>>12)&0x3FF;
	uint32* catalog = GET_CATALOG_ADDR(catalogN);
	catalog[pageN] = 0;
	pageMap[index] = 0;
	//printf("Rp%h=%h",index,pageMap[index]);
	TLB_FLUSH;
}



void Pager_Mount(void* paddr, void* vaddr){
	uint16 catalogN = ((uint32)vaddr>>22)&0x3FF;
	uint16 pageN = ((uint32)vaddr>>12)&0x3FF;

	if ((root[catalogN]&0x1)==0){

		uint32* newCatalog = Pager_GetPage();
		//initCatalog(newCatalog);
		printfln("MOUNT CATALOG");
		c3ff[catalogN] = MAKE_PAGE_ENTRY(newCatalog);
		root[catalogN] = MAKE_CATALOG_ENTRY(newCatalog);
	};

	uint32* catalog = GET_CATALOG_ADDR(catalogN);
	catalog[pageN] = MAKE_PAGE_ENTRY(paddr);

	TLB_FLUSH;

}


void Pager_SetPageItem(void* catalog,void* item, uint32 index){
	uint32* cat = (uint32*)catalog;
	cat[index] = MAKE_PAGE_ENTRY(item);

}
void Pager_SetRootItem(uint32* cr3,uint32* cat3ff, void* item, uint32 index){
	cr3[index] = MAKE_CATALOG_ENTRY(item);
	cat3ff[index] = MAKE_PAGE_ENTRY(item);

}

void Pager_InitRoot(uint32* rootToInit,uint32* cat3ffToInit,uint32 p_cat3ff){
	for (uint16 i=0x3C0;i<0x3FE;i++){
		rootToInit[i] = root[i];
		cat3ffToInit[i] = c3ff[i];
	}
	rootToInit[0x3FF] = MAKE_CATALOG_ENTRY(p_cat3ff);
	cat3ffToInit[0x3FF] = MAKE_PAGE_ENTRY(p_cat3ff);
}


void Pager_MountForeign(uint32* cr3, uint32* c3ff, void* src, void* dst){
	uint32 srcCatN = ((uint32)src>>22)&0x3FF;
	uint32 srcPageN = ((uint32)src>>12)&0x3FF;

	uint32 dstCatN = ((uint32)dst>>22)&0x3FF;
	uint32 dstPageN = ((uint32)dst>>12)&0x3FF;

	uint32* srcCat = (uint32*)(root[srcCatN]&0xFFFFF000);
	void* srcPage = (void*)(srcCat[srcPageN]&0xFFFFF000);

	uint32* dstCat = (uint32*)(cr3[dstCatN]&0xFFFFF000);
	if ((cr3[dstCatN]&0x1)==0){
		dstCat = Pager_GetPage();
		cr3[dstCatN] = MAKE_CATALOG_ENTRY(dstCat);
		c3ff[dstCatN] = MAKE_PAGE_ENTRY(dstCat);
		printf("@");
	}
	printf("$");
	Pager_Mount(dstCat, PROCESS_FOREIGN_CATALOG);

	dstCat = PROCESS_FOREIGN_CATALOG;
	dstCat[dstPageN] = MAKE_PAGE_ENTRY(srcPage);




}

