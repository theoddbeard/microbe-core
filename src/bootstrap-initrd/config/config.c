/*
 * config.c
 *
 *  Created on: 27 сент. 2016 г.
 *      Author: sysadmin
 */
#include "config.h"
#include "../console.h"
#include "../stdlib/stdlib.h"
/*
ConfigCallback callbacks[32];
uint32 callbacksCount = 0;
*/
ConfigParser defaultParser;

ConfigParser* Parser_Create(){
	ConfigParser* parser = malloc(sizeof(ConfigParser));
	parser->callbacksCount = 0;
	return parser;
}

void Parser_Destroy(ConfigParser* parser){
	free(parser);
}

void Parser_RegisterCallback(ConfigParser* parser, ConfigCallback callback){
	parser->callbacks[parser->callbacksCount] = callback;
	parser->callbacksCount++;
}
void config_registerCallback(ConfigCallback callback){
	Parser_RegisterCallback(&defaultParser, callback);
}

void Parser_ClearCallbacks(ConfigParser* parser){
	for (uint8 i=0;i<32;i++){
			parser->callbacks[i].callback = NULL;
			parser->callbacks[i].cmd.buffer = 0;
			parser->callbacks[i].cmd.length = 0;
		}
	parser->callbacksCount = 0;
}

void config_clearCallbacks(){
	Parser_ClearCallbacks(&defaultParser);
}

ConfigCallback* Parser_SearchCallback(ConfigParser* parser,String cmd){

	for(uint32 i=0;i<parser->callbacksCount;i++){
			if (StringCompare(cmd, parser->callbacks[i].cmd)){
				return &(parser->callbacks[i]);
			}
		}
		return 0;
}

ConfigCallback* searchCallback(String cmd){
	return Parser_SearchCallback(&defaultParser, cmd);
}

void readLine(ConfigParser* parser,String line){
	String cmd,arg;
	cmd.buffer = line.buffer;
	cmd.length = 0;

	arg.buffer = 0;
	arg.length = 0;

	for (uint32 i=0;i<line.length;i++){
		if (line.buffer[i]==' '){
			if (cmd.length==0){
				cmd.length = i;
				arg.buffer = line.buffer+i+1;
				arg.length = line.length-i-1;
				break;
			}
		}
	}

	ConfigCallback* callback = Parser_SearchCallback(parser,cmd);
	if (callback!=NULL){
		callback->callback(arg);
	}

}
void Parser_Read(ConfigParser* parser,String conf){
	String line;
		line.buffer = conf.buffer;
		line.length = 0;
		for (uint32 i=0;i<conf.length;i++){
			if (conf.buffer[i]==0x0A){
				readLine(parser,line);
				line.buffer = line.buffer+line.length+1;
				line.length = 0;
			} else {
				line.length++;
			}
		}
}
void Config_read(String conf){
	Parser_Read(&defaultParser, conf);
}

