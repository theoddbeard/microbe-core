/*
 * config.h
 *
 *  Created on: 27 сент. 2016 г.
 *      Author: sysadmin
 */

#ifndef CONFIG_CONFIG_H_
#define CONFIG_CONFIG_H_
#include "../stdlib/stdlib.h"
typedef void(*ConfigCallbackFunction)(String arg);



typedef struct ConfigCallback{
	String cmd;
	ConfigCallbackFunction callback;
} ConfigCallback;

typedef struct{
	ConfigCallback callbacks[32];
	uint32 callbacksCount;
} ConfigParser;

void Config_read(String conf);
void config_registerCallback(ConfigCallback callback);
void config_clearCallbacks();

ConfigParser* Parser_Create();
void Parser_Destroy(ConfigParser* parser);
void Parser_RegisterCallback(ConfigParser* parser, ConfigCallback callback);
void Parser_ClearCallbacks(ConfigParser* parser);
void Parser_Read(ConfigParser* parser,String conf);

#endif /* CONFIG_CONFIG_H_ */
