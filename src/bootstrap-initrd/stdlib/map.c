/*
 * map.c
 *
 *  Created on: 4 февр. 2017 г.
 *      Author: irokez
 */


#include "strings.h"
#include "malloc.h"


typedef struct{
	String key;
	void* data;
	void* next;
	void* align;
} MapEntry;

typedef struct Map{
	MapEntry* entry;
} Map;

void* Map_Get(Map* map,String key){
	MapEntry* e = map->entry;

	while(true){
		if (StringCompare(e->key, key)){
			return e->data;
		}
		if (e->next==NULL){
			return NULL;
		}
		e = e->next;

	}
}
void Map_Set(Map* map,String key,void* data){
	MapEntry* e = malloc(sizeof(MapEntry));


}

