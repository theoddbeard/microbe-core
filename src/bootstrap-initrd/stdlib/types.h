#ifndef TYPES_H_
#define TYPES_H_

typedef unsigned int uint32;
typedef signed int int32;

typedef unsigned short uint16;
typedef signed short int16;

typedef unsigned char uint8;
typedef signed char int8;

typedef char boolean;

#define false 0
#define true 1

#define NULL (void*)0





#endif /* TYPES_H_ */


