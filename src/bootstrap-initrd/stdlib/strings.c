/*
 * strings.c
 *
 *  Created on: 27 сент. 2016 г.
 *      Author: sysadmin
 */
#include "types.h"
#include "strings.h"

String StringCreate(char* buffer){
	String r = {
			.buffer = buffer,
			.length = 0
	};
	while(buffer[r.length]!=0){
		r.length++;
	};
	return r;
}


boolean StringCompare(String str1,String str2){
	if (str1.length!=str2.length){
		return false;
	};
	for (uint32 i=0;(i<str1.length)&&(i<str2.length);i++){
		if (str1.buffer[i]!=str2.buffer[i]){
			return false;
		}
	}
	return true;
}
