/*
 * stdlib.h
 *
 *  Created on: 27 сент. 2016 г.
 *      Author: sysadmin
 */

#ifndef STDLIB_H_
#define STDLIB_H_

#ifdef __DEBUG__
#define DBG(fmt, ...) printfln(fmt, __VA_ARGS__)
#else
#define DBG(fmt, ...)
#endif

#define D(fmt) "#ifdef __D__ printfln(fmt) #else #endif"

#define asm(CODE)  __asm__ __volatile (CODE)

#include "types.h"
#include "strings.h"
#include "memory.h"





#endif /* RAMDISK_STDLIB_H_ */
