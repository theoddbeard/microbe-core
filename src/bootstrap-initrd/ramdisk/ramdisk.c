/*
 * ramdisk.c
 *
 *  Created on: 27 сент. 2016 г.
 *      Author: irokez
 */
#include "ramdisk.h"
#include "../stdlib/stdlib.h"
#include "../console.h"


#pragma pack(1)
typedef struct cpioItem{
	char    c_magic[6];
	char    c_ino[8];
	char    c_mode[8];
	char    c_uid[8];
	char    c_gid[8];
	char    c_nlink[8];
	char    c_mtime[8];
	char    c_filesize[8];
	char    c_devmajor[8];
	char    c_devminor[8];
	char    c_rdevmajor[8];
	char    c_rdevminor[8];
	char    c_namesize[8];
	char    c_check[8];
} cpioItem;
#pragma pack(0)

void* ramdisk = 0;
void Ramdisk_Set(void* rd){
	ramdisk = rd;
}
void* Ramdisk_Get(){
	return ramdisk;
}
boolean strCmp(char* src, char* dst){
	while(*src !=0 ){
		if (*src!=*dst){
			return false;
		}
		src++;
		dst++;
	}
	return true;
}

char charToDigit(char c){
	if (c>=0x30&&c<=0x39){
		return c-0x30;
	}
	if (c>=0x41&&c<=0x46){
		return c-0x37;
	}
	if (c>=0x61&&c<=0x66){
		return c-0x57
				;
	}
	return 0;
}

uint32 strToUint32(char* src){
	uint32 dst = 0;
	for (int8 i=0;i<8;i++){
		char digit = charToDigit(src[i]);
		dst = dst+(digit<<(4*(7-i)));
	}

	return dst;
}


char* cpioFinalItemName = "TRAILER!!!";
String cpioLastItemName = {
		.buffer = "TRAILER!!!",
		.length = 10
};
uint32 count = 0;
CPIOFile Ramdisk_SearchFile(void* cpioAddr,String need){

	cpioItem* item = (cpioItem*) cpioAddr;
	uint32 namesize = (strToUint32(item->c_namesize)+0)&0xffffffff;
	uint32 filesz = (strToUint32(item->c_filesize)+0)&0xfffffffff;

	uint32 nameOffset = sizeof(cpioItem);
	uint32 dataOffset = (sizeof(cpioItem)+namesize+3)&0xfffffffc;
	uint32 nextOffset = (dataOffset+filesz+3)&0xfffffffc;
	//system/service/storage.service

	char* namePtr = (char*)((uint32)cpioAddr+nameOffset);

	String name = {
			.buffer = namePtr,
			.length = namesize-1
	};


//	printfln("FILE=%s,no=%d,do=%d,no=%d,ns=%d",namePtr,nameOffset,dataOffset,nextOffset,namesize);
	if (StringCompare(need,name)==true){
		//printfln("Found %s",namePtr);
		CPIOFile file;
		file.data = (void*)((uint32)cpioAddr+dataOffset);
		file.size = filesz;

		return file;
	}


	if (StringCompare(cpioLastItemName, name)==false){
		void* nextItem = (void*)((uint32)cpioAddr+nextOffset);
		return Ramdisk_SearchFile(nextItem,need);
	}
	//printfln("End of archive");
	CPIOFile f;
	f.data = 0;
	f.size = 0;
	return f;
};



