/*
 * ramdisk.h
 *
 *  Created on: 27 сент. 2016 г.
 *      Author: irokez
 */

#ifndef RAMDISK_RAMDISK_H_
#define RAMDISK_RAMDISK_H_

#include "../stdlib/stdlib.h"

typedef struct CPIOFile{
	uint32 size;
	void* data;
} CPIOFile;

CPIOFile  Ramdisk_SearchFile(void* cpioAddr,String need);
void Ramdisk_Set(void* rd);
void* Ramdisk_Get();

#endif /* RAMDISK_RAMDISK_H_ */
