/*
 * pager.h
 *
 *  Created on: 27 сент. 2016 г.
 *      Author: sysadmin
 */

#ifndef PAGER_PAGER_H_
#define PAGER_PAGER_H_
#include "../stdlib/stdlib.h"

uint32* Pager_GetPage();
void Pager_Mount(void* paddr, void* vaddr);
void Pager_Init(uint32 total,void* freeMem);




#endif /* PAGER_PAGER_H_ */
