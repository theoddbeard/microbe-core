/*
 * pager.c
 *
 *  Created on: 27 сент. 2016 г.
 *      Author: sysadmin
 */
//#define __DEBUG__ 0
#include "pager.h"
#include "../stdlib/stdlib.h"
#include "../console.h"
#undef __DEBUG__



uint32 freePages = 0;
uint32 totalPages = 0;
uint32* rootCatalog = NULL;

uint32* Pager_GetPage(){
	freePages++;
	return (uint32*)(freePages<<12);
}

void initCatalog(uint32* catalog){
	for (uint16 i=0;i<0x1000;i++){
		catalog[i] = 0;
	}
}


void Pager_Mount(void* paddr, void* vaddr){
	DBG("MOUNT %h to %h",paddr,vaddr);
	uint16 catN = ((uint32)vaddr>>22)&0x3FF;
	uint16 pageN = ((uint32)vaddr>>12)&0x3FF;

	if (!(rootCatalog[catN]&0x1)){
				uint32* cat = Pager_GetPage();
				initCatalog(cat);
				rootCatalog[catN] = (uint32)(((uint32)cat&0xFFFFF000)|0b1111);
			};
	uint32* catalog = (uint32*)(rootCatalog[catN]&0xFFFFF000);
	catalog[pageN] = ((uint32)paddr&0xFFFFF000)|0b1111;

}

void MountOneToOne(void* mem){
	rootCatalog = Pager_GetPage();
	initCatalog(rootCatalog);
	for (uint32 i=0;i<(uint32) mem;i+=0x1000){
		uint16 catN = (i>>22)&0x3FF;
		uint16 pageN = (i>>12)&0x3FF;

		if (!(rootCatalog[catN]&0x1)){
			uint32* cat = Pager_GetPage();
			initCatalog(cat);
			rootCatalog[catN] = (uint32)(((uint32)cat&0xFFFFF000)|0b1111);
		};
		uint32* catalog = (uint32*)(rootCatalog[catN]&0xFFFFF000);
		catalog[pageN] = (uint32)((i&0xFFFFF000)|0b1111);
	}

	asm("pushl %eax");
	asm("movl rootCatalog, %eax");
	asm("movl %eax, %cr3");
	asm("movl %cr0, %eax");
	asm("orl $0x80000000, %eax");
	asm("movl %eax, %cr0");
	asm("popl %eax");
}

void Pager_Init(uint32 total,void* freeMem){
	freePages = ((uint32)freeMem+0xFFF)>>12;
	totalPages = total>>12;

	MountOneToOne((void*)total);
}





