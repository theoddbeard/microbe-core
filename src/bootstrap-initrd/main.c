#include "stdlib/stdlib.h"
#include "elf.h"
#include "console.h"
#include "multiboot.h"
#include "ramdisk/ramdisk.h"
#include "config/config.h"
#include "pager/pager.h"
#include <corelib/corelib.h>
#include "service/service.h"
#include "cpio.h"

void loadCore(String path);

uint32 ramSize = 0;

String confString;
void main(multibootInfo* mbInfo){
	console_init(80,25);
	printfln("Starting Microbecore : %s",mbInfo->cmdLine);

	ramSize = (mbInfo->mem_lower+mbInfo->mem_upper*1024);
	printfln("RAM=%h bytes",ramSize);

	moduleInfo* ramdiskModule = mbInfo->mods_addr;

	printf("Init pager...");
	Pager_Init(ramSize, ramdiskModule->mod_end);
	printfln("done!");

	void* ramdisk = ramdiskModule->mod_start;
	Ramdisk_Set(ramdisk);
	String bootCfg = {.buffer="boot.cfg",.length=10};

	CPIOFile conf =  Ramdisk_SearchFile(ramdisk,StringCreate("./boot.cfg"));
	if (conf.size==0){
		printfln("Boot config not found!");
		for(;;);
	}

	confString.buffer = conf.data;
	confString.length = conf.size;



	ConfigCallback callback_CORE;
	callback_CORE.cmd.buffer = "core";
	callback_CORE.cmd.length = 4;
	callback_CORE.callback = &loadCore;
	config_registerCallback(callback_CORE);

	printfln("Loading config...");
	Config_read(confString);

	for (;;);

}

uint32 max(uint32 a,uint32 b){
	return a>b?a:b;
}

typedef uint32(*coreInit)(uint32,uint32,void*,void*);


uint32 delay(){
	uint32 a = 1;
	for(uint32 i=0;i<(65535*10);i++){
		a = a+i;
		asm("pushl %eax");
		asm("pushl %ebx");
		asm("movl $0xFABA, %eax");
		asm("movl $0x1234, %ebx");
		asm("mull %ebx");
		asm("popl %ebx");
		asm("popl %eax");
	}
	return a;
}

void process2(){
	Core_Kprint("Process 1");
	//Core_Process_Switch(0);
	for(;;){
		delay();
		Core_Kprint("2");
		//printf("2");
	}
}
void loadService(String path){
	printfln("Service %g",path);
	CPIOFile serviceConf = Ramdisk_SearchFile(Ramdisk_Get(), path);
	Service_Read(&serviceConf);
};

void process1(){
	//Core_Syscall(0xAA, 0xBB, 0xCC, 0xDD);
//	Core_Kprint("SYSCALL_PRINT_TEST");

	//Core_Process_Switch(1);
	//Core_Kprint("Process 0");

	//uint32 spaceId = Core_Space_Create();
	//printfln("Create space %h",spaceId);
	//printfln("syscall");
	//Core_Kprint("Continue proc0");

	uint32 allocedPages = Core_Mem_Alloc((void*)0x90000000,0x10000);
	Heap_Init((void*)0x90000000, 0x10000);

	config_clearCallbacks();

	ConfigCallback callback_SERVICE;
	callback_SERVICE.cmd.buffer = "service";
	callback_SERVICE.cmd.length = 7;
	callback_SERVICE.callback = &loadService;
	config_registerCallback(callback_SERVICE);

	Config_read(confString);
//	Core_Process_Create(0, &process2, 0);
	for(;;){
//		delay();
//		Core_Kprint("1");
//		printf("1");
	}

}


void loadCore(String path){
	printf("Loading core ");
	console_outStringLenLn(path.buffer, path.length);
	CPIOFile core = Ramdisk_SearchFile(Ramdisk_Get(), path);

	if (core.size==0){
		printfln("Core not found!");
		for(;;);
	}
	elfHeader* elf = (elfHeader*)core.data;

	elfProgHdr* pheader = (elfProgHdr*)((uint32)core.data+elf->e_phoff);
	uint32 freeBarier = 0;
	for (uint32 i=0;i<elf->e_phnum;i++){
		uint32 pagesNeed = (pheader->p_memsz+0xFFF)>>12;

		printfln("N=%d,Memsz=%d,filesz=%d,va=%h,pn=%d",i,pheader->p_memsz,pheader->p_filesz,pheader->p_vaddr,pagesNeed);
		for (uint32 j=0;j<pagesNeed;j++){
			void* page = Pager_GetPage();
			uint32 vaddr = ((uint32)pheader->p_vaddr+j*0x1000);
			freeBarier = max(freeBarier,vaddr);
			Pager_Mount(page, (void*)((uint32)pheader->p_vaddr+j*0x1000));
		}
		uint32 p_offset = pheader->p_offset+(uint32)elf;
		memcpy((void*)(p_offset), pheader->p_vaddr, pheader->p_filesz);
		pheader++;
	}

	printfln("Entry=%h",elf->e_entry);
	//printfln("barier=%h",freeBarier+0x1000);
	//for(;;);
	uint32 esp = 0;
	asm("movl %esp, %eax");
	asm("movl %%eax, %0":"=m"(esp));
	esp+=0;

	coreInit initFn = elf->e_entry;
	uint32 k42 = initFn((uint32)Pager_GetPage(),ramSize,(void*)(&process1),(void*)esp);
	//printf("42=%d",k42);
	for(;;);
}











