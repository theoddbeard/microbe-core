/*
 * service.c
 *
 *  Created on: 4 февр. 2017 г.
 *      Author: irokez
 */

#include "service.h"
#include "../elf.h"
#include "../console.h"
#include "../cpio.h"
#include "../config/config.h"
#include <corelib/corelib.h>

void Service_Load(void* bin){
	elfHeader* elf = (elfHeader*)bin;
	elfProgHdr* pHeader = (void*) ((uint32)bin+(uint32)elf->e_phoff);
	uint32 spaceId = Core_Space_Create();

	printfln("Offset=%h,phnum=%d,sid=%h,init=%h",elf,elf->e_phnum,spaceId,elf->e_entry);
	for (uint32 i=0;i<elf->e_phnum;i++){

		uint32 pagesNeed = (pHeader->p_memsz+0xFFF)>>12;
		printfln("Poff=%h,Psz=%h,Msz=%h,Pva=%h,n=%h",pHeader->p_offset,pHeader->p_filesz,pHeader->p_memsz,pHeader->p_vaddr,pagesNeed);
		uint32 pagesAlloc = Core_Mem_Alloc((void*)0x80000000, (pHeader->p_memsz+0xFFF));

		uint32 p_offset = pHeader->p_offset+(uint32)elf;
		memcpy((void*)(p_offset), (void*)0x80000000, pHeader->p_filesz);
//		uint32* __volatile__  a = (uint32*)0x80000000;
		//*a = 0x1234abcd;
		uint32 r = Core_Mem_Gift(spaceId,(void*) 0x80000000, pHeader->p_vaddr, pHeader->p_memsz);
		//printf("Gift=%h",r);
		pHeader++;
	}

	//Core_Mem_Alloc((void*)0x80000000, 1);
	//Core_Mem_Gift(spaceId, (void*)0x80000000,  (void*)0xF000000, 0x1000);

	Core_Process_Create(spaceId, elf->e_entry, (void*)0xF000FF0);

}


void callback_exec(String exec){
	CPIOFile f = Ramdisk_SearchFile(Ramdisk_Get(), exec);
	Service_Load(f.data);
}


void Service_Read(CPIOFile* file){

	ConfigParser* parser = Parser_Create();
	ConfigCallback execCallback ={
			.callback = &callback_exec,
	};
	execCallback.cmd.buffer = "exec";
	execCallback.cmd.length = 4;

	String cfgString = {
			.buffer = file->data,
			.length = file->size
	};
	Parser_RegisterCallback(parser,execCallback);
	Parser_Read(parser, cfgString);
	Parser_Destroy(parser);
}




