/*
 * init.c
 *
 *  Created on: 8 февр. 2017 г.
 *      Author: sysadmin
 */

#include <stdlib/stdlib.h>
#include <corelib/corelib.h>

extern void*  __stack;
extern void*  __heap;
extern void*  __heap_end;
extern void main(uint32 argc, char** argv);

__asm__(".global init");
__asm__(".text");
__asm__("init:");
__asm__("movl $__stack  , %eax");
__asm__("movl %eax,%esp");
__asm__("call c__main");

void c__main(){
	void* heap_addr = &__heap;
	void* heap_end_addr = &__heap_end;

	heap_addr = (void*)(((uint32)heap_addr+0xFFF)&0xFFFFF000);
	heap_end_addr = (void*)(((uint32)heap_end_addr+0xFFF)&0xFFFFF000);
	uint32 heap_sz = (uint32) heap_end_addr - (uint32) heap_addr;
	Heap_Init(heap_addr, heap_sz);
	//Core_Mem_Alloc(heap_addr, heap_sz>>12);

	main(0,0);
}

void* getHeap(){
	return &__heap;
}
void* getStack(){
	return &__stack;
}
