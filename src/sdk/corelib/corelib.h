/*
 * corelib.h
 *
 *  Created on: 11 окт. 2016 г.
 *      Author: sysadmin
 */

#ifndef CORELIB_CORELIB_H_
#define CORELIB_CORELIB_H_

#include <stdlib/stdlib.h>
#include <stdlib/types.h>
#ifdef __cplusplus
extern "C" {
#endif


typedef struct {
	uint32 __volatile__ eax;
	uint32 __volatile__ ebx;
	uint32 __volatile__ ecx;
	uint32 __volatile__ edx;
	uint32 __volatile__ esi;
	uint32 __volatile__ edi;
} SyscallResult;


SyscallResult Core_Syscall(uint32 __volatile__ eax, uint32 __volatile__ ebx, uint32 __volatile__ ecx, uint32 __volatile__ edx,uint32 __volatile__ edi, uint32 __volatile__ esi);
void Core_Kprint(const char* string);
void Core_Process_Create(uint32 spaceId,void* entry,void* stack);
void Core_Process_Switch(uint32 processId);

uint32 Core_Space_Create();
uint32 Core_Mem_Gift(uint32 dstSpaceId,void* src,void* dst,uint32 memsz);
uint32 Core_Mem_Alloc(void* target,uint32 count);
uint32 Core_Mem_Release(void* src,uint32 count);
uint32 Core_Mem_Mount(void* physMem,void* target,uint32 count);
uint32 Core_IOPort_Enable(uint32 port,uint32 length);
void Core_TEST_VGA();

void Core_Service_Register(char* name);

//uint32 Core_Message_Send(uint32 processId,uint32 msg,void* msg, uint32 sz);
//uint32 Core_Message_SendAsync(uint32 processId,uint32 msg,void* msg, uint32 sz);
//uint32 Core_Message_Recive(char buffer[]);

#ifdef __cplusplus
}
#endif
#endif /* CORELIB_CORELIB_H_ */
