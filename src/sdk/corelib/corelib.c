/*
 * corelib.c
 *
 *  Created on: 11 окт. 2016 г.
 *      Author: sysadmin
 */

#include "corelib.h"
#include "../../core/syscall/syscalls.h"




void Core_Kprint(const char* string){
	Core_Syscall(0x1, (uint32) string, 0xCC, 0xDD,0,0);
}

void Core_Process_Create(uint32 spaceId,void* entry,void* stack){
	Core_Syscall(SYSCALL_PROCESS_CREATE, spaceId, (uint32)entry, (uint32)stack,0,0);
}

void Core_Process_Switch(uint32 processId){
	Core_Syscall(SYSCALL_PROCESS_SWITCH, processId, 0, 0,0,0);
}

uint32 Core_Space_Create(){
	uint32 spaceId = 0;
	SyscallResult r = Core_Syscall(SYSCALL_SPACE_CREATE, spaceId, 0, 0,0,0);
	return r.eax;
}

uint32 Core_Mem_Gift(uint32 dstSpaceId,void* src,void* dst,uint32 memsz){
	SyscallResult r = Core_Syscall(SYSCALL_MEMORY_GIFT,dstSpaceId,(uint32)src, (uint32) dst, (uint32) memsz,0);
	return r.eax;
}

uint32 Core_Mem_Alloc(void* target,uint32 count){
	SyscallResult r = Core_Syscall(SYSCALL_MEMORY_ALLOC,(uint32)target,count,0,0,0);
	return r.eax;
}

uint32 Core_Mem_Release(void* src,uint32 count){
	SyscallResult r = Core_Syscall(SYSCALL_MEMORY_RELEASE, (uint32) src, count, 0, 0, 0);
	return r.eax;
}

uint32 Core_Mem_Mount(void* physMem,void* target,uint32 count){
	SyscallResult r = Core_Syscall(SYSCALL_MEMORY_MOUNT,(uint32) physMem,(uint32)target,count,0,0);
	return r.eax;
}

uint32 Core_IOPort_Enable(uint32 port,uint32 length){
	SyscallResult r = Core_Syscall(SYSCALL_MEMORY_MOUNT,port,length,0,0,0);
	return r.eax;
}




void Core_Service_Register(char* name){

}
void Core_Service_Unregister(char* name){

}
void Core_Service_Get(char* name){

}
SyscallResult
__attribute__((optimize("-fno-omit-frame-pointer"),cdecl,noinline))
Core_Syscall(uint32 __volatile__ eax, uint32 __volatile__ ebx, uint32 __volatile__ ecx, uint32 __volatile__ edx,uint32 __volatile__ edi, uint32 __volatile__ esi){
	//uint32 eax = 0
	SyscallResult __volatile__ r = {
		.eax = 0,
		.ebx = 0,
		.ecx = 0,
		.edx = 0,
		.esi = 0,
		.edi = 0
};
	asm("pushl %eax");
	asm("pushl %ebx");
	asm("pushl %ecx");
	asm("pushl %edx");
	asm("pushl %esi");
	asm("pushl %edi");
	asm("pushf");

	asm("movl %0, %%eax"::"m"(eax));
	asm("movl %0, %%ebx"::"m"(ebx));
	asm("movl %0, %%ecx"::"m"(ecx));
	asm("movl %0, %%edx"::"m"(edx));
	asm("movl %0, %%esi"::"m"(esi));
	asm("movl %0, %%edi"::"m"(edi));
	asm("int $0x50");

	asm("movl %%eax, %0":"=l"(r.eax));
	asm("movl %%ebx, %0":"=l"(r.ebx));
	asm("movl %%ecx, %0":"=l"(r.ecx));
	asm("movl %%edx, %0":"=l"(r.edx));
	asm("movl %%esi, %0":"=l"(r.esi));
	asm("movl %%edi, %0":"=l"(r.edi));


	asm("popf");
	asm("popl %edi");
	asm("popl %esi");
	asm("popl %edx");
	asm("popl %ecx");
	asm("popl %ebx");
	asm("popl %eax");
	return r;
}






