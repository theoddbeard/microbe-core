#ifndef TYPES_H_
#define TYPES_H_

typedef unsigned int uint32;
typedef int int32;

typedef unsigned short int uint16;
typedef short int int16;

typedef unsigned char uint8;
typedef char int8;

#define NULL (void*)0


#endif /* TYPES_H_ */


