void pager_init(void* start,unsigned int length);
void pager_map(void* vaddr, void* paddr);
void* pager_getRootAddr();
void pager_on();
void* pager_getPage();
