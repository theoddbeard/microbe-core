/*
 * syscalls.c
 *
 *  Created on: 29 янв. 2016 г.
 *      Author: theoddbeard
 */
#include "syscalls.h"
#include "../console.h"


#define SYSCALL_SPACE_CREATE		0x1001
#define SYSCALL_SPACE_ALLOC_MEM		0x1002

#define SYSCALL_PROCESS_CREATE		0x2001
#define SYSCALL_PROCESS_SWITCH		0x2F01  //Process_Switch(uint32 processId) Switch to process

typedef struct{
	unsigned int eax;
	unsigned int ebx;
	unsigned int ecx;
	unsigned int edx;
} SyscallResult;

SyscallResult syscall(unsigned int __volatile__  eax,unsigned int __volatile__  ebx,unsigned int __volatile__  ecx,unsigned int __volatile__  edx){
	SyscallResult __volatile__ result = {0,0,0,0};
	__asm__("movl %0, %%eax"::"a"(eax));
	__asm__("movl %0, %%ebx"::"b"(ebx));
	__asm__("movl %0, %%ecx"::"c"(ecx));
	__asm__("movl %0, %%edx"::"d"(edx));


	__asm__("int $0x50");


	__asm__("movl %%eax,%0":"=a"(result.eax));
	__asm__("movl %%ebx,%0":"=b"(result.ebx));
	__asm__("movl %%ecx,%0":"=c"(result.ecx));
	__asm__("movl %%edx,%0":"=d"(result.edx));

	return result;
}



void syscall_Kprintfln(char* string){
	syscall(0x0F01,(unsigned int)string,0,0);
}

uint32 syscall_Space_Create(){
	SyscallResult sr = syscall(SYSCALL_SPACE_CREATE,0,0,0);
	return sr.eax;
}

uint32 syscall_Space_AllocMem(void* vaddr, uint32 count){
	SyscallResult sr = syscall(SYSCALL_SPACE_ALLOC_MEM,(uint32)vaddr,count,0);
	return sr.eax;
}

uint32 syscall_Process_Create(uint32 dstSpace, void* entryPoint, void* stack){
	SyscallResult sr = syscall(SYSCALL_PROCESS_CREATE,dstSpace,(uint32)entryPoint, (uint32)stack);
	return sr.eax;
}

void syscall_Process_Switch(uint32 processId){
	syscall(SYSCALL_PROCESS_SWITCH,processId,0,0);
}
