/*
 * syscalls.h
 *
 *  Created on: 30 янв. 2016 г.
 *      Author: theoddbeard
 */

#ifndef CORELIB_SYSCALLS_H_
#define CORELIB_SYSCALLS_H_
#include "../types.h"

void syscall_Kprintfln(char* string);
uint32 syscall_Space_Create();
uint32 syscall_Space_AllocMem(void* vaddr, uint32 count);
uint32 syscall_Process_Create(uint32 dstSpace, void* entryPoint, void* stack);
void syscall_Process_Switch(uint32 processId);

#endif /* CORELIB_SYSCALLS_H_ */
