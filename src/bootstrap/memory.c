/*
 * memory.c
 *
 *  Created on: 26 янв. 2016 г.
 *      Author: sysadmin
 */
#include "console.h"

typedef unsigned int uint ;
typedef uint* PageCatalog;

uint rootCat[1024] __attribute__ ((aligned (4096)));
PageCatalog rootCatalog = &rootCat[0];

void* freeMemoryBorder = 0;
void* memSize = 0;


void* pager_getPage(){
	if (freeMemoryBorder+0x1000>memSize) {
		return 0;
	}

	void* page = freeMemoryBorder;
	freeMemoryBorder = freeMemoryBorder+0x1000;
	return page;
}

void initCatalog(PageCatalog catalog){
	for (int i=0;i<1024;i++){
		catalog[i] = 0;
	}
}
unsigned char checkCatalog(unsigned int index){
	return rootCatalog[index]&1;
}



void pager_init(void* start,unsigned int length){
	initCatalog(&rootCatalog[0]);
	freeMemoryBorder = start;
	memSize = (void*)length;

}


void* pager_getRootAddr(){
	return rootCatalog;
}

void pager_map(void* vaddr, void* paddr){

	unsigned int va = (unsigned int)(vaddr)&0xFFFFF000;

	unsigned int pageIndex = ((uint)va & 0x3FF000)>>12;
	unsigned int catalogIndex = (uint)va >>22;

	//printfln("Map %h to %h (%d,%d)",vaddr,paddr,pageIndex,catalogIndex);

	PageCatalog catalog = 0;
	if (!checkCatalog(catalogIndex)){

		catalog = pager_getPage();
		initCatalog(catalog);
		rootCatalog[catalogIndex] = ((uint)catalog&0xFFFFFF000)|0b1111;
		//printfln("Create catalog %d, %h",catalogIndex,catalog);
	}
	catalog = (PageCatalog)((uint)(rootCatalog[catalogIndex]&0xFFFFFF000));


	catalog[pageIndex] = ((uint)paddr&0xFFFFFF000)|0b1111;

}


void pager_on(){
	__asm__("pushl %eax");
	__asm__("movl rootCatalog, %eax");
	__asm__("movl %eax, %cr3");

	__asm__("movl %cr0, %eax");
	__asm__("orl $0x80000000, %eax");
	__asm__("movl %eax, %cr0");
	__asm__("popl %eax");
}

