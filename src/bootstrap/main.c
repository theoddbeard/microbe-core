#include "elf.h"
#include "console.h"
#include "multiboot.h"
#include "memory.h"
#include "core.h"
#include "corelib/syscalls.h"
#include "types.h"

typedef void(*kinit)(unsigned int memsz,void* freemem, void* stackPointer);
unsigned int esp = 0x123;
void init();
void main(multibootInfo* mbInfo){
	console_init(80,25);
	printfln("Starting Microbecore...");

	unsigned int totalMem = mbInfo->mem_lower+mbInfo->mem_upper;

	printfln("Memory: %d kbytes, %d pages",totalMem, totalMem/4);

	unsigned int lastModuleEnd = 0;
	for (unsigned int i=0;i<mbInfo->mods_count;i++){
		moduleInfo* module = &mbInfo->mods_addr[i];
		if (lastModuleEnd<(unsigned int)(module->mod_end)){
			lastModuleEnd = (unsigned int)(module->mod_end);
		}
	}

	unsigned int freeMemory = (lastModuleEnd&0xFFFFF000)+0x1000;
	printfln("Last module ends at %h, free mem at %h",lastModuleEnd,freeMemory);



	pager_init((void*)freeMemory,totalMem*1024);

	unsigned int pagesCount = totalMem/4;
	void* addr = 0;
	for (unsigned int i=0;i<pagesCount;i++){
		addr = (void*)(i*0x1000);
		pager_map(addr,addr);
	}



	printf("Switch on pages... ");
	pager_on();
	printfln("done!");
	moduleInfo* coreInfo = mbInfo->mods_addr;
	printfln(coreInfo->cmdLine);
	unsigned int lastCoreAddr = core_load(coreInfo->mod_start);
	printfln("LastAddr=%h",lastCoreAddr);

//	for(;;);

	__asm__("pushl %eax");
	__asm__("movl %esp,%eax");
	__asm__("movl %%eax, %0":"=l"(esp));
	__asm__("popl %eax");

	core_init(totalMem*1024,pager_getPage(),&init,(void*)esp,(void*) lastCoreAddr);


}
uint32 _delay(){
	uint32 i=0;
	//for (i=0;i<0xFFFFF;i++){
		__asm__("pushl %eax");
		__asm__("pushl %edx");
		__asm__("pushl %ebx");
		__asm__("movl $0xFFFF, %eax");
		__asm__("movl $0xFF, %edx");
		__asm__("mull %ebx");
		__asm__("popl %ebx");
		__asm__("popl %edx");
		__asm__("popl %eax");

	//}
	return i;
}

uint32 delay(){
    uint32 i = 0;
    for (i=0;i<0xfffffff;i++){
	i += _delay();
    }
    return i;
}
void process(){

	syscall_Kprintfln("PROCESS TWO!");
//	syscall_Process_Switch(1);
	for(;;){
			syscall_Kprintfln("PROC TWO");

			delay();
			//__asm__("hlt");
			//__asm__("pushl %eax");
			//__asm__("pushl %edx");
			//__asm__("movb 0b00110011, %al");
			//__asm__("outb %al,$0x3fb");
			//__asm__("movb 0x3e,%al");
			//__asm__("movw 0x278, %dx");
			//__asm__("outb %al,$0x3f8");
			//__asm__("outb %al, %dx");
			//__asm__("popl %edx");
			//__asm__("popl %eax");
			//syscall_Process_Switch(1);
	}
}


void init(){

	syscall_Kprintfln("RING 3!");

	uint32 newSpaceId = syscall_Space_Create();
	printf("NEW SPACE=%h",newSpaceId);
	syscall_Space_AllocMem((void*)0xE0000000,1);
//	uint32 processId = syscall_Process_Create(0x1,(void*)(&process),(void*)0xE0000FF0);
	//printf("PROCID=%h",processId);
	/*
	syscall_Process_Switch(processId);
	syscall_Kprintfln("R3:Switched!");
	syscall_Process_Switch(processId);
	syscall_Kprintfln("Process ZeRO!");
	*/
	//__asm__("hlt");

//	syscall_Process_Switch(2);
	for(;;);
	for(;;){
    
		syscall_Kprintfln("PROC ONE");

		//__asm__("hlt");
		delay();
		//
	}
}








