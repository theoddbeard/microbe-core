/*
 * core.h
 *
 *  Created on: 26 янв. 2016 г.
 *      Author: theoddbeard
 */

unsigned int core_load(void* coreAddr);
void core_init(unsigned int memsz,void* freemem,void* initEntry,void* stackPointer,void* freeKMem);
