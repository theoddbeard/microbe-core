/*
 * core.c
 *
 *  Created on: 26 янв. 2016 г.
 *      Author: theoddbeard
 */
#include "elf.h"
#include "console.h"
#include "memory.h"
#include "core.h"

typedef void(*kinit)(unsigned int memsz,void* freemem,void* initEntry,void* stackPointer,void* freeKMem);
kinit kInit;
void memcpy(char* src, char* dst,unsigned int count){
	for (unsigned int i=0;i<count;i++){
		dst[i] = src[i];
	}
}

unsigned int core_load(void* coreAddr){
	printfln("Loading core");
	elfHeader* header = coreAddr;
	printfln("c=%d",header->e_phnum);
	unsigned int lastAddr = 0;

	for (unsigned int i=0;i<header->e_phnum;i++){
		elfProgHdr* pheader = (elfProgHdr*)(coreAddr+header->e_phoff)+i;

		//printfln("Section poff=%h,voff=%h,fsz=%h,msz=%h",pheader->p_offset,pheader->p_vaddr,pheader->p_filesz,pheader->p_memsz);
		unsigned int pagesNeed = (pheader->p_memsz+0xFFF)>>12;
		unsigned int needToCopy = pheader->p_filesz;

		for (unsigned j=0;j<pagesNeed;j++){
			void* page = pager_getPage();

			if (needToCopy>0){
				unsigned int bytesToCopy = needToCopy;
				if (bytesToCopy>0x1000){
					bytesToCopy = 0x1000;
					needToCopy -= 0x1000;
				} else {
					needToCopy = 0;
				}

				char* src = pheader->p_offset+coreAddr+(j*0x1000);

				memcpy(src,page,bytesToCopy);
				printfln("Copy %d bytes from %h to %h",bytesToCopy,src,page);
			}

			void* vaddr = pheader->p_vaddr+(j*0x1000);
			//printfln("Map %h to %h",page,vaddr);
			pager_map(vaddr,page);
		}

		unsigned int endOfSection = (unsigned int)pheader->p_vaddr+pagesNeed*0x1000;
		if (endOfSection>lastAddr){
			lastAddr = endOfSection;
		}

	}

	printfln("Done");
	kInit = header->e_entry;
	return ((lastAddr+0xFFF)&0xFFFFF000);
}


void core_init(unsigned int memsz,void* freemem,void* initEntry,void* stackPointer,void* freeKMem){

	kInit(memsz,freemem,initEntry,stackPointer,freeKMem);
}


