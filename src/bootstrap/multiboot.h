typedef struct moduleInfo {
	char *mod_start;
	char *mod_end;
	char *cmdLine;
	unsigned long Reserved;
} moduleInfo;


typedef struct multibootInfo {
	unsigned long flags;
	unsigned long mem_lower;
	unsigned long mem_upper;
	unsigned long boot_device;
	char *cmdLine;
	unsigned long mods_count;
	moduleInfo *mods_addr;

} multibootInfo;
