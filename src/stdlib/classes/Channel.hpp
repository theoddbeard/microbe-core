/*
 * Chan.hpp
 *
 *  Created on: 6 февр. 2017 г.
 *      Author: sysadmin
 */

#ifndef CHAN_CHAN_HPP_
#define CHAN_CHAN_HPP_

#include <stdlib/types.h>

typedef struct ChannelHeader{
	uint32 __volatile__ sz;
	uint32 __volatile__ rdptr;
	uint32 __volatile__ wrptr;
	uint32 __volatile__ maxptr;
	uint32 __volatile__ align0;
	uint32 __volatile__ align1;
	uint32 __volatile__ align2;
	uint32 __volatile__ MAGIC;
} ChannelHeader;




class OutputChannel{
public:
	OutputChannel(void* ptr,uint32 sz);
	void init(void* ptr,uint32 sz);
	OutputChannel();
	OutputChannel* operator<<(uint8 c){
		this->write(c);
		return this;
	}
	void write(uint8 c);
private:

	ChannelHeader* hdr;
};

class InputChannel{
public:
	InputChannel(void* ptr,uint32 sz);
	void init(void* ptr,uint32 sz);
	InputChannel();
	InputChannel* operator>>(uint8 c){
		return 0;
	}
	uint8 read();
private:
	ChannelHeader* hdr;

};


class IOChannel{
public:
	IOChannel(void* ptr,uint32 sz);
	uint8 read();
	void write(uint8 c);
private:
	InputChannel in;
	OutputChannel out;
};



#endif /* CHAN_CHAN_HPP_ */
