/*
 * Chan.cpp
 *
 *  Created on: 6 февр. 2017 г.
 *      Author: sysadmin
 */

#ifndef CHAN_CHAN_CPP_
#define CHAN_CHAN_CPP_
#include "Channel.hpp"

#define CHAN_HDR_MAGIC 0xDEADC0DE

OutputChannel::OutputChannel(){

}
OutputChannel::OutputChannel(void* ptr, uint32 sz){
	this->init(ptr, sz);
}

void OutputChannel::init(void* ptr, uint32 sz){
	this->hdr = (ChannelHeader*)ptr;
	if (this->hdr->MAGIC!=CHAN_HDR_MAGIC){
		this->hdr->MAGIC = CHAN_HDR_MAGIC;
		this->hdr->sz = sz-sizeof(ChannelHeader);
		this->hdr->rdptr = (uint32)ptr+sizeof(ChannelHeader);
		this->hdr->wrptr = (uint32)ptr+sizeof(ChannelHeader);
		this->hdr->maxptr = (uint32)ptr+sz;
	}
}

void OutputChannel::write(uint8 c){
	for(;this->hdr->wrptr==this->hdr->rdptr-1;){

	};
	uint8* cp = (uint8*)this->hdr->wrptr;
	*cp = c;

	this->hdr->wrptr+=1;
	if (this->hdr->wrptr>=this->hdr->maxptr){
		this->hdr->wrptr = (uint32)this->hdr+sizeof(ChannelHeader);
	}

}

InputChannel::InputChannel(){

}
InputChannel::InputChannel(void* ptr, uint32 sz){
	this->init(ptr, sz);
}

void InputChannel::init(void* ptr, uint32 sz){
	this->hdr = (ChannelHeader*)ptr;
	if (this->hdr->MAGIC!=CHAN_HDR_MAGIC){
			this->hdr->MAGIC = CHAN_HDR_MAGIC;
			this->hdr->sz = sz-sizeof(ChannelHeader);
			this->hdr->rdptr = (uint32)ptr+sizeof(ChannelHeader);
			this->hdr->wrptr = (uint32)ptr+sizeof(ChannelHeader);
			this->hdr->maxptr = (uint32)ptr+sz;
	}
}


uint8 InputChannel::read(){

	for(;this->hdr->rdptr==this->hdr->wrptr;){

	}
	uint8* c = (uint8*)(this->hdr->rdptr);

	this->hdr->rdptr+=1;

	if (this->hdr->rdptr>=this->hdr->maxptr){
		this->hdr->rdptr = (uint32)this->hdr+sizeof(ChannelHeader);
	}
	return *c;
}



IOChannel::IOChannel(void* ptr, uint32 sz){
	uint32 halfSz = sz/2;
	void* ptrIn = ptr;
	void* ptrOut = (void*)((uint32)ptr+halfSz);
	this->in.init(ptr, halfSz);
	this->out.init(ptrOut, halfSz);
}

#endif /* CHAN_CHAN_CPP_ */
