/*
 * strings.h
 *
 *  Created on: 27 сент. 2016 г.
 *      Author: sysadmin
 */

#ifndef STDLIB_STRINGS_H_
#define STDLIB_STRINGS_H_
#include "types.h"

typedef struct String{
	char* buffer;
	uint32 length;
} String;

String StringCreate(char* buffer);
boolean StringCompare(String str1,String str2);



#endif /* STDLIB_STRINGS_H_ */
