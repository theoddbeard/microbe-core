/*
 * stdlib.h
 *
 *  Created on: 27 сент. 2016 г.
 *      Author: sysadmin
 */

#ifndef STDLIB_H_
#define STDLIB_H_

#ifdef __cplusplus
extern "C" {
#endif


#define asm(CODE)  __asm__ __volatile (CODE)
#define ASM(CODE)  __asm__(CODE)
#include "types.h"
#include "strings.h"
#include "memory.h"
#include "malloc.h"


#ifdef __cplusplus
}
#endif


#endif /* RAMDISK_STDLIB_H_ */
