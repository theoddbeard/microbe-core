/*
 * core-mmap.h
 *
 *  Created on: 19 апр. 2016 г.
 *      Author: sysadmin
 */

#ifndef CORE_MMAP_H_
#define CORE_MMAP_H_


#define CORE_BASE	0xE0000000

#define $PAGES_CATALOG	0xFFC00000
#define $CR3_ADDR		0xFFBFF000


#define $SPACE_CONTEXT				(0xFF800000)
#define $SPACE_MUTEX				$SPACE_CONTEXT+0x1000 //(Item 0x1)

#define $THREAD_CONTEXT				(0xFF400000)
#define $THREAD_TSS_ADDR			$THREAD_CONTEXT
#define $THREAD_KSTACK  			($THREAD_CONTEXT+0x1000) //(Item 0x1)
#define $THREAD_KSTACK_POINTER 		($THREAD_KSTACK+0x0FF0)
#define $THREAD_MUTEX				($THREAD_CONTEXT+0x2000) //(Item 0x2)

#define $THREAD_FOREIGN_CR3			(void*)($THREAD_CONTEXT+0x3000)
#define $THREAD_FOREIGN_3FF			(void*)($THREAD_CONTEXT+0x4000)
#define $THREAD_FOREIGN_CATALOG		(void*)($THREAD_CONTEXT+0x5000)

#define $THREAD_FOREIGN_TSS			(void*)($THREAD_CONTEXT+0x6000)







#endif /* CORE_MMAP_H_ */
