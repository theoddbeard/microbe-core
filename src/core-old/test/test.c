/*
 * test.c
 *
 *  Created on: 27 апр. 2016 г.
 *      Author: sysadmin
 */
#include "test.h"

typedef struct {
	uint32 int1;
	uint32 int2;
	uint32 int3;
	uint32 int4;
}  __attribute__((__packed__)) TestIntStruct;
//uint32* testInt = (uint32*)0xFF200000;
void testPageParamError(void* addr){
	uint32* testInt = (uint32*) addr;
	printfln("ADDR=%h",testInt);
	*testInt = 20;
	printfln("VALUE=%d (%h)",testInt[0],testInt);
}

#pragma GCC push_options
#pragma GCC optimize("O2")
void testPageStructParamError(void* addr){
	//TestIntStruct* __volatile__ test = (TestIntStruct*) 0xff200000;
	uint32* testArray = (uint32*) 0xff200000;
	TestIntStruct* __volatile__ test1 = (TestIntStruct*) 0xff200000;
	testArray[0] = 0x10;
	testArray[1] = 0x20;
	testArray[2] = 0x30;
	testArray[3] = 0x40;

	/*
	test->int1 = 0x10;
	test->int2 = 0x20;
	test->int4 = 0x40;
	test->int3 = 0x30;
	*/
	printfln("ADDR=%h,int1=%h,int2=%h,int3=%h,int4=%h",addr,test1->int1,test1->int2,test1->int3,test1->int4);
}
#pragma GCC pop_options
void testPageError(){
	testPageParamError((void*)0xFF201000);
	testPageStructParamError((void*)0xFF200000);

	for(;;);
}

typedef struct{
	unsigned int eax;
	unsigned int ebx;
	unsigned int ecx;
	unsigned int edx;
} SyscallResult;



SyscallResult syscall(unsigned int __volatile__  eax,unsigned int __volatile__  ebx,unsigned int __volatile__  ecx,unsigned int __volatile__  edx){
	SyscallResult __volatile__ result = {0,0,0,0};
	__asm__("movl %0, %%eax"::"a"(eax));
	__asm__("movl %0, %%ebx"::"b"(ebx));
	__asm__("movl %0, %%ecx"::"c"(ecx));
	__asm__("movl %0, %%edx"::"d"(edx));


	__asm__("int $0x50");


	__asm__("movl %%eax,%0":"=a"(result.eax));
	__asm__("movl %%ebx,%0":"=b"(result.ebx));
	__asm__("movl %%ecx,%0":"=c"(result.ecx));
	__asm__("movl %%edx,%0":"=d"(result.edx));

	return result;
}

void testSyscall(){
/*
	__asm__("pushl %eax");
	__asm__("pushl %ebx");
	__asm__("pushl %ecx");
	__asm__("pushl %edx");

	__asm__("movl $0xA000, %eax");
	__asm__("movl $0xB000, %ebx");
	__asm__("movl $0xC000, %ecx");
	__asm__("movl $0xD000, %edx");

	__asm__("int $0x50");

	__asm__("popl %edx");
	__asm__("popl %ecx");
	__asm__("popl %ebx");
	__asm__("popl %eax");
*/
	SyscallResult __volatile__ r = syscall(0xAAA0,0xBBBB,0xCCCC,0xDDDD);
	printfln("SYSCALL_RESULT=%h %h %h %h",r.eax,r.ebx,r.ecx,r.edx);
	SyscallResult __volatile__ r1 = syscall(0xAAA1,0xBBBB,0xCCCC,0xDDDD);
	printfln("SYSCALL_RESULT=%h %h %h %h",r1.eax,r1.ebx,r1.ecx,r1.edx);
	SyscallResult __volatile__ r2 = syscall(0xAAA2,0xBBBB,0xCCCC,0xDDDD);
	printfln("SYSCALL_RESULT=%h %h %h %h",r2.eax,r2.ebx,r2.ecx,r2.edx);
	//for(;;);
}



