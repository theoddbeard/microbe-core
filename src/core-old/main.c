#include "types.h"
#include "core-mmap.h"
#include "console.h"
#include "memory/pager.h"
#include "tables/gdt.h"
#include "tables/idt.h"
#include "tables/tss.h"
#include "hardware/pic.h"
#include "hardware/timer.h"
#include "memory/malloc.h"
#include "syscalls/syscalls.h"
#include "process/space.h"
#include "process/process.h"

#include "test/test.h"



#pragma pack(1)

typedef struct {
	void* addr;
	unsigned short segment;
} LCallPtr;
LCallPtr lcallPtr __attribute__ ((aligned(16)));
#pragma pack(0)

void GPHandler(INTPARAMS){
	printfln("#GP %h, %h:%h",errcode,segment,addr);
	__asm__("hlt");
}


uint32 esp = 0;
void* entryPoint;


void init(unsigned int memsz,void* freeMem, void* initEntry, uint32 stackPointer, void* freeKMem){
	console_init(80,25);
	printfln("Core init...%h,stack=%h,kmem=%h",freeMem,stackPointer,freeKMem);
	uint32 dirtyPages = (uint32)freeMem>>12;
	pager_init(dirtyPages);

	void* kstack_page = pager_reservePage();
	pager_mount(kstack_page,(void*)$THREAD_KSTACK);


	GDT_init();
	IDT_init();
	PIC_init();

	IDT_setIntHandler(&GPHandler,0xD);
	IDT_setIntHandler(getInt0xEHandler(),0xE);

	//testPageError();

	printfln("Init TSS");
	uint32* __volatile__ tss_addr = (uint32*)$THREAD_TSS_ADDR;
	TSS_init(tss_addr);

	printfln("Append TSS");
	GDT_AppendTSS(TSS_get_addr(),TSS_get_size());

	printfln("Flush TSS");
	TSS_flush();


	printfln("Init HEAP");
	heap_init(freeKMem,1024*1024*64);

	IDT_setHandler(0x50,0x8,SYSCALL_getIntHandler(),INT_HANDLER_INT);

	space_init();
	process_init();
	esp = stackPointer;
	entryPoint = initEntry;

	ASM("sti");

	Timer_Init();
	IDT_setIntHandler(Timer_GetInt0Handler(),0x20);
	PIC_enable_irq(0x0);
	//for(;;);
	__asm__("movw $0x23, %ax");
	__asm__("movw %ax, %ds");
	__asm__("movw %ax, %es");
	__asm__("movw %ax, %fs");
	__asm__("movw %ax, %gs");

	__asm__("movl %0,%%eax"::"l"(esp));
	__asm__("pushl $0x23");
	__asm__("pushl %eax");
	__asm__("pushf");
	__asm__("movl %0,%%eax"::"l"(entryPoint));
	__asm__("pushl $0x1B");
	__asm__("pushl %eax");

	__asm__("iret");

	for(;;);
}











