/*
 * gdt.h
 *
 *  Created on: 27 янв. 2016 г.
 *      Author: theoddbeard
 */

void GDT_init();
void GDT_AppendTSS(void* TSSaddr, unsigned int TSSsize);
