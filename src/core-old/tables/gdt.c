/*
 * gdt.c
 *
 *  Created on: 27 янв. 2016 г.
 *      Author: theoddbeard
 */
#include "../console.h"
#include "../types.h"
#pragma pack(1)
typedef struct{
	unsigned char byte0;
	unsigned char byte1;
	unsigned char byte2;
	unsigned char byte3;
	unsigned char byte4;
	unsigned char byte5;
	unsigned char byte6;
	unsigned char byte7;
} __attribute__((packed)) GDT_Entry;


struct{
	unsigned short size;
	void* gdtAddt;
} LGDT_arg;

#pragma pack(0)

GDT_Entry GDT[6] __attribute__ ((aligned (4096)));



void GDT_init(){
	//RING0 CODE 0x8

	GDT[1].byte0 = 0xFF;
	GDT[1].byte1 = 0xFF;
	GDT[1].byte2 = 0x00;
	GDT[1].byte3 = 0x00;
	GDT[1].byte4 = 0x00;
	GDT[1].byte5 = 0b10011010;
	GDT[1].byte6 = 0b11001111;
	GDT[1].byte7 = 0x00;

	//RING0 DATA 0x10
	GDT[2].byte0 = 0xFF;
	GDT[2].byte1 = 0xFF;
	GDT[2].byte2 = 0x00;
	GDT[2].byte3 = 0x00;
	GDT[2].byte4 = 0x00;
	GDT[2].byte5 = 0b10010010;
	GDT[2].byte6 = 0b11001111;
	GDT[2].byte7 = 0x00;

	//RING3 CODE 0x1B

	GDT[3].byte0 = 0xFF;
	GDT[3].byte1 = 0xFF;
	GDT[3].byte2 = 0x00;
	GDT[3].byte3 = 0x00;
	GDT[3].byte4 = 0x00;
	GDT[3].byte5 = 0b11111010;
	GDT[3].byte6 = 0b11001111;
	GDT[3].byte7 = 0x00;

	//RING3 DATA 0x23
	GDT[4].byte0 = 0xFF;
	GDT[4].byte1 = 0xFF;
	GDT[4].byte2 = 0x00;
	GDT[4].byte3 = 0x00;
	GDT[4].byte4 = 0x00;
	GDT[4].byte5 = 0b11110010;
	GDT[4].byte6 = 0b11001111;
	GDT[4].byte7 = 0x00;


	LGDT_arg.gdtAddt = &GDT;
	LGDT_arg.size = sizeof(GDT)-1;
	//printfln("GDT %h %d",LGDT_arg.gdtAddt,LGDT_arg.size);
	__asm__("lgdt LGDT_arg");

}

void GDT_AppendTSS(void* TSSaddr, unsigned int TSSsize){
	printfln("TSS offset=%h, size=%h",TSSaddr,TSSsize);
	//TSS Segment 0x2B
	GDT[5].byte7 = ((uint32)TSSaddr>>24)&0xFF;
	//GDT[5].byte6 = 0b10000000;
	GDT[5].byte6 = 0b01000000|((TSSsize>>16)&0xF);
	//GDT[5].byte5 = 0b11101001;
	GDT[5].byte5 = 0b11101001;
	GDT[5].byte4 = ((uint32)TSSaddr>>16)&0xFF;
	GDT[5].byte3 = ((uint32)TSSaddr>>8)&0xFF;
	GDT[5].byte2 = (uint32)TSSaddr&0xFF;
	GDT[5].byte1 = (TSSsize>>8)&0xFF;
	GDT[5].byte0 = (TSSsize)&0xFF;
	printfln("%h %h %h %h %h %h %h %h",GDT[5].byte7,GDT[5].byte6,GDT[5].byte5,GDT[5].byte4,GDT[5].byte3,GDT[5].byte2,GDT[5].byte1,GDT[5].byte0);

	/* 0x00
	 * 0x40
	 * 0x8b
	 * 0x00
	 * 0x00
	 * 0x00
	 * 0xff
	 * 0xff
	 */
	// 0x00408b00, dl=0x0000ffff

}



