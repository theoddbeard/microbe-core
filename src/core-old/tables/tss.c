/*
 * tss.c
 *
 *  Created on: 27 янв. 2016 г.
 *      Author: sysadmin
 */


#include "tss.h"
#include "../types.h"
#include "../console.h"

/*

+00h: 4 байта - селектор предыдущей задачи (старшее слово содержит нули -
здесь и для всех остальных селекторов)
+04h: 4 байта - ESP для CPL = О
+08h: 4 байта - SS для CPL = О
+OCh: 4 байта - ESP для CPL = 1
+10h: 4 байта - SS для CPL = 1
+14h: 4 байта - ESP для CPL = 2
+ 18h: 4 байта - SS для CPL = 2
+lCh: 4байта-СRЗ
+20h: 4байта-ЕIР
+24h: 4 байта - EFLAGS
+28h: 4байта-ЕАХ
+2Ch: 4 байта - ЕСХ
+30h: 4 байта - EDX
+34h: 4байта-ЕВХ
+38h: 4 байта-ESP
+3Ch: 4 байта - EBP
+40h: 4 байта - ESI
+44h: 4 байта-EDI
+48h: 4 байта - ES
+4Ch: 4 байта - CS
+50h: 4 байта - SS
+54h: 4 байта - DS
-f 58h: 4 байта - FS
+5Ch: 4 байта - GS
+60h: 4 байта - LDTR
+64h: 2 байта - слово флагов задачи
 */

typedef struct {
	__volatile__ unsigned int __volatile__ previous_tss;
	__volatile__ unsigned int __volatile__ ESP0;
	__volatile__ unsigned int __volatile__ SS0;
	unsigned int EPS1;
	unsigned int SS1;
	unsigned int ESP2;
	unsigned int SS2;
	unsigned int CR3;
	unsigned int EIP;
	unsigned int EFLASGS;
	unsigned int EAX;
	unsigned int ECX;
	unsigned int EDX;
	unsigned int EBX;
	__volatile__  unsigned int __volatile__ ESP; //56
	unsigned int EBP;
	unsigned int ESI;
	unsigned int EDI;
	unsigned int ES;
	unsigned int CS;
	unsigned int SS;
	unsigned int DS;
	unsigned int FS;
	unsigned int GS;
	unsigned int LDTR;
	unsigned int tss_flags;
	unsigned int iomap_base;
}   __attribute__ ((__packed__)) TSS;


TSS* tss;

void TSS_init(uint32* __volatile__ tss_addr){
	tss = (TSS*) $THREAD_TSS_ADDR;
	//TSS*  atss = (TSS*) $THREAD_TSS_ADDR;
	//TSS* __volatile__ atss = (TSS*) tss_addr;
	uint32* __volatile__ atss =  (uint32*) tss_addr;
	atss[0] = 0;
	atss[1] = (uint32)$THREAD_KSTACK_POINTER;
	atss[2] = 0x10;

	printfln("Addr=%h,size=%h SS0=%h, ESP0=%h",tss_addr,atss, tss->SS0,tss->ESP0);

}

void  TSS_init_foreign(void* __volatile__ tss_addr, uint32 __volatile__ esp){
	TSS* __volatile__ t = (TSS*) tss_addr;
	uint32* __volatile__ atss = (uint32*) tss_addr;
	atss[0] = 0;
	atss[1] = (uint32)$THREAD_KSTACK_POINTER;
	atss[2] = 0x10;
	atss[14] = esp;

	printfln("FTSS=%h,SS0=%h,SP0=%h,SP=%h",t,t->SS0,t->ESP0,t->ESP);

	/*
	atss->SS0 = 0x10;
	atss->ESP0 = (uint32)$THREAD_KSTACK_POINTER;
	atss->ESP = esp;
	*/
}
void TSS_Stat(){
	//tss = (TSS*)0xFF400000;
	//printfln("Addr=%h,size=%h SS0=%h, ESP0=%h",(uint32)tss,sizeof(TSS), tss->SS0,tss->ESP0);
}
void TSS_flush(){
	__asm__("pushl %eax");
	__asm__("movw $0x2B,%ax");
	__asm__("ltr %ax");
	__asm__("popl %eax");
}

void* TSS_get_addr(){
	return tss;
}

uint32 TSS_get_size(){
	return sizeof(TSS);
}




