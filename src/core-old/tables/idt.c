/*
 * idt.c
 *
 *  Created on: 27 янв. 2016 г.
 *      Author: theoddbeard
 */
#include "../types.h"
#include "idt.h"
#include "idt-handlers.inc"
#include "../console.h"

#pragma pack(1)

typedef struct {
	unsigned char byte0;
	unsigned char byte1;
	unsigned char byte2;
	unsigned char byte3;
	unsigned char byte4;
	unsigned char byte5;
	unsigned char byte6;
	unsigned char byte7;
} IDTEntry;

struct {
	unsigned short size;
	void* addr;
} LIDT_arg;

#pragma pack(0)

IDTEntry IDT[256];



IntHandler intHandlers[256];



void IDT_init(){
	for(uint32 i=0;i<256;i++){
		IDT[i].byte0 = 0x00;
		IDT[i].byte1 = 0x00;
		IDT[i].byte2 = 0x00;
		IDT[i].byte3 = 0x00;
		IDT[i].byte4 = 0x00;
		IDT[i].byte5 = 0x00;
		IDT[i].byte6 = 0x00;
		IDT[i].byte7 = 0x00;

		intHandlers[i] = 0;
	}

	LIDT_arg.size = sizeof(IDT)-1;
	LIDT_arg.addr = &IDT;

	__asm__("lidt LIDT_arg");

	IDT_setHandler(0,0x8,int_0,INT_HANDLER_TRAP);
	IDT_setHandler(1,0x8,int_1,INT_HANDLER_TRAP);
	IDT_setHandler(2,0x8,int_2,INT_HANDLER_TRAP);
	IDT_setHandler(3,0x8,int_3,INT_HANDLER_TRAP);
	IDT_setHandler(4,0x8,int_4,INT_HANDLER_TRAP);
	IDT_setHandler(5,0x8,int_5,INT_HANDLER_TRAP);
	IDT_setHandler(6,0x8,int_6,INT_HANDLER_TRAP);
	IDT_setHandler(7,0x8,int_7,INT_HANDLER_TRAP);
	IDT_setHandler(8,0x8,int_8,INT_HANDLER_TRAP);
	IDT_setHandler(9,0x8,int_9,INT_HANDLER_TRAP);
	IDT_setHandler(0xA,0x8,int_0xA,INT_HANDLER_TRAP);
	IDT_setHandler(0xB,0x8,int_0xB,INT_HANDLER_TRAP);
	IDT_setHandler(0xC,0x8,int_0xC,INT_HANDLER_TRAP);
	IDT_setHandler(0xD,0x8,int_0xD,INT_HANDLER_TRAP);
	IDT_setHandler(0xE,0x8,int_0xE,INT_HANDLER_INT);
	IDT_setHandler(0xF,0x8,int_0xF,INT_HANDLER_TRAP);
	IDT_setHandler(0x10,0x8,int_0x10,INT_HANDLER_TRAP);
	IDT_setHandler(0x11,0x8,int_0x11,INT_HANDLER_TRAP);
	IDT_setHandler(0x12,0x8,int_0x12,INT_HANDLER_TRAP);

	//PIC1
	IDT_setHandler(0x20,0x8,int_0x20,INT_HANDLER_TRAP);
	IDT_setHandler(0x21,0x8,int_0x21,INT_HANDLER_TRAP);
	IDT_setHandler(0x22,0x8,int_0x22,INT_HANDLER_INT);
	IDT_setHandler(0x23,0x8,int_0x23,INT_HANDLER_INT);
	IDT_setHandler(0x24,0x8,int_0x24,INT_HANDLER_INT);
	IDT_setHandler(0x25,0x8,int_0x25,INT_HANDLER_INT);
	IDT_setHandler(0x26,0x8,int_0x26,INT_HANDLER_INT);
	IDT_setHandler(0x27,0x8,int_0x27,INT_HANDLER_INT);

	//PIC2
	IDT_setHandler(0x28,0x8,int_0x28,INT_HANDLER_INT);
	IDT_setHandler(0x29,0x8,int_0x29,INT_HANDLER_INT);
	IDT_setHandler(0x2A,0x8,int_0x2A,INT_HANDLER_INT);
	IDT_setHandler(0x2B,0x8,int_0x2B,INT_HANDLER_INT);
	IDT_setHandler(0x2C,0x8,int_0x2C,INT_HANDLER_INT);
	IDT_setHandler(0x2D,0x8,int_0x2D,INT_HANDLER_INT);
	IDT_setHandler(0x2E,0x8,int_0x2E,INT_HANDLER_INT);
	IDT_setHandler(0x2F,0x8,int_0x2F,INT_HANDLER_INT);


}


void IDT_setHandler(unsigned char intNumber, unsigned short segment,void* handler,uint8 handlerType){
	IDT[intNumber].byte7 = (unsigned int)handler>>24;
	IDT[intNumber].byte6 = ((unsigned int)handler>>16)&0xFF;
	IDT[intNumber].byte5 = 0b11100000|handlerType;
	IDT[intNumber].byte4 = 0;
	IDT[intNumber].byte3 = 0;
	IDT[intNumber].byte2 = segment&0xFF;
	IDT[intNumber].byte1 = ((unsigned int)handler>>8)&0xFF;
	IDT[intNumber].byte0 = (unsigned int)handler&0xFF;
}


void IDT_setIntHandler(void* addr, uint32 intn){
	intHandlers[intn] = addr;
}


void int_handler(INTPARAMS){
	//__asm__("pusha");

	__asm__("pushl %eax");
	__asm__("pushl %ebx");
	__asm__("pushl %ecx");
	__asm__("pushl %edx");
	__asm__("pushl %esi");
	__asm__("pushl %edi");


	
	if (intHandlers[intn]!=0){
		intHandlers[intn](intn,errcode,addr,segment,addrE);
		//printfln("[Int %h,Error=%h,Addr=%h:%h,%h]",intn,errcode,segment,addr,addrE);
	} else {
		printfln("[Int %h,Error=%h,Addr=%h:%h]",intn,errcode,segment,addr);
		__asm__("hlt");
	}

	__asm__("sti");
	__asm__("popl %edi");
	__asm__("popl %esi");
	__asm__("popl %edx");
	__asm__("popl %ecx");
	__asm__("popl %ebx");
	__asm__("popl %eax");

	//__asm__("popa");

	//for(;;);
}

