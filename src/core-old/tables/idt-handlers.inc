
#define INTHANDLER(num) __asm__("int_"#num":\r\n cli\r\n pushf\r\n pusha\r\n pushl $0xABCD \r\n pushl $"#num"\r\n call int_handler\r\n addl $8, %esp \r\n popa\r\n popf\r\n sti\r\n iret");
#define INTHANDLER_ERRCODE(num) __asm__("int_"#num":\r\n \r\n pushl $"#num"\r\n call int_handler\r\n addl $8, %esp \r\n  \r\n iret");





INTHANDLER(0);
INTHANDLER(1);
INTHANDLER(2);
INTHANDLER(3);
INTHANDLER(4);
INTHANDLER(5);
INTHANDLER(6);
INTHANDLER(7);
INTHANDLER(8);
INTHANDLER(9);
INTHANDLER_ERRCODE(0xA);
INTHANDLER_ERRCODE(0xB);
INTHANDLER_ERRCODE(0xC);
INTHANDLER_ERRCODE(0xD);
INTHANDLER_ERRCODE(0xE);
INTHANDLER(0xF);
INTHANDLER(0x10);
INTHANDLER_ERRCODE(0x11);
INTHANDLER(0x12);
INTHANDLER(0x13);
INTHANDLER(0x14);
INTHANDLER(0x15);
INTHANDLER(0x16);
INTHANDLER(0x17);
INTHANDLER(0x18);
INTHANDLER(0x19);
INTHANDLER(0x1A);
INTHANDLER(0x1B);
INTHANDLER(0x1C);
INTHANDLER(0x1D);
INTHANDLER(0x1E);
INTHANDLER(0x1F);


//PIC1
INTHANDLER(0x20);
INTHANDLER(0x21);
INTHANDLER(0x22);
INTHANDLER(0x23);
INTHANDLER(0x24);
INTHANDLER(0x25);
INTHANDLER(0x26);
INTHANDLER(0x27);

//PIC2
INTHANDLER(0x28);
INTHANDLER(0x29);
INTHANDLER(0x2A);
INTHANDLER(0x2B);
INTHANDLER(0x2C);
INTHANDLER(0x2D);
INTHANDLER(0x2E);
INTHANDLER(0x2F);


void int_0();
void int_1();
void int_2();
void int_3();
void int_4();
void int_5();
void int_6();
void int_7();
void int_8();
void int_9();
void int_0xA();
void int_0xB();
void int_0xC();
void int_0xD();
void int_0xE();
void int_0xF();
void int_0x10();
void int_0x11();
void int_0x12();
void int_0x13();
void int_0x14();
void int_0x15();
void int_0x16();
void int_0x17();
void int_0x18();
void int_0x19();
void int_0x1A();
void int_0x1B();
void int_0x1C();
void int_0x1D();
void int_0x1E();
void int_0x1F();

void int_0x20();
void int_0x21();
void int_0x22();
void int_0x23();
void int_0x24();
void int_0x25();
void int_0x26();
void int_0x27();

void int_0x28();
void int_0x29();
void int_0x2A();
void int_0x2B();
void int_0x2C();
void int_0x2D();
void int_0x2E();
void int_0x2F();


