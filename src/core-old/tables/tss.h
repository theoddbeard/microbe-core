/*
 * tss.h
 *
 *  Created on: 27 янв. 2016 г.
 *      Author: sysadmin
 */

//#ifndef TABLES_TSS_H_
//#define TABLES_TSS_H_
#include "../core-mmap.h"
#include "../types.h"
#define TSS_ADDR  (void*)$THREAD_CONTEXT

void TSS_init(uint32* __volatile__ tss_addr);
void  TSS_init_foreign(void* __volatile__ tss_addr, uint32 __volatile__ esp);
void TSS_flush();
void* TSS_get_addr();
uint32 TSS_get_size();

void TSS_Stat();


//#endif /* TABLES_TSS_H_ */
