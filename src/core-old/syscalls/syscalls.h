/*
 * syscalls.h
 *
 *  Created on: 28 янв. 2016 г.
 *      Author: sysadmin
 */

#ifndef SYSCALLS_SYSCALLS_H_
#define SYSCALLS_SYSCALLS_H_

#pragma pack(1)
typedef struct{
	unsigned int eax;
	unsigned int ebx;
	unsigned int ecx;
	unsigned int edx;
} SyscallResult;
#pragma pack(0)
//DEBUG calls

#define SYSCALL_KPRINTFLN 		0x0F01	// kprintfln

// SPACE syscalls
#define SYSCALL_SPACE_CREATE 		0x1001	//Space_Create()
#define SYSCALL_SPACE_DESTROY		0x1002	//Space_Destroy(uint32 spaceId)
#define SYSCALL_SPACE_ALLOC_MEM		0x1002  //allocPages(void* vaddr, uint32 count)
//PROCESS syscalls
#define SYSCALL_PROCESS_CREATE		0x2001  //createProcess(uint32 dstSpace, void* entryPoint, void* stackPoint)
#define SYSCALL_PROCESS_START		0x2002	//startProcess
#define SYSCALL_PROCESS_PAUSE		0x2003	//pauseProcess
#define SYSCALL_PROCESS_DESTROY		0x2004	//destroyProcess

#define SYSCALL_PROCESS_SWITCH		0x2F01  //Process_Switch(uint32 processId) Switch to process

//INT syscalls
//#define 0x3001	addGlobalInt
//#define 0x3002	removeGlobalInt
//#define 0x3003	addLocalInt
//#define 0x3004	removeLocalInt

//KMOD syscalls
//#define 0x4001	defineKmod
//#define 0x4002	loadKmod
//#define 0x4003	unloadKmod


void* SYSCALL_getIntHandler();

#endif /* SYSCALLS_SYSCALLS_H_ */
