/*
 * syscalls.c
 *
 *  Created on: 28 янв. 2016 г.
 *      Author: sysadmin
 */
#include "../types.h"
#include "../console.h"
#include "syscalls.h"
#include "../process/space.h"
#include "../process/process.h"

//Syscalls int 0x40



void syscalls_int_handler();
__asm__ ("syscalls_int_handler:");
__asm__ ("pusha");
__asm__ ("pushl %edx");
__asm__ ("pushl %ecx");
__asm__ ("pushl %ebx");
__asm__ ("pushl %eax");
__asm__ ("call syscalls_dispatcher");

__asm__ ("popl %eax");
__asm__ ("popl %ebx");
__asm__ ("popl %ecx");
__asm__ ("popl %edx");
__asm__ ("popa");
//__asm__("addl $16,%esp");

__asm__ ("iret");

void* SYSCALL_getIntHandler(){
	return &syscalls_int_handler;
}


__attribute__((noinline,cdecl))SyscallResult syscall_KPrintfln(char* string){
	SyscallResult sr = {0,0,0,0};
	printfln(string);
	return sr;
};

__attribute__((noinline,cdecl))SyscallResult syscall_Space_Create(){
	SyscallResult sr = {0,0,0,0};
	sr.eax = space_create();
	return sr;
}

__attribute__((noinline,cdecl))SyscallResult syscall_Space_AllocPages(uint32 vaddr, uint32 count){
	SyscallResult sr = {0,0,0,0};
	sr.eax = space_alloc((void*)vaddr,count);
	return sr;
}



__attribute__((noinline,cdecl))SyscallResult syscall_Process_Create(uint32 dstSpace, uint32 entryPoint, uint32 stackPointer){
	SyscallResult sr = {0,0,0,0};
	sr.eax = process_create(dstSpace,(void*) entryPoint, (void*) stackPointer);
	return sr;
}

__attribute__((noinline,cdecl))SyscallResult syscall_Process_Switch(uint32 targetProcess){
	SyscallResult sr = {0,0,0,0};
	process_switch(targetProcess);
	return sr;
}

void __attribute__((cdecl)) syscalls_dispatcher(uint32 __volatile__ eax,uint32 __volatile__ ebx, uint32 __volatile__ ecx, uint32 __volatile__ edx){

	//printfln("SYSCALL %h %h,%h,%h",eax,ebx,ecx,edx);
	SyscallResult sr = {0,0,0,0};
	switch(eax){
	case SYSCALL_KPRINTFLN:sr = syscall_KPrintfln((char*)ebx);break;
	case SYSCALL_SPACE_CREATE:sr = syscall_Space_Create();break;
	case SYSCALL_SPACE_ALLOC_MEM:sr = syscall_Space_AllocPages(ebx,ecx);break;
	case SYSCALL_PROCESS_CREATE:sr = syscall_Process_Create(ebx,ecx,edx);break;
	case SYSCALL_PROCESS_SWITCH:sr = syscall_Process_Switch(ebx);break;
	default:printfln("SYSCALL %h %h,%h,%h",eax,ebx,ecx,edx);break;
	}
	//printfln("SR %h %h %h %h",sr.eax,sr.ebx,sr.ecx,sr.edx);
	eax = sr.eax;
	ebx = sr.ebx;
	ecx = sr.ecx;
	edx = sr.edx;

	return;

}




