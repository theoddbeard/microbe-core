/*
 * pager.c
 *
 *  Created on: 12 апр. 2016 г.
 *      Author: sysadmin
 */

#include "../types.h"
#include "../console.h"
#include "pager.h"
#include "../core-mmap.h"
#include "../tables/idt.h"


#define MAKE_PAGE_ENTRY(ADDR) ((uint32)ADDR&0xFFFFF000)|0b11111
#define GET_CATALOG_INDEX(ADDR) (uint32)ADDR>>22
#define GET_PAGE_INDEX(ADDR) (((uint32)ADDR>>12)&0x3FF)

uint8 mmap[1024*1024] __attribute__ ((aligned (8)));

#define CR3 ((PageCatalog)$CR3_ADDR)
#define CATALOG(INDEX) ((PageCatalog)($PAGES_CATALOG+(INDEX<<12)))
#define CATALOGS CATALOG(0x3FF)



typedef uint32* PageCatalog;


void init_mmap(uint32 dirtyPages){
	for (uint32 i=0;i<1024*1024;i++){
		if (i>=dirtyPages){
			mmap[i] = 0;
		} else {
			mmap[i] = 1;
		}
	}
}

void* pager_reservePage(){
	for (uint32 i=0;i<1024*1024;i++){
		if (mmap[i]==0){
			mmap[i]++;
			return (void*)(i<<12);
		}
	}
	printfln("OUT OF MEMORY!");
	ASM("hlt");
	return 0;
}

void* CMPreservePage(){
	void* page = pager_reservePage();
	for (uint32 i=0;i<1024;i++){
		if (CR3[i]==(uint32)page){
			printfln("EXISTS!");
			ASM("hlt");
		}
	}
	return page;
}


void mount_catalog(void* catalogAddr, uint32 index){
	CR3[index] = MAKE_PAGE_ENTRY(catalogAddr);
	CATALOGS[index] = MAKE_PAGE_ENTRY(catalogAddr);
}

void pager_init(uint32 dirtyPages){
	printfln("Init pager");
	init_mmap(dirtyPages);
	PageCatalog cr3 = 0;
	ASM("pushl %eax");
	ASM("movl %cr3, %eax");
	ASM("movl %%eax,%0":"=l"(cr3));
	ASM("popl %eax");

	PageCatalog entry3FF = pager_reservePage();
	PageCatalog entry3FE = pager_reservePage();
	PageCatalog entry3FD = pager_reservePage();
	printfln("cr3=%h",cr3);
	printfln("3FF=%h",entry3FF);


	cr3[0x3FF] = MAKE_PAGE_ENTRY(entry3FF);
	cr3[0x3FE] = MAKE_PAGE_ENTRY(entry3FE);
	cr3[0x3FD] = MAKE_PAGE_ENTRY(entry3FD);

	for (uint32 i=0;i<1024;i++){
		entry3FF[i] = cr3[i];
	}

	entry3FE[0x3FF] = MAKE_PAGE_ENTRY(cr3);

}


PageCatalog getCatalog(uint32 catalogIndex){
	if ((CR3[catalogIndex]&1)==0){
		//Нет каталога для такого адреса
		//Создаем и монтируем
		//printfln("CREATE CATALOG %h",catalogIndex);
		PageCatalog newCatalog = pager_reservePage();
		mount_catalog(newCatalog,catalogIndex);
		return CATALOG(catalogIndex);
		//return newCatalog;
	} else {
		return CATALOG(catalogIndex);
	}
}

void pager_mount(void* __volatile__ paddr, void* __volatile__ vaddr){
	//printfln("MOUNT %h to %h",paddr,vaddr);
	//uint32 catalogIndex = GET_CATALOG_INDEX(vaddr);
	uint32 __volatile__ pageIndex = GET_PAGE_INDEX(vaddr);

	PageCatalog __volatile__ catalog = getCatalog(GET_CATALOG_INDEX(vaddr));
	//printfln("Catalog %h",catalog);
	catalog[pageIndex] = MAKE_PAGE_ENTRY(paddr);
	ASM("pushl %eax");
	ASM("movl %cr3, %eax");
	ASM("movl %eax, %cr3");
	ASM("popl %eax");
	//printfln("RESULT %h",catalog[pageIndex]);
}

void pager_unmount(void* __volatile__ vaddr){
	uint32 __volatile__ catalogIndex = GET_CATALOG_INDEX(vaddr);
	uint32 __volatile__ pageIndex = GET_PAGE_INDEX(vaddr);
	PageCatalog __volatile__ catalog = CATALOG(catalogIndex);
	if (catalog==0){
		printfln("CANT UNMOUNT");
		return;
	}
	catalog[pageIndex] = 0;

	ASM("pushl %eax");
	ASM("movl %cr3, %eax");
	ASM("movl %eax, %cr3");
	ASM("popl %eax");
}


void* pager_getCurrentCR3(){
	return (void*)(CATALOG(0x3FE)[0x3FF]&0xFFFFF000);
}

void* pager_getCurrentCatalog(uint32 catalogIndex){
	return (void*)(CATALOGS[catalogIndex]&0xFFFFF000);
}




void pager_mountToForeign(void* cr3, void* paddr,void* vaddr){
	pager_mount(cr3,$THREAD_FOREIGN_CR3);
	PageCatalog foreignCR3 = (PageCatalog)$THREAD_FOREIGN_CR3;

	pager_mount((void*)(foreignCR3[0x3FF]&0xFFFFF000),$THREAD_FOREIGN_3FF);
	PageCatalog foreign3FF = (PageCatalog)$THREAD_FOREIGN_3FF;

	uint32 catalogIndex = GET_CATALOG_INDEX(vaddr);
	uint32 pageIndex = GET_PAGE_INDEX(vaddr);

	if ((foreignCR3[catalogIndex]&0x1)==0){
		void* newCatalog = pager_reservePage();
		uint32 newCatalogEntry = MAKE_PAGE_ENTRY(newCatalog);
		foreignCR3[catalogIndex] = newCatalogEntry;
		foreign3FF[catalogIndex] = newCatalogEntry;
		pager_mount(newCatalog,(void*)$THREAD_FOREIGN_CATALOG);
	} else {
		void* existsCatalog = (void*)(foreignCR3[catalogIndex]&0xFFFFF000);
		pager_mount(existsCatalog,(void*)$THREAD_FOREIGN_CATALOG);
	}
	PageCatalog catalog = (PageCatalog)$THREAD_FOREIGN_CATALOG;
	catalog[pageIndex] = MAKE_PAGE_ENTRY(paddr);

}


void* pager_createSpace(){

	void* cr3page = pager_reservePage();
	void* catalog3ffpage = pager_reservePage();
	void* catalog3fepage = pager_reservePage();


	pager_mount(cr3page,$THREAD_FOREIGN_CR3);
	pager_mount(catalog3ffpage,$THREAD_FOREIGN_3FF);
	pager_mount(catalog3fepage,(void*)$THREAD_FOREIGN_CATALOG);


	PageCatalog cr3 = (PageCatalog)$THREAD_FOREIGN_CR3;
	PageCatalog catalog3ff = (PageCatalog)$THREAD_FOREIGN_3FF;
	PageCatalog catalog3fe = (PageCatalog)$THREAD_FOREIGN_CATALOG;

	for (uint32 i=0;i<1024;i++){
		cr3[i] = 0;
		catalog3ff[i] = 0;
		catalog3fe[i] = 0;
	}

	cr3[0x3FF] = MAKE_PAGE_ENTRY(catalog3ffpage);
	cr3[0x3FE] = MAKE_PAGE_ENTRY(catalog3fepage);
	catalog3ff[0x3FF] = cr3[0x3FF];
	catalog3ff[0x3FE] = cr3[0x3FE];
	catalog3fe[0x3FF] = MAKE_PAGE_ENTRY(cr3page);

	pager_unmount($THREAD_FOREIGN_CR3);
	pager_unmount($THREAD_FOREIGN_3FF);
	pager_unmount((void*)$THREAD_FOREIGN_CATALOG);

	return cr3page;

}


void pager_setCatalogItem(void* __volatile__ catalogPage, uint32 __volatile__ index, void* __volatile__ item){
	pager_mount(catalogPage,(void*)$THREAD_FOREIGN_CATALOG);
	PageCatalog __volatile__ catalog = (PageCatalog) $THREAD_FOREIGN_CATALOG;
	catalog[index] = MAKE_PAGE_ENTRY(item);
	pager_unmount((void*)$THREAD_FOREIGN_CATALOG);
}


void int0xEHandler(INTPARAMS){

	void* errorAddr = 0;

	ASM("pushl %eax");
	ASM("movl %cr2, %eax");
	ASM("movl %%eax,%0":"=l"(errorAddr));
	ASM("popl %eax");

	//printfln("PAGE ERR at %h %h:%h,%h",errorAddr,segment,addr,errcode);

	uint32 catalogIndex = GET_CATALOG_INDEX(errorAddr);
	uint32 pageIndex = GET_PAGE_INDEX(errorAddr);

	PageCatalog catalog = getCatalog(catalogIndex);
	if ((catalog[pageIndex]&0x1)==0){
		void* page = CMPreservePage();
		pager_mount(page,errorAddr);
	}


}

void* getInt0xEHandler(){
	return &int0xEHandler;
}









