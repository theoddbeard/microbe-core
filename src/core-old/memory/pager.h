/*
 * pager.h
 *
 *  Created on: 12 апр. 2016 г.
 *      Author: sysadmin
 */

#ifndef MEMORY_PAGER_H_
#define MEMORY_PAGER_H_


void pager_init(uint32 dirtyPages);
void* pager_reservePage();
void pager_mount(void* __volatile__ paddr, void* __volatile__ vaddr);
void pager_unmount(void* __volatile__ vaddr);
void* getInt0xEHandler();

void* pager_getCurrentCR3();
void* pager_getCurrentCatalog(uint32 catalogIndex);

void pager_mountToForeign(void* cr3, void* paddr,void* vaddr);

void* pager_createSpace();

void pager_setCatalogItem(void* __volatile__ catalogPage, uint32 __volatile__ index, void* __volatile__ item);

#endif /* MEMORY_PAGER_H_ */
