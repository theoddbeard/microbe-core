/*
 * new_malloc.h
 *
 *  Created on: 28 апр. 2016 г.
 *      Author: sysadmin
 */

#ifndef MEMORY_MALLOC_H_
#define MEMORY_MALLOC_H_

#include "../types.h"

void heap_init(void* __volatile__ addr,uint32 __volatile__ size);
void* malloc(uint32 memsz);
void* zmalloc(uint32 memsz);
void free(void* mem);


#endif /* MEMORY_MALLOC_H_ */
