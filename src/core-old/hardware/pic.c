/*
 * pic.h
 *
 *  Created on: 27 янв. 2016 г.
 *      Author: theoddbeard
 */

#include "../console.h"
#include "../types.h"
void PIC_init(){

	__asm__("pushl %eax");
	//ICW1
	__asm__("movb $0b00010101,%al");
	__asm__("outb %al,$0x20");
	__asm__("outb %al,$0xA0");
	//ICW2
	__asm__("movb $0x20,%al");
	__asm__("outb %al,$0x21");
	__asm__("movb $0x28,%al");
	__asm__("outb %al,$0xA1");

	//ICW3
	__asm__("movb $0x4, %al");
	__asm__("outb %al,$0x21");
	__asm__("movb $0x4, %al");
	__asm__("outb %al,$0xA1");
	//ICW4
	__asm__("movb $0b00011111, %al");
	__asm__("outb %al,$0x21");
	__asm__("movb $0b00011011, %al");
	__asm__("outb %al,$0x21");

	//Disable IRQ
	__asm__("movb $0b11111111,%al");
	__asm__("outb %al,$0x21");
	__asm__("movb $0b11111111,%al");
	__asm__("outb %al,$0xA1");

	__asm__("popl %eax");
}

void PIC_enable_irq(uint16 index){
	if (index<8){
		uint8 mask = ~(0x1<<index);
		printfln("mask=%h",mask);
		__asm__("pushl %eax");
		__asm__("inb $0x21,%al");
		__asm__("movb %0,%%ah"::"b"(mask));
		__asm__("andb %ah,%al");
		__asm__("outb %al,$0x21");
		__asm__("popl %eax");

	} else {
		uint8 mask = ~(0x1<<(index-8));
		__asm__("pushl %eax");
		__asm__("inb $0xA1,%al");
		__asm__("movb %0,%%ah"::"b"(mask));
		__asm__("andb %ah,%al");
		__asm__("outb %al,$0xA1");
		__asm__("popl %eax");
	}
}

void PIC_disable_irq(uint16 index){
	if (index<8){
		uint8 mask = 0x1<<index;
		__asm__("pushl %eax");
		__asm__("inb $0x21,%al");
		__asm__("movb %0,%%ah"::"b"(mask));
		__asm__("orb %ah,%al");
		__asm__("outb %al,$0x21");
		__asm__("popl %eax");
	} else {
		uint8 mask = 0x1<<(index-8);
		__asm__("pushl %eax");
		__asm__("inb $0xA1,%al");
		__asm__("movb %0,%%ah"::"b"(mask));
		__asm__("orb %ah,%al");
		__asm__("outb %al,$0xA1");
		__asm__("popl %eax");
	}
}



