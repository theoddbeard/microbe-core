#include "../../types.h"
#include "../../console.h"

void bochs_write(uint16 port,uint16 value){
	__asm__("pushl %eax");
	__asm__("pushl %edx");
	__asm__("movw $0x01ce, %dx");
	__asm__("movw %0, %%ax"::"r"(port));
	__asm__("outw %ax,%dx");
	__asm__("movw $0x01cf, %dx");
	__asm__("movw %0, %%ax"::"r"(value));
	__asm__("outw %ax,%dx");
	__asm__("popl %edx");
	__asm__("popl %eax");
}

uint16 bochs_read(uint16 port){
	uint16 value = 0;
	__asm__("pushl %eax");
	__asm__("pushl %edx");
	__asm__("movw $0x01ce, %dx");
	__asm__("movw %0, %%ax"::"r"(port));
	__asm__("outw %ax,%dx");
	__asm__("movw $0x01cf, %dx");

	__asm__("inw %dx,%ax");
	__asm__("movw %%ax, %0":"=r"(value));
	__asm__("popl %edx");
	__asm__("popl %eax");
	return value;

}

void BOCHS_init(){
	uint16 version = bochs_read(0x0);
	printfln("BGA=%h",version);
	bochs_write(0x3,0x20);
	bochs_write(0x1,1024);
	bochs_write(0x2,768);
	bochs_write(0x6,1024);
	bochs_write(0x7,768);

	bochs_write(0x4,1);
}
