/*
 * pic.h
 *
 *  Created on: 27 янв. 2016 г.
 *      Author: theoddbeard
 */

#ifndef HARDWARE_PIC_H_
#define HARDWARE_PIC_H_

void PIC_init();
void PIC_enable_irq(uint16 index);
void PIC_disable_irq(uint16 index);



#endif /* HARDWARE_PIC_H_ */
