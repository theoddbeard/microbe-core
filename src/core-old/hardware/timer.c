/*
 * timer.c
 *
 *  Created on: 13 мая 2016 г.
 *      Author: sysadmin
 */

#include "timer.h"
#include "../tables/idt.h"
#include "../process/process.h"


void Timer_Init(){
	ASM("pushl %eax");
	ASM("movb $0b00000000, %al");
	ASM("outb %al, $0x43");
	ASM("movb 0xff, %al");
	ASM("outb %al, $0x40");
	ASM("movb 0xff, %al");
	ASM("outb %al, $0x40");
	ASM("popl %eax");
}

uint32 processId = 1;
void int0handler(INTPARAMS){
	if (processId==1){
		processId = 2;
	} else {
		processId = 1;
	}


	ASM("pushl %eax");
	ASM("inb $0x60, %al");


	ASM("inb $0x61, %al");
	ASM("pushl %eax");
	ASM("orb 0b01000000,%al");
	ASM("outb %al, $0x61");
	ASM("popl %eax");
	ASM("outb %al, $0x61");

	ASM("movb $0b01100000,%al");
	ASM("outb %al,$0x20");
	ASM("outb %al,$0xA0");

	ASM("movb $0b00100000,%al");
	ASM("outb %al,$0x20");
	ASM("outb %al,$0xA0");

	ASM("popl %eax");
	//printfln("T %d",processId);
	process_switch(processId);

}


void* Timer_GetInt0Handler(){
	return &int0handler;
}
