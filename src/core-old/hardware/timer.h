/*
 * timer.h
 *
 *  Created on: 13 мая 2016 г.
 *      Author: sysadmin
 */

#ifndef HARDWARE_TIMER_H_
#define HARDWARE_TIMER_H_

#include "../types.h"
#include "../console.h"

void Timer_Init();
void* Timer_GetInt0Handler();


#endif /* HARDWARE_TIMER_H_ */
