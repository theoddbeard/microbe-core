/*
 * process.c
 *
 *  Created on: 29 апр. 2016 г.
 *      Author: sysadmin
 */



#include "process.h"
#include "../memory/pager.h"
#include "../memory/malloc.h"
#include "../tables/tss.h"
#include "../console.h"
#include "space.h"

#define MAX_PROCESS 1024

Process* processees[MAX_PROCESS];
uint32 currentProcess = 0;
void process_init(){
	for (uint32 i=0;i<MAX_PROCESS;i++){
		processees[i] = 0;
	}

	Process* rootProcess = zmalloc(sizeof(Process));
	rootProcess->spaceId = 1;
	rootProcess->page3FD = pager_getCurrentCatalog(0x3fd);

	processees[1] = rootProcess;
	currentProcess = 1;

}

uint32 find_free_slot(){
	for (uint32 i=1;i<MAX_PROCESS;i++){
		if (processees[i]==0){
			return i;
		}
	}
	return 0;
}

uint32 find_next_process(uint32 current){
	for (uint32 i=1;i<MAX_PROCESS;i++){

	}

	return 0;
};
asm("PROCESS_INIT_STUB:");
/*
asm("movl $0xFF401FF0, %esp");
asm("movl $0x23, %eax");
asm("pushl %eax");
asm("movl $0xE0000FF0, %eax");
asm("pushl %eax");
asm("pushf");
asm("movl $0x1B, %eax");
asm("pushl %eax");
asm("movl $0x100724, %eax");
asm("pushl %eax");
*/
asm("sti");
asm("iret");


uint32 process_create(uint32 spaceId,void* __volatile__ init,void* __volatile__ stackPointer){
	uint32 __volatile__ slot = find_free_slot();
	if (slot==0){
		return 0;
	}

	Process* __volatile__ process = zmalloc(sizeof(Process));

	void* __volatile__ page3fd = pager_reservePage();
	void* __volatile__ TssPage = pager_reservePage();
	void* __volatile__ kstackPage = pager_reservePage();

	process->spaceId = spaceId;
	process->page3FD = page3fd;

	processees[slot] = process;

	pager_setCatalogItem(page3fd,0,TssPage);
	pager_setCatalogItem(page3fd,1,kstackPage);


	//PREPARE STACK FOR NEW PROCESS
	uint32 __volatile__ esp = 0x0;
	pager_mount(kstackPage,(void*)$THREAD_FOREIGN_TSS);

	//SAVE REGISTERS AND STACK
	ASM("movl %esp, %esi");
	ASM("pushl %eax");
	ASM("pushl %ebx");
	ASM("pushl %ecx");
	ASM("pushl %edx");
	ASM("movl %esp, %ebx");

	//SET STACK
	//ASM("movl $0xFF406FF0, %esp");
	//PREPARE NEW STACK
	ASM("movl %esi, %esp");
	ASM("movl %0, %%eax"::"a"(stackPointer));
	ASM("movl %0, %%edx"::"d"(init));
	//ASM("hlt");
	ASM("movl $0xFF406FF0, %esp");
	//Push iret address
	ASM("pushl $0x0");
	ASM("pushl $0x23");
	ASM("pushl %eax");
	ASM("sti");
	ASM("pushf");
	ASM("cli");
	ASM("pushl $0x1B");
	ASM("pushl %edx");
/*
	typedef struct {
		unsigned int gs;
		unsigned int fs;
		unsigned int es;
		unsigned int ds;
		unsigned int ebp;
		unsigned int esi;
		unsigned int edi;
		unsigned int edx;
		unsigned int ecx;
		unsigned int ebx;
		unsigned int eax;
		unsigned int ret;
		unsigned int iret_eip;
		unsigned int iret_cs;
		unsigned int flags;
		unsigned int r3_esp;
		unsigned int r3_ss;
	} NewStack;
*/
	//Push RET address
	ASM("pushl $PROCESS_INIT_STUB");

	//Push register states
	ASM("movl $0, %eax");
	ASM("pushf");
	ASM("pushl %eax");
	ASM("pushl %eax");
	ASM("pushl %eax");
	ASM("pushl %eax");
	ASM("pushl %eax");
	ASM("pushl %eax");
	ASM("pushl %eax");
	ASM("movl $0x23, %eax");
	ASM("pushl %eax");
	ASM("pushl %eax");
	ASM("pushl %eax");
	ASM("pushl %eax");

	ASM("movl %esp, %ecx");
	ASM("subl $0x5000, %ecx");

	//RECOVER STACK
	ASM("movl %esi, %esp");
	ASM("movl %%ecx, %0":"=m"(esp));
	ASM("movl %ebx, %esp");

	ASM("popl %edx");
	ASM("popl %ecx");
	ASM("popl %ebx");
	ASM("popl %eax");

	//ASM("movl $0xABCD, 0x2c(%esp)");

	pager_unmount((void*)$THREAD_FOREIGN_TSS);


	pager_mount(TssPage,(void*)$THREAD_FOREIGN_TSS);

	TSS_init_foreign((void*)$THREAD_FOREIGN_TSS,esp);


	pager_unmount((void*)$THREAD_FOREIGN_TSS);



	printfln("NEW PROC %h",slot);
	printfln("init=%h,stack=%h,kstack=%h, 3fd=%h",init,stackPointer,esp,process->page3FD);
	return slot;
}



asm("PROCESS_SWITCH:");

//SAVE REGISTERS TO STACK
asm("pushf");
asm("pushl %eax");
asm("pushl %ebx");
asm("pushl %ecx");
asm("pushl %edx");
asm("pushl %edi");
asm("pushl %esi");
asm("pushl %ebp");
asm("movl %ds, %eax");
asm("pushl %eax");
asm("movl %es, %eax");
asm("pushl %eax");
asm("movl %fs, %eax");
asm("pushl %eax");
asm("movl %gs, %eax");
asm("pushl %eax");



/* eax 4
 * ebx 8
 * ecx 12
 * edx 16
 * edi 20
 * esi 24
 * ebp 28
 * ds 32
 * es 36
 * fs 40
 * gs 44
 */

asm("movl %esp, %ebp");
//asm("addl $48, %ebp");
asm("addl $52, %ebp");

asm("movl (%ebp), %edx"); //edx - 3fd
asm("movl 0x4(%ebp), %eax"); //eax - cr3
//asm("hlt");

//SAVE STACK
asm("movl %esp, 0xFF400038");

//SWITCH CR3
asm("movl %eax, %cr3");

//SWITHC 3FD
asm("orl $0x17, %edx");
asm("movl %edx, 0xFFFFFFF4");
asm("movl %edx, 0xFFBFFFF4");
asm("movl %eax, %cr3");

//RESTORE STACK
asm("movl 0xFF400038, %esp");
//RESTORE REGISTERS FROM STACK

asm("popl %eax");
asm("movl %eax, %gs");
asm("popl %eax");
asm("movl %eax, %fs");
asm("popl %eax");
asm("movl %eax, %es");
asm("popl %eax");
asm("movl %eax, %ds");
asm("popl %ebp");
asm("popl %esi");
asm("popl %edi");
asm("popl %edx");
asm("popl %ecx");
asm("popl %ebx");
asm("popl %eax");
asm("popf");
asm("ret");




void process_switch(uint32 processId){

	if (processees[processId]==0){
		printfln("PROC IS NULL");
		return;
	}

	Process* __volatile__ target = processees[processId];
	Space* __volatile__ targetSpace = space_getSpace(target->spaceId);


	//printfln("SWITCH TO PROCID=%h, CR3=%h, 3FD=%h",processId,targetSpace->cr3,target->page3FD);

	currentProcess = processId;

	ASM("pushl %eax");
	ASM("pushl %edx");
	ASM("addl $0x8 ,%esp");
	ASM("movl %0, %%edx"::"m"(target->page3FD));
	ASM("movl %0, %%eax"::"m"(targetSpace->cr3));

	ASM("subl $0x8, %esp");
	ASM("pushl %eax");
	ASM("pushl %edx");
	ASM("call PROCESS_SWITCH");
	ASM("addl $0x8, %esp");
	ASM("popl %edx");
	ASM("popl %eax");

/*

	//SAVE CONTEXT TO TSS
	ASM("movl %eax, 0xFF400000+0x28");
	ASM("movl %ebx, 0xFF400000+0x34");
	ASM("movl %ecx, 0xFF400000+0x2C");
	ASM("movl %edx, 0xFF400000+0x30");
	ASM("movl %esp, 0xFF400000+0x38");
	ASM("movl %ebp, 0xFF400000+0x3C");
	ASM("movl %esi, 0xFF400000+0x40");
	ASM("movl %edi, 0xFF400000+0x44");
	ASM("movl %es, %eax");
	ASM("movl %eax, 0xFF400000+0x48");
	ASM("movl %ss, %eax");
	ASM("movl %eax, 0xFF400000+0x50");
	ASM("movl %ds, %eax");
	ASM("movl %eax, 0xFF400000+0x54");
	ASM("movl %fs, %eax");
	ASM("movl %eax, 0xFF400000+0x58");
	ASM("movl %gs, %eax");
	ASM("movl %eax, 0xFF400000+0x5C");

	//SWITCH CR3
	ASM("movl %0, %%eax"::"a"(targetSpace->cr3));
	ASM("movl %eax, %cr3");

	//SWITCH 3FD

	//RESTORE CONTEXT FROM TSS
	ASM("movl 0xFF400000+0x5C, %eax");
	ASM("movl %eax, %gs");
	ASM("movl 0xFF400000+0x58, %eax");
	ASM("movl %eax, %fs");
	ASM("movl 0xFF400000+0x54, %eax");
	ASM("movl %eax, %ds");
	ASM("movl 0xFF400000+0x50, %eax");
	ASM("movl %eax, %ss");
	ASM("movl 0xFF400000+0x4C, %eax");
	//ASM("movl %eax, %cs");
	ASM("movl 0xFF400000+0x48, %eax");
	ASM("movl %eax, %es");

	ASM("movl 0xFF400000+0x44, %edi");
	ASM("movl 0xFF400000+0x40, %esi");
	ASM("movl 0xFF400000+0x3C, %ebp");
	ASM("movl 0xFF400000+0x38, %esp");
	ASM("movl 0xFF400000+0x30, %edx");
	ASM("movl 0xFF400000+0x2C, %ecx");
	ASM("movl 0xFF400000+0x34, %ebx");

	ASM("movl 0xFF400000+0x28, %eax");
*/
}
