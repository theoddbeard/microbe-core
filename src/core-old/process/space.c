/*

/ * space.c
 *
 *  Created on: 27 апр. 2016 г.
 *      Author: sysadmin
 */

#include "space.h"
#include "../console.h"
#include "../memory/malloc.h"
#include "../memory/pager.h"

#define $SPACES_COUNT 1024

Space* spaces[1024];
CurrentSpaceState* currentSpace = (CurrentSpaceState*)$SPACE_CONTEXT;

void space_init(){
	printfln("SPACES Init");
	for (uint32 i=0;i<$SPACES_COUNT;i++){
		spaces[i] = 0;
	}

	Space* __volatile__ rootSpace = zmalloc(sizeof(rootSpace));

	rootSpace->cr3 = pager_getCurrentCR3();
	rootSpace->parent = 0;
	spaces[1] = rootSpace;
}

uint32 get_free_slot(){
	for (uint32 i=1;i<$SPACES_COUNT;i++){
		if (spaces[i]==0){
			return i;
		}
	}

	return 0;
}

uint32 space_create(){
	printfln("CREATE SPACE");
	uint32 slot = get_free_slot();

	if (slot==0){
		printfln("SLOT NOT FOUND");
		return 0;
	}
	printfln("SLOT=%h",slot);

	Space* newSpace = (Space*)malloc(sizeof(Space));
	newSpace->cr3 = pager_createSpace();
	//newSpace->parent = currentSpace->index;
	spaces[slot] = newSpace;

	return slot;
}


uint32 space_alloc(void* vaddr,uint32 count){
	printfln("Try alloc %d pages, vaddr=%h",count,vaddr);
	for (uint32 i=0;i<count;i++){
		vaddr = (void*)((uint32)vaddr&0xFFFFF000);
		void* page = pager_reservePage();
		pager_mount(page,vaddr);
		vaddr+=0x1000;
	}
	return count;
}

uint32 space_mount(void* src, uint32 dstSpace, void* dst){
	return 0;
}

uint32 space_fork(){
	return 0;
}

Space* space_getSpace(uint32 spaceId){
	return spaces[spaceId];
}






