/*
 * space.h
 *
 *  Created on: 27 апр. 2016 г.
 *      Author: sysadmin
 */

#ifndef PROCESS_SPACE_H_
#define PROCESS_SPACE_H_

#include "../types.h"
#include "../core-mmap.h"

typedef struct {
	void* __volatile__ page3FF;
	void* __volatile__ cr3;
	uint32 parent;
	uint32 __padding;
} Space;



typedef struct {
	uint32 index;
	uint32 cr3;
} CurrentSpaceState;

void space_init();
uint32 space_create();
uint32 space_alloc(void* vaddr,uint32 count);

Space* space_getSpace(uint32 spaceId);
#endif /* PROCESS_SPACE_H_ */
