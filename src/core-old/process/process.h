/*
 * process.h
 *
 *  Created on: 29 апр. 2016 г.
 *      Author: sysadmin
 */

#ifndef PROCESS_PROCESS_H_
#define PROCESS_PROCESS_H_

#include "../types.h"
#include "../core-mmap.h"


typedef struct {
	uint32 __volatile__ spaceId;
	void* __volatile__ page3FD;
	uint32 __align0;
	uint32 __align1;
} Process;

void process_init();
uint32 process_create(uint32 spaceId,void* __volatile__ run,void* __volatile__ stackPointer);
void process_switch(uint32 processId);


#endif /* PROCESS_PROCESS_H_ */
