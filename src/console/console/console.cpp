#include "console.hpp"

#include <stdarg.h>


namespace Console{
unsigned short width,height;
unsigned short x,y;

unsigned char color;
unsigned char color_str;
unsigned char color_dec;
unsigned char color_hex;

char* vbuffer;
char* cursor;
char* secondline;
char* lastline;

char lptchar;

void init(void* buffer,unsigned short aWidth, unsigned short aHeight){
	vbuffer = (char*) buffer;
	cursor = vbuffer;
	width = aWidth;
	height = aHeight;
	x = 0;
	y = 0;
	color = EGAWhite;
	color_str = EGAWhite;
	color_dec = EGABlue;
	color_hex = EGAYellow;
	
	secondline = vbuffer+width*2;
	lastline = vbuffer+(width*(height-1)*2);
	
	unsigned long count = aWidth*aHeight;
	for(unsigned long i = 0;i<count;i++){
		vbuffer[i*2] = color;
		vbuffer[i*2+1] = 0x00;
	}
	

}

void setColor(EGAColor aColor){
	color = aColor;
}


void memcpyd(short* src,short* dest,unsigned long count){
	for(unsigned long i=0;i<count;i++){
		dest[i] = src[i];
	}
}



void checkCursorPosition(){
	if (x==width){
		x = 0;
		y++;
		//cursor+=2;
	}
	
	if (y==height){
		memcpyd((short*)secondline,(short*)vbuffer,width*(height-1));
		for (unsigned short i=0;i<width;i++){
			lastline[i*2] = 0x0;
			lastline[i*2+1] = color;
		}
		y--;
		cursor-=width<<1;
	}	
}

void doCR(){

	unsigned short linePosition = (cursor-vbuffer)%(width<<1);
	x-=linePosition;
	cursor-=linePosition;
	checkCursorPosition();

}

void doLF(){
	y++;
	cursor+=width<<1;
	checkCursorPosition();

}

void doTab(){
	outString("        ");
}


void outChar(char aChar){
	if (aChar==0x0A){
		doLF();
		return;
	}
	if (aChar==0x0D){
		doCR();
		return;
	}

	checkCursorPosition();
	cursor[0] = aChar;
	cursor[1] = color;

	x++;	
	cursor+=2;
}

void outString(char* aString){
	while (aString[0] != 0x0){
		unsigned char achar = aString[0];
		switch(achar){
			case 0x0A: doLF();break;
			case 0x0D: doCR();break;
			case 0x09: doTab();break;
			default:outChar(achar);break;
		}
		aString++;
	}
}

void outStringLen(char* aString, unsigned int length){
	for (unsigned int i=0;i<length;i++){
		unsigned char achar = aString[i];
		switch(achar){
			case 0x0A: doLF();break;
			case 0x0D: doCR();break;
			case 0x09: doTab();break;
			default:outChar(achar);break;
		}

	}
}
void outStringLenLn(char* aString, unsigned int length){
	outStringLen(aString, length);
	doCR();
	doLF();
}

void outStringln(char* aString){
	outString(aString);
	doCR();
	doLF();
	
}


void outIntDec(unsigned long aValue){
	char buffer[11];
	char* current = &buffer[10];
	
	*current = 0x0;
	do{
		current--;		
		char digit = (aValue%10)+0x30;						
		*current = digit;
		
		
		aValue = aValue/10;
	} while (aValue>0);

	char oldc = color;
	color = color_dec;
	outString(current);
	color = oldc;
	
	
	
}
void outIntDecln(unsigned long aValue){
	outIntDec(aValue);
	doCR();
	doLF();
}

void outIntHex(unsigned long aValue){
	char buffer[11];
	char *string = &buffer[8];
	*string = 0x0;
	do{
		string--;
		char digit = aValue&0xF;
		aValue = aValue>>4;
		switch(digit){
			case 0x0A:digit = 'A';break;
			case 0x0B:digit = 'B';break;
			case 0x0C:digit = 'C';break;
			case 0x0D:digit = 'D';break;
			case 0x0E:digit = 'E';break;
			case 0x0F:digit = 'F';break;
			default:digit+=0x30;break;
		}
		*string = digit;		
	} while (aValue>0);
	char oldc = color;
	color = color_hex;
	outString("0x");
	outString(string);
	color = oldc;
	
}
void outIntHexln(unsigned long aValue){
		outIntHex(aValue);
		doCR();
		doLF();
}

void endline(){
	doLF();
	doCR();
}

void vprintf(const char* format,va_list args){
	while (*format){
		switch(*format){
			case '%':
				format++;
				switch(*format){
					case 's':
						outString(va_arg(args, char*));
						break;
					case 'c':
						outChar(va_arg(args,unsigned int));
						break;
					case 'd':
						outIntDec(va_arg(args,unsigned int));
						break;
					case 'h':
						outIntHex(va_arg(args,unsigned int));
						break;
					case 'n':
						endline();
						break;
					case 't':
						doTab();
						break;
				}
				break;
			
			default: 
				outChar(*format);
						
		}
		format++;
	}
	
}

void printf(const char* format,...){
	va_list args;
	va_start(args,format);
	vprintf(format,args);
	va_end(args);
}

void printfln(const char* format,...){
	va_list args;
	va_start(args,format);
	vprintf(format,args);
	endline();
	va_end(args);
}

}
