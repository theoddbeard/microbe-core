#ifndef CONSOLE_H_
#define CONSOLE_H_

namespace Console{
typedef enum EGAColor {EGABlack=0,EGANavy=1,EGAGreen=2,EGATeal=3,EGAMaroon=4,EGAPurpure=5,EGAOlive=6,EGAGray=7,
	 EGASilver=8,EGABlue=9,EGALime=10,EGAAqua=11,EGARed=12,EGAFuchsia=13,EGAYellow=14,EGAWhite=15} EGAColor;

	 
void init(void* buffer,unsigned short aWidth, unsigned short aHeight);
void setColor(EGAColor);

void outString(char*);
void outStringln(char*);
void outStringLen(char* aString, unsigned int length);
void outStringLenLn(char* aString, unsigned int length);
void outChar(char aChar);

void outIntDec(unsigned long);
void outIntDecln(unsigned long);

void outIntHex(unsigned long);
void outIntHexln(unsigned long);

void endline();

void printf(const char* format,...);
void printfln(const char* format,...);
void disableScreen();
}
#endif
