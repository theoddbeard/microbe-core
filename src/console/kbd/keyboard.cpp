/*
 * keyboard.cpp
 *
 *  Created on: 6 февр. 2017 г.
 *      Author: sysadmin
 */

#include "keyboard.hpp"

char shift = 0;



namespace KeyboardKeys{
	typedef enum  {
		ESC=0x1,
		K_1=0x2,
		K_2=0x3,
		K_3=0x4,
		K_4=0x5,
		K_5=0x6,
		K_6=0x7,
		K_7=0x8,
		K_8=0x9,
		K_9=0xA,
		K_0=0xB,
		K_MINUS=0xC,
		K_PLSU=0xD,
		K_BACKSPACE=0xE,
		K_TAB=0xF,
		K_Q=0x10,
		K_W=0x11,
		K_E=0x12,
		K_R=0x13,
		K_T=0x14,
		K_Y=0x15,
		K_U=0x16,
		K_I=0x17,
		K_O=0x18,
		K_P=0x19,
		K_LQB=0x1A,
		K_RQB=0x1B,
		K_ENTER=0x1C,
		K_CTRL=0x1D,
		K_A=0x1E,
		K_S=0x1F,
		K_D=0x20,
		K_F=0x21,
		K_G=0x22,
		K_H=0x23,
		K_J=0x24,
		K_K=0x25,
		K_L=0x26,
		K_SEMICOLON=0x27,
		K_QUOTE=0x28,
		K_TILDA=0x29,
		K_LSHIFT=0x2A,
		K_BACKSLASH=0x2B,
		K_Z=0x2C,
		K_X=0x2D,
		K_C=0x2E,
		K_V=0x2F,
		K_B=0x30,
		K_N=0x31,
		K_M=0x32,
		K_SEMIDOT=0x33,
		K_DOT=0x34,
		K_SLASH=0x35,
		K_RSHIFT=0x36,
		K_NUM_STAR=0x37,
		K_ALT=0x38,
		K_SPACE=0x39,
		K_CAPS=0x3A,
		K_F1=0x3B,
		K_F2=0x3C,
		K_F3=0x3D,
		K_F4=0x3E,
		K_F5=0x3F,
		K_F6=0x40,
		K_F7=0x41,
		K_F8=0x42,
		K_F9=0x43,
		K_F10=0x44,
		K_NUM=0x45,
		K_SCROLL=0x46,
		K_HOME=0x47,
		K_PGUP=0x49,
		K_NUM_MINUS=0x4A,
		K_NUM_5=0x4C,
		K_NUM_PLUS=0x4E,
		K_END=0x4F,
		K_VSLASH=0x50,
		K_PGDN=0x51,
		K_INS=0x52,
		K_DEL=0x53,
		K_SYSRQ=0x54,
		K_MACRO=0x56,
		K_F11=0x57,
		K_F12=0x58,
		K_PA1=0x5A,
		K_LWIN=0x5B,
		K_RWIN=0x5C,
		K_MENU=0x5D,
		K_F16=0x63,
		K_F17=0x64,
		K_F18=0x65,
		K_F19=0x66,
		K_F20=0x67,
		K_F21=0x68,
		K_F22=0x69,
		K_F23=0x6A,
		K_F24=0x6B

	} KeyboardScanCode;
	char getChar(uint8 code){
		switch (code){
		case K_1:return '1';
		case K_2:return '2';
		case K_3:return '3';
		case K_4:return '4';
		case K_5:return '5';
		case K_6:return '6';
		case K_7:return '7';
		case K_8:return '8';
		case K_9:return '9';
		case K_0:return '0';
		case K_A:return 'a';
		case K_B:return 'b';
		case K_C:return 'c';
		case K_D:return 'd';
		case K_E:return 'e';
		case K_F:return 'f';
		case K_G:return 'g';
		case K_H:return 'h';
		case K_I:return 'i';
		case K_J:return 'j';
		case K_K:return 'k';
		case K_L:return 'l';
		case K_M:return 'm';
		case K_N:return 'n';
		case K_O:return 'o';
		case K_P:return 'p';
		case K_Q:return 'q';
		case K_R:return 'r';
		case K_S:return 's';
		case K_T:return 't';
		case K_U:return 'u';
		case K_V:return 'v';
		case K_W:return 'w';
		case K_X:return 'x';
		case K_Y:return 'y';
		case K_Z:return 'z';
		case K_SPACE:return ' ';
		case K_MINUS:return '-';
		case K_QUOTE:return '\'';
		case K_TILDA:return '`';
		case K_SEMICOLON:return ';';
		case K_SEMIDOT:return ',';
		case K_DOT:return '.';
		case K_LQB:return '[';
		case K_RQB:return ']';
		case K_PLSU:return '=';
		}
		return code;
	}

}

char upperCase(char c){
	if (c>='a'&&c<='z'){
		return c-0x20;
	}
	switch (c){
	case '1':return '!';
	case '2':return '@';
	case '3':return '#';
	case '4':return '$';
	case '5':return '%';
	case '6':return '^';
	case '7':return '&';
	case '8':return '*';
	case '9':return '(';
	case '0':return ')';
	case '-':return '_';
	case '\'':return '"';
	case '`':return '~';
	case ';':return ':';
	case '.':return '>';
	case ',':return '<';
	case '[':return '{';
	case ']':return '}';
	case '=':return '+';
	}
	return c;
}

uint8 read(){
	uint8 a = 0;
	    	asm("pushl %eax");
	//    	asm("inb $0x64, %al");
	//    	asm("andb 0b11,%al");
	//    	asm("cmp $0,%al");
	//    	asm("jz NOKB");
	    	asm("inb $0x60, %al");
	    	asm("addl $0x4,%esp");
	    	asm("movb %%al,%0":"=m"(a));
	    	asm("subl $0x4,%esp");
	    	asm("popl %eax");
	    	return a;
}
char lastKey = 0;
char Keyboard::Read(){
	uint8 key = read();
	if (key<0x80){
		if (key==KeyboardKeys::K_LSHIFT||key==KeyboardKeys::K_RSHIFT){
			shift = 1;
		}
		if (key!=lastKey){
			lastKey = key;
			char c = KeyboardKeys::getChar(key);
			if (c>0){
				if (shift==1){
					c = upperCase(c);
				}
				//Console::outChar(c);

			}
			return c;
		}
	} else {
		key = key-0x80;
		if (key==KeyboardKeys::K_LSHIFT||key==KeyboardKeys::K_RSHIFT){
			shift = 0;
		}
		lastKey = 0;
	}
	return 0;
}






