#include <corelib/corelib.h>
#include "console/console.hpp"
#include "kbd/keyboard.hpp"
#include <classes/Channel.hpp>
#include <stdlib/stdlib.h>
#include <classes/Classes.hpp>
#include "vga/vga.hpp"


void kbdLoop(){
	OutputChannel os((void*)0xA0000000,0x800);
	for(;;){
		char c = Keyboard::Read();
		if (c!=0){
			os.write(c);
		}

	}
}

void dispatcher(){

}


void main(uint32 argc, char** argv){

	Core_Mem_Mount((void*)0xb8000,(void*)0x0A000000,0x1000);
	Console::init((void*)0x0A000000, 80, 25);

	Core_Mem_Alloc((void*)0xA0000000, 0x1000);
	InputChannel ic((void*)0xA0000000,0x800);

	Core_Mem_Alloc((void*)0x09000000, 0x1000);
	Core_Process_Create(0, (void*)&kbdLoop,(void*)0x09000FF0);


	Console::printf("root@localhost#:");

	for(;;){
		char c = ic.read();
		if (c==0x1c){
			Console::printf("\r\nroot@localhost#:");
		} else {
			Console::outChar(c);
		}
	}

}
