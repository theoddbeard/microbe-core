/*
 * vga.hpp
 *
 *  Created on: 9 февр. 2017 г.
 *      Author: sysadmin
 */

#ifndef VGA_VGA_HPP_
#define VGA_VGA_HPP_


class VideoDriver{
public:
	virtual void init() = 0;
};


class VgaDriver:public VideoDriver{
public:
	void init();
};







#endif /* VGA_VGA_HPP_ */
